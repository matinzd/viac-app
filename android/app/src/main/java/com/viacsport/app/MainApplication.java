package com.viacsport.app;

import android.app.Application;
import android.support.multidex.MultiDexApplication;

import com.facebook.react.ReactApplication;
import com.brentvatne.react.ReactVideoPackage;
import com.shahenlibrary.RNVideoProcessingPackage;
import io.invertase.firebase.RNFirebasePackage;
import com.rnfs.RNFSPackage;
import com.apsl.versionnumber.RNVersionNumberPackage;
import com.reactnativecommunity.viewpager.RNCViewPagerPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.dylanvann.fastimage.FastImageViewPackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.imagepicker.ImagePickerPackage;
import com.swmansion.reanimated.ReanimatedPackage;
import com.airbnb.android.react.lottie.LottiePackage;
import com.facebook.react.modules.i18nmanager.I18nUtil;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.horcrux.svg.SvgPackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends MultiDexApplication implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new ReactVideoPackage(),
            new RNVideoProcessingPackage(),
            new RNFirebasePackage(),
            new RNFSPackage(),
            new RNVersionNumberPackage(),
            new RNCViewPagerPackage(),
            new PickerPackage(),
            new FastImageViewPackage(),
            new MapsPackage(),
            new ImagePickerPackage(),
            new ReanimatedPackage(),
            new LottiePackage(),
            new RNGestureHandlerPackage(),
            new VectorIconsPackage(),
            new SvgPackage(),
            new AsyncStoragePackage(),
            new ViacPackages()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
    I18nUtil.getInstance().allowRTL(this, true);
    I18nUtil.getInstance().forceRTL(this, true);
  }
}
