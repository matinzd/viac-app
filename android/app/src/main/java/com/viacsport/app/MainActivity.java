package com.viacsport.app;

import android.os.Bundle;
import android.util.Log;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;

import android.content.Intent;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "viac";
    }

    @Override
    protected ReactActivityDelegate createReactActivityDelegate() {
        return new ReactActivityDelegate(this, getMainComponentName()) {
            @Override
            protected ReactRootView createRootView() {
                return new RNGestureHandlerEnabledRootView(MainActivity.this);
            }
        };
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if( data != null ) {
            Log.d("MAIN_ACT", "Result " + data.getIntExtra("RESPONSE_CODE", 0));
            Log.d("MAIN_ACT", "Result " + data.getStringExtra("INAPP_PURCHASE_DATA"));
            Log.d("MAIN_ACT", "Result " + data.getStringExtra("INAPP_DATA_SIGNATURE"));
        }
        getReactInstanceManager().onActivityResult(MainActivity.this, requestCode, resultCode, data);
    }
}
