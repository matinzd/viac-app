package com.viacsport.app;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Nonnull;

import util.IabHelper;
import util.IabResult;
import util.Inventory;
import util.Purchase;

public class CafeBazaarIABHandler extends ReactContextBaseJavaModule implements ActivityEventListener {

    private IabHelper iabHelper;
    private String SKU = "";
    private int RC_REQUEST = -1;
    private static String TAG = "CafeBazaarIABHelper";
    private static String EXTRA_DATA = "VIAC_APP_EXTRA_DATA";
    private Promise promise = null;

    public CafeBazaarIABHandler(@Nonnull ReactApplicationContext reactContext) {
        super(reactContext);
        reactContext.addActivityEventListener(this);
    }

    @Nonnull
    @Override
    public String getName() {
        return "CafeBazaarIABHandler";
    }


    @ReactMethod
    public void consume(String SKU, Promise promise) {
//        iabHelper.consumeAsync();
        iabHelper.queryInventoryAsync((response, inv) -> {
            if (inv.hasPurchase(SKU) && response.isSuccess()) {
                iabHelper.consumeAsync(inv.getPurchase(SKU), (purchase, result) -> {
                    if (result.isSuccess()) {
                        WritableMap object = new WritableNativeMap();
                        object.putString("purchaseToken", purchase.getToken());
                        object.putDouble("purchaseTime", purchase.getPurchaseTime());
                        object.putString("productId", purchase.getSku());
                        object.putString("packageName", purchase.getPackageName());
                        object.putInt("purchaseState", purchase.getPurchaseState());
                        promise.resolve(object);
                    } else {
                        promise.reject("500", "خطا مصرف در خرید");
                    }
                });
            } else {
                promise.reject("500", "خطا در خرید");
            }
        });
    }

    @ReactMethod
    public void startPayment(String base64EncodedPublicKey, String SKU, int RC_REQUEST, Promise promise) {
        this.SKU = SKU;
        this.RC_REQUEST = RC_REQUEST;
        this.promise = promise;
        iabHelper = new IabHelper(getReactApplicationContext(), base64EncodedPublicKey);
        iabHelper.enableDebugLogging(true);
        try {
            iabHelper.startSetup(result -> {
                Log.d(TAG, "Setup finished");
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    Log.d(TAG, "Problem setting up in-app billing: " + result);
                    promise.reject("400", "لطفا نرم افزار کافه بازار را نصب کنید");
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (iabHelper == null) return;

                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                Log.d(TAG, "Setup successful. Start payment");
                iabHelper.launchPurchaseFlow(getCurrentActivity(), SKU, RC_REQUEST, (result1, purchase) -> {
                    Log.d(TAG, "Purchase finished: " + result1 + result1.getResponse() + ", purchase: " + purchase);

                    // if we were disposed of in the meantime, quit.
                    if (iabHelper == null) return;
//                iabHelper.dispose();
//                iabHelper = null;
                    if (result1.isFailure()) {
                        Log.d(TAG, "Error purchasing: " + result1);
                        if (result1.getResponse() == -1005) {
                            promise.reject(String.valueOf(result1.getResponse()), "شما از پرداخت منصرف شدید");
                        } else if( result1.getResponse() == -1002 ) {
                            promise.reject(String.valueOf(result1.getResponse()),"شما از ورود به حساب کافه بازار خود منصرف شدید!");
                        } else {
                            promise.reject(String.valueOf(result1.getResponse()),"خطا در شروع پرداخت");
                        }
                        return;
                    }
                    if (!purchase.getDeveloperPayload().equals(EXTRA_DATA)) {
                        Log.d(TAG, "Error purchasing. Authenticity verification failed.");
                        promise.reject("500", "Error purchasing. Authenticity verification failed.");
                        return;
                    }

                    Log.d(TAG, "Purchase successful.");

                    if (purchase.getSku().equals(SKU)) {
                        // bought 1/4 tank of gas. So consume it.
                        Log.d(TAG, "Purchase is " + SKU);
                    }

                    WritableMap object = new WritableNativeMap();
                    object.putString("purchaseToken", purchase.getToken());
                    object.putDouble("purchaseTime", purchase.getPurchaseTime());
                    object.putString("productId", purchase.getSku());
                    object.putString("packageName", purchase.getPackageName());
                    object.putInt("purchaseState", purchase.getPurchaseState());
                    promise.resolve(object);
                }, EXTRA_DATA);

            });
        } catch (Exception ex) {
            if( ex instanceof SecurityException ) {
                promise.reject("300", "لطفا دوباره نرم افزار را از بازار دانلود و نصب کنید.\n بابت این موضوع بسیار متاسفیم!");
            }
        }
    }

    @ReactMethod
    public void close() {
        if (iabHelper != null) {
            iabHelper.dispose();
            iabHelper = null;
        }
    }

    @Override
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {

        if (iabHelper == null) return;

        Log.d(TAG, "Handle activity result");

        if (!iabHelper.handleActivityResult(requestCode, resultCode, data)) {
            Log.i(TAG, "Not handled");
        } else {
            Log.i(TAG, "Handled");
        }
    }

    @Override
    public void onNewIntent(Intent intent) {

    }
}
