package com.viacsport.app;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.imagepicker.FileProvider;

import java.io.File;

import javax.annotation.Nonnull;

public class UpdateHandler extends ReactContextBaseJavaModule {

    private ReactApplicationContext context;
    public UpdateHandler(@Nonnull ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Nonnull
    @Override
    public String getName() {
        return "UpdateHandler";
    }

    @ReactMethod
    public void openInstaller(String dir, String appName){
        File file = new File(dir, appName);
//        Uri uri = FileProvider.getUriForFile(getReactApplicationContext(), BuildConfig.APPLICATION_ID + ".provider", file);
//        Intent installPrompt = new Intent(Intent.ACTION_VIEW)
//                .setDataAndType(uri, "application/vnd.android.package-archive");
//        installPrompt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        installPrompt.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        getReactApplicationContext().startActivity(installPrompt);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Uri apkUri = FileProvider.getUriForFile(getReactApplicationContext(), BuildConfig.APPLICATION_ID + ".provider", file);
            Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
            intent.setData(apkUri);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            getReactApplicationContext().startActivity(intent);
        } else {
            Uri apkUri = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getReactApplicationContext().startActivity(intent);
        }
    }
}
