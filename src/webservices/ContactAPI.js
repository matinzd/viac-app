import api from './BaseAdapter'
import { setStore, getStore } from 'trim-redux';
import ViacToast from '../controllers/ViacToast';

class ContactAPI {

    async send(fullName, subject, email, phoneNumber, content) {
        return await api.request('/contactus/send', {
            fullName,
            subject,
            email,
            phoneNumber,
            content
        }, 'post', false);
    }

}

export default new ContactAPI