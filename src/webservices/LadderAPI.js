
import api from './BaseAdapter'

class LadderAPI {

    async getAll() {
        return await api.request('/ladder/GetAll', {
        }, 'post', false, false);
    }

}

export default new LadderAPI