import api from './BaseAdapter'
import { setStore, getStore } from 'trim-redux';

class CategoryAPI {

    /**
     * 
     * @param {String} parentId
     */
    async getAll(parentId) {
        return await api.request('/category/GetAll', { parentId }, 'post', true);
    }

}

export default new CategoryAPI