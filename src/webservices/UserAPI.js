import api from './BaseAdapter'
import { setStore, getStore } from 'trim-redux';

class UserAPI {

    /**
     * 
     * @param {number} mobileNumber
     */
    async signUp(mobileNumber) {
        return await api.request('/auth/signUp', { mobileNumber }, 'post', false);
    }

    /**
     * 
     * @param {FormData} formData 
     */
    async updateMe(formData){
        let result =  await api.request('/auth/updateMe', formData, 'post', true, true);
        this.getProfile()
        return result
    }


     /**
     * 
     * @param {number} mobileNumber 
     */
    async validate(mobileNumber, activationCode) {
        return await api.request('/auth/validateActivationCode', { mobileNumber, activationCode }, 'post', false);
    }

    async getProfile() {
        let userInfo = getStore('userInfo');
        let response = await api.request('/auth/me', null, 'post', true)
        userInfo.name = response.data.data.user.name;
        userInfo.lastName = response.data.data.user.lastName;
        userInfo.email = response.data.data.user.email;
        userInfo.cityId = response.data.data.user.city_id;
        userInfo.mobileNumber = response.data.data.user.mobileNumber;
        userInfo.deActive = response.data.data.user.deActive;
        userInfo.pix = response.data.data.user.pix;
        userInfo.sex = response.data.data.user.sex;
        userInfo.reagentCode = response.data.data.user.reagentCode;
        userInfo.fromReagentCode = response.data.data.user.fromReagentCode;
        setStore({
            userInfo: userInfo
        })
    }

}

export default new UserAPI