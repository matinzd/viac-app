import api from './BaseAdapter'
import { setStore, getStore } from 'trim-redux';

class PlayerAPI {

    /**
     * 
     * @param {FormData} formData
     */
    async insertUpdate(formData) {
        return await api.request('/player/InsertUpdate', formData, 'post', true, true);
    }

    /**
     * 
     * @param {String} parentId
     */
    async getAll(tags = [], states = [], cities = [], categories = [], search = "") {
        let response = await api.request('/player/GetAll', {
            tags,
            states,
            cities,
            categories,
            search
        }, 'post', true, false);
        return response
    }

    async get(id) {
        return await api.request('/player/Get', { id }, 'post', false, false);
    }

}

export default new PlayerAPI