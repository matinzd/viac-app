import api from './BaseAdapter'
import { ToastAndroid } from 'react-native'
import { setStore, getStore } from 'trim-redux';
import ViacToast from '../controllers/ViacToast';

class AdvertisingAPI {

    /**
     * 
     * @param {FormData} formData
     */
    async insertUpdate(formData) {
        let response = await api.request('/advertising/InsertUpdate', formData, 'post', true, true);
        return response
    }


    async getStatics() {
        let response = await api.request('/advertising/GetStatics', null, 'post', true, false);
        return response
    }

    /**
     * 
     * @param {String} parentId
     */
    async getAll(tags = getStore('selectedTags'), states = getStore('selectedStates'), cities = getStore('selectedStates'), categories = getStore('selectedCategories'), search = getStore('textSearch')) {
        let response = await api.request('/advertising/GetAll', {
            tags,
            states,
            cities,
            categories,
            search
        }, 'post', true, false);
        setStore({ advertisements: response.data.data })
    }

    async get(id) {
        let token = getStore('userInfo').token
        return await api.request('/advertising/Get', { id, token }, 'post', true, false);
    }

    async delete(id) {
        return await api.request('/advertising/Delete', { id, isRemove: true }, 'post', true, false);
    }

    async getUserAds() {
        return await api.request('/advertising/GetMine', null, 'post', true, false);
    }
    async getUserBookmarks() {
        return await api.request('/advertising/GetBookMark', null, 'post', true, false);
    }

    async toggleLike(advertisingId) {
        if (!getStore('userInfo').isUserLoggedIn) {
            ViacToast.showToast('لطفا برای نشان کردن آگهی به حساب کاربری خود وارد شوید', 2000)
            return;
        }
        return await api.request('/advertising/ToggleLike', { advertisingId }, 'post', true, false)
    }

    async getBookmarks() {
        return await api.request('/advertising/GetBookMark', null, 'post', true, false)
    }

    async sendWarning(advertisingId, message) {
        return await api.request('/advertising/SendWarning', { advertisingId, message }, 'post', true, false)
    }

    async updateForFifthLadder(formData) {
        return await api.request('/advertising/updateP5', formData, 'post', true, true)
    }

}

export default new AdvertisingAPI