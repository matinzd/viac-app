import api from './BaseAdapter'
import { setStore, getStore } from 'trim-redux';

class ResumeAPI {

    
    /**
     * @param {number} type
     * @param {*} ages 
     * @param {*} categoryId 
     * @param {*} coach 
     * @param {*} sportType 
     * @param {*} experience 
     * @param {*} placeName 
     * @param {*} matchType 
     * @param {*} height 
     * @param {*} weight 
     * @param {*} coachDegree 
     * @param {Object} contact 
     * @param {Object[]} honor 
     */
    async insertUpdate(type, contact, honor, ages, categoryId, coach, sportType, experience, placeName, matchType, height, weight, coachDegree, degreeOfEducation, major, cityId, aboutMe, id) {
        return await api.request('/resume/InsertUpdate', {
            type,
            contact,
            honor,
            ages,
            categoryId,
            coach,
            sportType,
            experience,
            placeName,
            matchType,
            height,
            weight,
            coachDegree,
            degreeOfEducation,
            major,
            cityId,
            aboutMe,
            id
        }, 'post', true, false);
    }

    // /**
    //  * 
    //  * @param {String} parentId
    //  */
    // async getAll(tags = [], states = [], cities = [], categories = [], search = "") {
    //     let response = await api.request('/player/GetAll', {
    //         tags,
    //         states,
    //         cities,
    //         categories,
    //         search
    //     }, 'post', true, false);
    //     return response
    // }

    async getMine() {
        return await api.request('/resume/GetMine', null, 'post', true, false);
    }

    async getEnum() {
        let result = await api.request('/resume/GetEnum', null, 'post', false, false);
        setStore({ enums: result.data.data })
    }

    async getAll(type, search) {
        return await api.request('/resume/GetAll', { type, search }, 'post', false, false);
    }

    async get(id) {
        return await api.request('/resume/Get', { id }, 'post', false, false);
    }

    async delete(id) {
        return await api.request('/resume/Delete', { id }, 'post', true, false);
    }

}

export default new ResumeAPI