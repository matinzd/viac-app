import api from './BaseAdapter'
import { setStore, getStore } from 'trim-redux';

class VersionAPI {

    /**
     * 
     * @param {String} versionCode
     */
    async get(versionCode) {
        return await api.request('/version/Get', { versionCode }, 'post', false);
    }

    

}

export default new VersionAPI