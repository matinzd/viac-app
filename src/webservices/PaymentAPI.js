import api from './BaseAdapter'
import { setStore, getStore } from 'trim-redux';

class PaymentAPI {

    /**
     * 
     * @param {String} advertisingId 
     * @param {String} ladderId 
     * @param {Object} cafeBazaar 
     * @param {boolean} zarinPal 
     */
    async submitLadder(advertisingId, ladderId, cafeBazzar, zarinPal) {
        return await api.request('/payment/submitLadder', {
            advertisingId,
            ladderId,
            cafeBazzar,
            zarinPal
        }, 'post', true);
    }

    async send() {
        await fetch("https://api.themoviedb.org/3/movie/now_playing?api_key=55957fcf3ba81b137f8fc01ac5a31fb5&language=en-US")
            .then(response => response.json())
            .then((responseJson) => {
                return response.json()
                // this.setState({
                //     loading: false,
                //     dataSource: responseJson.results
                // })
            })
            .catch(error => console.log(error))
    }
}

const mapStateToProps = state => ({ getMovieTree: state.getMovieTree })

const mapDispatchToProps = dispatch => {
    return {
        getMovieTree: () => dispatch({}) ///your desired action
    }
}

// getMovieTree
// export default connect(mapStateToProps, mapDispatchToProps)(App);



export default new PaymentAPI