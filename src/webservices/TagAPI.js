import api from './BaseAdapter'
import { setStore, getStore } from 'trim-redux';

class CategoryAPI {

    /**
     * 
     * @param {String} parentId
     */
    async getAll() {
        return await api.request('/tag/GetAll', null, 'post', false);
    }

    async getRandom(){
        return await api.request('/tag/GetRandom', null, 'post', false)
    }

}

export default new CategoryAPI