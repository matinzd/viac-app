import axios, { CancelToken } from 'axios'

export const BASE_URL = 'https://api.viac-sport.com'
// export const BASE_URL = 'http://192.168.1.104:8000'
const TIMEOUT = 20000
const IS_DEBUG = true
import { setStore, getStore } from "trim-redux";
import Fonts from '../constants/Fonts';
import AsyncStorage from '@react-native-community/async-storage';
import NavigationController from '../controllers/NavigationController';

export const NO_INTERNET = "No intenet connection";
const instance = axios.create()

export default {

    async request(endpoint, body, method, needToken, formData = false) {
        let source;
        if (!formData) {
            body = JSON.stringify(body)

        }


        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }


        if (needToken)
            headers = {
                'Authorization': 'Bearer ' + getStore('userInfo').token,
                'Content-Type': formData ? 'multipart/form-data' : 'application/json',
                'Accept': 'application/json'
            }


        if (IS_DEBUG) {
            console.log('Request body: ', body)
            console.log(`Request info: ${BASE_URL}${endpoint} / ${method.toUpperCase()}`)
            console.log('Headers: ', headers)
        }



        let config = { headers: headers, method: method }
        if (method !== 'get') {
            config = { headers: headers, method: method, body: body }
        }

        return new Promise((resolve, reject) => {

            var timeout = setTimeout(() => reject({ message: 'Timeout' }), TIMEOUT)
            fetch(BASE_URL + endpoint, config)
                .then(response => {
                    clearTimeout(timeout)
                    if (response.status !== 200) {
                        reject({
                            statusCode: response.status,
                            statusText: response.statusText,
                            apiEndpoint: BASE_URL + endpoint,
                            method: method,
                            response: response,
                            // time: moment().format('LLLL')
                        })
                    }
                    return response.json()
                })
                .then(response => {
                    if (IS_DEBUG)
                        console.log(endpoint, { data: response })
                    resolve(
                        {
                            data: response
                        }
                    )
                })
                .catch(err => {
                    clearTimeout(timeout)
                    reject(err)
                })
        })

    },

    _handleError(statusCode, response) {
        if (statusCode) {
            console.warn('Error status code ', statusCode)
            setStore({ loading: false })
            if (statusCode === 401 || statusCode === 403) {
                console.warn('401 or 403 Unauthorized - clearing stroage .. ')
                setStore({
                    userInfo: {
                        isUserLoggedIn: false,
                        token: '',
                    }
                })
                AsyncStorage.clear().then(() => {
                    NavigationController.navigate('SplashScreen')
                })
                // this._showToast('لطفا دوباره وارد شوید', 'warning');
                return;
            }
            if (statusCode === 500) {
                console.warn('ERROR RESPONSE ', response)
                // this._showToast(response.error.message, 'warning');
            }
        }
    },

    // _showToast(message, type, position = 'top') {
    //     Toast.show({
    //         text: message,
    //         type: type,
    //         textStyle: {
    //             fontFamily: Fonts.IRANSansMobile,
    //             fontSize: 12
    //         },
    //         duration: 3000,
    //         position: position,
    //     })
    // },

    async getAuthToken() {
        return await AsyncStorage.getItem('userToken');
    },

    async hasAuthToken() {
        return await AsyncStorage.getItem('userToken') !== null
    },

    BaseUrl() {
        return BASE_URL;
    }
}