import { NavigationActions, StackActions, DrawerActions } from 'react-navigation';

let _navigator;

function setTopLevelNavigator(navigatorRef) {
    _navigator = navigatorRef;
}

function navigate(routeName, params) {
    _navigator.dispatch(
        NavigationActions.navigate({
            routeName,
            params,
        })
    );
}

function getCurrentRouteName(){
    let navIterator = _navigator.state.nav;
    while (navIterator.index != null) {
        navIterator = navIterator.routes[navIterator.index];
    }
    return navIterator.routeName
}

function reset(routeName, params){
    _navigator.dispatch(
        StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({routeName, params})
            ]
        })
    );
}

function openDrawer(){
    _navigator.dispatch(DrawerActions.openDrawer())
}

function closeDrawer(){
    _navigator.dispatch(DrawerActions.closeDrawer())
}

function hideTabBar(){
    _navigator.setParams({ tabBarVisible: false })
}

function showTabBar(){
    _navigator.setParams({ tabBarVisible: true })
}

function goBack(){
    _navigator.dispatch(
        NavigationActions.back()
    );
}

export default {
    navigate,
    hideTabBar,
    showTabBar,
    setTopLevelNavigator,
    goBack,
    reset,
    openDrawer,
    closeDrawer,
    getCurrentRouteName
}