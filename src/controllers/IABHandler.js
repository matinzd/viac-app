import { NativeModules } from 'react-native'

const { CafeBazaarIABHandler } = NativeModules;

const base64EncodedPublicKey = 'MIHNMA0GCSqGSIb3DQEBAQUAA4G7ADCBtwKBrwCloqvcleLPi3pLmD0R+2cxrtmrPH0KlXiv6NFguZJMaHAYQ19VKvuiym7ho9rj9YntkLGVxrxhh+effQDTWRHix/jMHy3S83kdLiO1fUB7ksulW94lKC7CCN8ghVSLpAfvBIu6VZlRqiE90oFbWaFKirVq6z5c4mjA2c0KkmFkjtVjDCX1QlSqF/MLSJarKvzfiQ51u21+a+jYcZscr91YFQjyUo/6NiFhu/Y8fqcCAwEAAQ=='

class IABHandler {

    /**
     * Cafe bazaar IAB start payment
     * @param {String} SKU 
     * @param {number} RC_REQUEST 
     * @returns {Promise} Result of transaction or error message
     */
    payByCafeBazaar(SKU, RC_REQUEST) {
        return new Promise((resolve, reject) => {
            CafeBazaarIABHandler.startPayment(base64EncodedPublicKey, SKU, RC_REQUEST)
                .then(result => resolve(result))
                .catch(err => reject(err))
        })
    }

    consumeProduct(SKU) {
        return new Promise((resolve, reject) => {
            CafeBazaarIABHandler.consume(SKU)
                .then(result => resolve(result))
                .catch(err => reject(err))
        })
    }

    close() {
        CafeBazaarIABHandler.close()
    }
}

export default new IABHandler()