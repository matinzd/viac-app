function minmizeText(_text, _len) {
    return _text.length > _len ? _text.substring(0,_len)+" ..." : _text
}

export{
    minmizeText
}

