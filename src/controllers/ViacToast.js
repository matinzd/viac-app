let _toast;

function setToastRef(toastRef) {
    _toast = toastRef;
}

function showToast(text, duration){
    _toast.showToast(text, duration)
}

export default {
    setToastRef,
    showToast
}