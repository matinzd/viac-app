import React, { Component } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { View, Text, StyleSheet, FlatList, BackHandler, StatusBar } from 'react-native'
import IIcons from 'react-native-vector-icons/Ionicons';
import { TextField } from 'react-native-material-textfield';
import Colors from '../constants/Colors';
import { IranSansMobile } from '../constants/Fonts';
import Ripple from 'react-native-material-ripple';
import states from '../constants/data/states.json'
import * as Animatable from 'react-native-animatable'
import _ from 'lodash'
import Modal from 'react-native-modal'
import { connect, getStore, setStore } from 'trim-redux';
import RF from 'react-native-responsive-fontsize';

class StateModal extends Component {

    constructor(props) {
        super(props)
        // this.state = {
        // index: 0,
        // routes: [
        // { key: 'sort', title: 'مرتب سازی' },
        //     { key: 'filter', title: 'فیلتر کردن' },
        // ],
        // isTabBarVisible: true

        // };

        this.cities = _.filter(states, { parentId: null });
        this.all = states
        this.state = {
            selectedCityId: null,
            data: this.cities,
            tempData: this.cities
        }
    }

    componentWillMount() {

    }

    onCitySelected(id) {
        if (this.state.selectedCityId === null) {
            this.setState({
                selectedCityId: id
            })
            let newData = _.filter(this.all, (o) => {
                if (o.parentId !== null && o.parentId === id) {
                    console.log(o)
                    return o
                }
            })
            this.setState({
                data: newData,
                tempData: newData
            })
        } else {
            let cityName = _.find(this.all, { id: this.state.selectedCityId }).stateName;
            let stateName = _.find(this.all, { id: id }).stateName;
            setStore({
                isStateSelected: true,
                selectedStateId: id,
                selectedStateString: `${cityName} > ${stateName}`,
                isStateModalVisible: false
            })
        }
    }

    onSearchText(searchText) {
        let searchData = _.filter(this.state.tempData, (o) => {
            if (o.stateName.includes(searchText)) {
                return o
            }
        })
        this.setState({
            data: searchData
        })
    }

    render() {
        return (
            <Modal isVisible={getStore('isStateModalVisible')} useNativeDriver={true} style={{ borderColor: Colors.SECONDARY, borderWidth: wp('1%'), width: wp('70%'), height: hp('70%'), justifyContent: 'center', backgroundColor: Colors.WHITE, alignSelf: 'center' }}
                onBackdropPress={() => {
                    if (this.state.selectedCityId !== null) {
                        this.setState({
                            data: this.cities,
                            tempData: this.cities,
                            selectedCityId: null
                        })
                    } else {
                        setStore({ isStateModalVisible: false })
                    }
                }}
                onBackButtonPress={() => {
                    if (this.state.selectedCityId !== null) {
                        this.setState({
                            data: this.cities,
                            tempData: this.cities,
                            selectedCityId: null
                        })
                    } else {
                        setStore({ isStateModalVisible: false })
                    }
                }} >
                <StatusBar backgroundColor="rgba(0,0,0,0.7)" barStyle="light-content" />
                <View style={{ marginHorizontal: wp('2%') }}>
                    <TextField onChangeText={searchText => this.onSearchText(searchText)} label='جست و جو ...' selectionColor={Colors.PRIMARY} labelTextStyle={{ fontFamily: IranSansMobile, fontSize: 9 }} style={{ fontFamily: IranSansMobile }} />
                </View>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    style={{ padding: wp('3%') }}
                    data={this.state.data}
                    ListFooterComponent={() => (
                        <View style={{ margin: wp('2%'), borderColor: Colors.PRIMARY }}>
                        </View>
                    )}
                    renderItem={({ item }) => (
                        <Ripple onPress={() => this.onCitySelected(item.id)} style={{ marginTop: wp('2%'), borderWidth: 2, borderRadius: 3, borderColor: Colors.PRIMARY }}>
                            <View style={{ backgroundColor: Colors.WHITE, marginHorizontal: wp('5%'), flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                <Text style={{ fontSize: RF(2.5), fontFamily: IranSansMobile, color: Colors.PRIMARY }}>{item.stateName}</Text>
                                {
                                    this.state.selectedCityId === null ?
                                        <IIcons name="ios-arrow-back" size={15} color={Colors.PRIMARY} />
                                        :
                                        null
                                }
                            </View>
                        </Ripple>
                    )}
                />

            </Modal>

        )
    }

}

const mstp = state => ({ isStateModalVisible: state.isStateModalVisible })
export default connect(mstp)(StateModal)