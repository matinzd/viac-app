import React, { Component } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { View, Text, Image, StatusBar } from 'react-native'
import Ripple from 'react-native-material-ripple';
import MapView, { Marker, MapViewAnimated } from 'react-native-maps'
import _ from 'lodash'
import RF from "react-native-responsive-fontsize"
import { setStore, getStore, connect } from 'trim-redux';
import Modal from 'react-native-modal';
import Colors from '../constants/Colors';
import { IranSansMobile } from '../constants/Fonts';

class LocationModal extends Component {

    constructor(props) {
        super(props)

        this.state = {
            longitude: 51.6798700,
            longitudeDelta: 0.01,
            latitudeDelta: 0.01,
            latitude: 32.647100,
            markerLatitude: 32.647100,
            markerLongitude: 51.6798700
        }
    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.isLocationModalVisible) {
            setTimeout(() => {
                this.handleLocation()
            }, 2000)
        }
    }

    handleLocation() {
        let location = getStore('location');
        if (location.latitude === 0 && location.longitude === 0) {
            navigator.geolocation.getCurrentPosition((pos) => {
                console.log('Position ', pos)
                this.setState({
                    latitude: pos.coords.latitude,
                    markerLatitude: pos.coords.latitude,
                    markerLongitude: pos.coords.longitude,
                    longitude: pos.coords.longitude,
                })
                this.mapViewRef.animateCamera({ center: { longitude: pos.coords.longitude, latitude: pos.coords.latitude } }, {
                    duration: 1000
                })
            }, (err) => console.log('Error ', err), { timeout: 10 })
        } else {
            this.setState({
                latitude: location.latitude,
                markerLatitude: location.latitude,
                markerLongitude: location.longitude,
                longitude: location.longitude,
            })
            this.mapViewRef.animateCamera({ center: { longitude: location.longitude, latitude: location.latitude } }, {
                duration: 1000
            })
        }
    }

    handleMapEvent(event) {
        console.log('Map event ', event.nativeEvent)
        this.setState({
            markerLongitude: event.nativeEvent.coordinate.longitude,
            markerLatitude: event.nativeEvent.coordinate.latitude,
            latitude: event.nativeEvent.coordinate.latitude,
            longitude: event.nativeEvent.coordinate.longitude,
            latitudeDelta: this.state.latitudeDelta,
            longitudeDelta: this.state.longitudeDelta
        })
        this.mapViewRef.animateCamera(
            {
                center: { longitude: event.nativeEvent.coordinate.longitude, latitude: event.nativeEvent.coordinate.latitude },
                zoom: 17
            }, {
                duration: 1000
            })
        this.setLocation()
    }

    setLocation() {
        setStore({
            location: {
                latitude: this.state.markerLatitude,
                longitude: this.state.markerLongitude,
            },
        })
    }

    render() {
        return (

            <View style={{ justifyContent: 'center', marginVertical: wp('3%') }}>
                <View style={{ width: '100%', alignSelf: 'center', height: hp('40%'), borderWidth: 1, borderColor: Colors.SECONDARY, overflow: 'hidden'}}>
                    <MapView
                        showsUserLocation={true}
                        showsMyLocationButton={true}
                        onRegionChangeComplete={(regionData) => {
                            this.setState({
                                longitudeDelta: regionData.longitudeDelta,
                                latitudeDelta: regionData.latitudeDelta,
                                longitude: regionData.longitude,
                                latitude: regionData.latitude,
                            })
                        }}
                        style={{ width: '100%', height: hp('40%') }}
                        ref={mapView => this.mapViewRef = mapView}
                        showsCompass={false}
                        pitchEnabled={false}
                        onPress={(event) => this.handleMapEvent(event)}
                        initialRegion={{ longitude: 51.6798666, longitudeDelta: 0.01, latitudeDelta: 0.01, latitude: 32.647695 }}
                    // region={{ latitude: this.state.latitude, longitude: this.state.longitude, longitudeDelta: this.state.longitudeDelta, latitudeDelta: this.state.latitudeDelta }}
                    >
                        <Marker
                            // image={require('../assets/images/viac_marker.png')}
                            // icon={}
                            draggable
                            coordinate={{
                                longitude: this.state.markerLongitude,
                                longitudeDelta: 0.01,
                                latitudeDelta: 0.01,
                                latitude: this.state.markerLatitude
                            }}>
                            {/* <Image source={require('../assets/images/viac_marker.png')} 
                            style={{ width: wp('10%'), height: wp('12%') }} /> */}
                        </Marker>
                    </MapView>
                </View>

            </View>
        )
    }

}

const mstp = state => ({ isLocationModalVisible: state.isLocationModalVisible })
export default connect(mstp)(LocationModal)
