import React, { Component } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { View, Text, Image, I18nManager, StyleSheet, Modal, ScrollView } from 'react-native'
import Ripple from 'react-native-material-ripple';
import MapView, { Marker, MapViewAnimated } from 'react-native-maps'
import _ from 'lodash'
import RF from "react-native-responsive-fontsize"
import { setStore, getStore, connect } from 'trim-redux';
// import Modal from 'react-native-modal';
import { Toolbar } from 'react-native-material-ui'
import { Statusbar } from '../components/viac';
import Colors from '../constants/Colors';
import Color from '../styles/Color';
import { IranSansMobile, IranSansMobileBold } from '../constants/Fonts';
import text from './terms.json'

class Terms extends Component {

    constructor(props) {
        super(props)

        this.state = {
            rulesText: '',
            warningText: '',
            safetyText: ''
        }
    }

    componentDidMount() {

    }

    render() {
        return (
            <Modal visible={getStore('isTermsVisible')} style={{ backgroundColor: '#FFF', margin: 0 }}  >
                <Statusbar light={false} backgroundColor={Colors.WHITE} />
                <View style={styles.container}>
                    <Toolbar
                        ref={ref => this.toolbar = ref}
                        leftElement={I18nManager.isRTL ? "arrow-forward" : "arrow-back"}
                        centerElement="قوانین ویاک"
                        onLeftElementPress={() => {
                            setStore({ isTermsVisible: false })
                        }}
                    />
                    <ScrollView style={styles.textContainer}>
                        <Text style={styles.textTitleStyle}>متن قوانین : </Text>
                        <Text style={styles.textContentStyle}>{text.terms}</Text>
                        {/* <Text style={styles.textTitleStyle}>هشدار ها : </Text>
                        <Text style={styles.textContentStyle}>{this.state.warningText}</Text> */}
                        <Text style={styles.textTitleStyle}>امنیت : </Text>
                        <Text style={styles.textContentStyle}>{text.security}</Text>
                    </ScrollView>
                    <View style={styles.bottomContainer}>
                        <Ripple onPress={() => setStore({ isTermsVisible: false })} style={{ backgroundColor: Colors.WHITE, justifyContent: 'center', borderColor: Color.BUTTON_ACCEPT, borderWidth: 1, borderRadius: wp('1%'), marginHorizontal:4, paddingHorizontal:8 }}>
                            <Text style={[styles.textContentStyle, { color: Color.BUTTON_ACCEPT }]}>موافق نیستم</Text>
                        </Ripple>
                        <Ripple onPress={() => { this.props.onAcceptTerms() }} style={{ backgroundColor: Color.BUTTON_ACCEPT, borderRadius: wp('1%'), justifyContent: 'center', marginHorizontal:4, paddingHorizontal:8}}>
                            <Text style={[styles.textContentStyle, { color: Colors.WHITE }]}>با قوانین موافقم</Text>
                        </Ripple>
                    </View>
                </View>
            </Modal>
        )
    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    textContainer: {
        padding: wp('5%'),
        marginBottom: hp('10%')
    },
    bottomContainer: {
        height: 48,
        bottom: 0,
        position: 'absolute',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        borderTopColor: Colors.PRIMARY,
        borderTopWidth:1,
        paddingVertical:4,
    },
    textContentStyle: {
        fontFamily: IranSansMobile,
        color: Colors.PRIMARY,
        fontSize: RF(2),
    },
    textTitleStyle: {
        fontFamily: IranSansMobileBold,
        color: Colors.PRIMARY,
        fontSize: RF(2.5),
    }
})

const mstp = state => ({ isTermsVisible: state.isTermsVisible })
export default connect(mstp)(Terms)