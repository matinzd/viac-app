const PRIMARY = "#263238";
const LIGHTENPRIMARY = "rgb(215, 215, 215)";
const SECONDARY = '#285483'
const LIGHTBLUE = '#139fdd'
const LIGHTGREY = '#cccccc'
const ORANGETAG = '#F4511E'
// const SECONDARY = "rgb(139, 219, 193)";
const WHITE = "#FFFFFF";
const LIGHT = "rgb(247, 247, 247)";
const MID = "rgb(215, 215, 215)";
const DARK = "rgb(60, 60, 60)";
const Button = "#f7c744";
const Background = "#1A1C20";
const DarkGray = "#30455a";
const RED = "#f04";
const Green = "#00ADB5";
const Border = "#E0E0E0";
const BlueSky = "#039be5";
const Dote = "#B1B1B1";
const Grays = "#B0BEC5";
const Black = "#000000";
const ColorPrimary = "#02ab80";
const ColorPrimaryDark = "#13bf81";
const ColorAccent = "#13bf81";

export default {
    PRIMARY,
    LIGHTENPRIMARY,
    SECONDARY,
    ORANGETAG,
    LIGHTBLUE,
    LIGHTGREY,
    WHITE,
    LIGHT,
    MID,
    DARK,
    RED,
    Button,
    Background,
    DarkGray,
    Green,
    Border,
    BlueSky,
    Dote,
    Grays,
    Black,
    ColorPrimary,
    ColorPrimaryDark,
    ColorAccent
};
