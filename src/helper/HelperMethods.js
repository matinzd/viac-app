import { Dimensions } from 'react-native';

const DEBUG = true
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

export function log( _txt='',_tag='DEBUG_MODE',)
{
  if(DEBUG)
    console.log(_tag, _txt)
}
export function toLocaleString(price) {
    var number = String(price);
    for(i=number.length; i - 3 > 0; i = i - 3){
      number = [number.slice(0, i - 3), ",", number.slice(i - 3)].join('');
    }
    return number;
  }

 export function wp (percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}

export function hp (percentage) {
  const value = (percentage * viewportHeight) / 100;
  return Math.round(value);
}