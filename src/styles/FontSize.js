import RF from "react-native-responsive-fontsize"
export const HEADER =RF(3.8) ;
export const TITLE = RF(3.3);
export const SUBTITLE = RF(2.8);
export const CONTENT = RF(2.3);
export const SMAL = RF(1.8);
export const MEDIUM = RF(2)
export const BUTTON = RF(2.8);

export default {
    HEADER,
    TITLE,
    SUBTITLE,
    CONTENT,
    MEDIUM,
    SMAL,
    BUTTON
};