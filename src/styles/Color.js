export const STATUSBAR = "#00D9CF";
export const TOOLBAR_ICON = "#FFFFFF";
export const TOOLBAR_TITLE = "#FFFFFF";
export const TOOLBAR_SUBTITLE = "#FFFFFF";
export const TITLE = "#949494";
export const TITLE_NAFAS = "#03706B";
export const SUBTITLE = "#9E9E9E";
export const CONTENT = "#ABABAB";
export const BUTTON = "#FFFFFF";
export const HEIGHLIGHT = "#FB5386";
export const GRADIENT_NAFAS_START= "#00CCCC";
export const GRADIENT_NAFAS_END = "#25A7AA";
export const GRADIENT_TANIN_HEADER_START = "#791CBF";
export const GRADIENT_TANIN_HEADER_END = "#00CCCC";
export const GRADIENT_TANIN_START = "#90E673";
export const GRADIENT_TANIN_END = "#00CCCC";
export const BACKGROUND = "#FFFFFF";
export const BACKGROUND_TANIN_NAFAS = "#F2F4F7";
export const BACKGROUND_NAFAS = "#00D9CF";
export const SEARCH_BACKGROUND = "#F8F8F8";
export const LOADING = "#00CCCC";
export const SHADOW_COLOR = "#000";
export const SLIDER_MIN_TRACK = "#00CCCC";
export const SLIDER_THUMB = "#FFD500";
export const BORDER = "#E4E4E4";
export const GRADIENT_COMPETITION_START= "#00D3FF";
export const GRADIENT_COMPETITION_END= "#BC13D1";
export const GRADIENT_HOME_START= "#ef68f2";
export const GRADIENT_HOME_END= "#ac07af";
export const CONTENT_HOME_CELL= "#5D5D5D";
export const LOGOUT= "#FFFF00";
export const INDICATOR= "#00D9CF";
export const LOGO_TINTCOLOR= "#00D9CF";
export const DOWNLOAD_FAILED= "#F50043";
export const SEARCH_HEIGHLIGHT= "#00D9CF";
export const BACKGRAOUND_RELATED= '#FFB507';
export const APP_TOUR_OUTER_CIRCLE= '#984FEC';
export const APP_TOUR_TITLE_CONTENT= '#FFFFFF';
export const BACKGROUND_GROUP_CONTAINER= '#F1F1F1';
export const NARRATED_GROUP_CONTAINER= '#F2CA85';
export const HEIGHLIGHT_NAFAS= '#59C833';
export const BUTTON_ACCEPT = '#007BFF'

export default {
    STATUSBAR,
    TOOLBAR_ICON,
    TOOLBAR_TITLE,
    TOOLBAR_SUBTITLE,
    TITLE,
    SUBTITLE,
    CONTENT,
    BUTTON,
    HEIGHLIGHT,
    HEIGHLIGHT_NAFAS,
    GRADIENT_NAFAS_START,
    GRADIENT_NAFAS_END,
    GRADIENT_TANIN_HEADER_START,
    GRADIENT_TANIN_HEADER_END,
    GRADIENT_TANIN_START,
    GRADIENT_TANIN_END,
    BACKGROUND,
    BACKGROUND_TANIN_NAFAS,
    BACKGROUND_NAFAS,
    BACKGRAOUND_RELATED,
    BACKGROUND_GROUP_CONTAINER,
    LOADING,
    SHADOW_COLOR,
    SLIDER_MIN_TRACK,
    SLIDER_THUMB,
    BORDER,
    GRADIENT_COMPETITION_START,
    GRADIENT_COMPETITION_END,
    TITLE_NAFAS,
    GRADIENT_HOME_START,
    GRADIENT_HOME_END,
    CONTENT_HOME_CELL,
    LOGOUT,
    INDICATOR,
    LOGO_TINTCOLOR,
    DOWNLOAD_FAILED,
    SEARCH_HEIGHLIGHT,
    SEARCH_BACKGROUND,
    APP_TOUR_OUTER_CIRCLE,
    APP_TOUR_TITLE_CONTENT,
    NARRATED_GROUP_CONTAINER,
    BUTTON_ACCEPT
};