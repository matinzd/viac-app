export const HEADER ='IRANYekanMobileFNBold' ;
export const TITLE = 'IRANYekanMobileFNBold';
export const SUBTITLE = 'IRANYekanMobileFNMedium';
export const CONTENT = 'IRANYekanMobileFNMedium';
export const BUTTON = 'IRANYekanMobileFNBold';

export default {
    HEADER,
    TITLE,
    SUBTITLE,
    CONTENT,
    BUTTON
};