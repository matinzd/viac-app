import React, { Component } from 'react'
import { View, Image, Text, NativeModules, BackHandler, Linking } from 'react-native'
import { Statusbar } from '../../components/viac';
import Colors from '../../constants/Colors';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import { IranSansMobile } from '../../constants/Fonts';
import Ripple from 'react-native-material-ripple';
import VersionNumber from 'react-native-version-number';
import RNFS from 'react-native-fs'
import NavigationController from '../../controllers/NavigationController';
import RF from 'react-native-responsive-fontsize';


const { UpdateHandler } = NativeModules;

export default class UpdateScreen extends Component {


    constructor(props) {
        super(props)
        this.state = {
            downloadProgress: 0,
            downloading: false,
            isDownloaded: false,
            isForce: this.props.navigation.getParam('isForce') === 0 ? false : true,
            logs: this.props.navigation.getParam('logs'),
        }
    }

    componentDidMount() {
        this.updateUI()
    }

    updateUI() {
        // RNFS.stat(RNFS.ExternalCachesDirectoryPath + '/Viac.apk')
        //     .then(result => {
        //         console.log(result)
        //         if (result.isFile) {
        //             this.setState({
        //                 isDownloaded: true
        //             })
        //         }
        //     })
        //     .catch(err => console.log(err))
    }

    download() {
        // let uri = RNFS.ExternalCachesDirectoryPath + '/Viac.apk'
        // let url = this.props.navigation.getParam('url')
        // console.log(uri)
        // console.log(url)
        // this.jobId = null
        // this.isDownloaded = false
        // this.setState({
        //     downloading: true,
        //     isDownloaded: false
        // })
        // RNFS.downloadFile({
        //     background: true,
        //     fromUrl: url,
        //     toFile: uri,
        //     progress: (progress) => {
        //         let percentage = Math.trunc(progress.bytesWritten / progress.contentLength * 100)
        //         this.jobId = progress.jobId
        //         if (percentage !== this.state.downloadProgress) {
        //             this.setState({
        //                 downloadProgress: percentage
        //             })
        //         }
        //     }
        // })
        //     .promise.then((result) => {
        //         this.setState({
        //             downloading: false,
        //             isDownloaded: true
        //         })
        //         UpdateHandler.openInstaller(RNFS.ExternalCachesDirectoryPath, 'Viac.apk')
        //     })
        Linking.openURL(this.props.navigation.getParam('url'))
    }

    pause() {
        RNFS.stopDownload(this.jobId)
    }

    install() {
        UpdateHandler.openInstaller(RNFS.ExternalCachesDirectoryPath, 'Viac.apk')
    }

    skip() {
        NavigationController.reset('Tabs')
    }

    exitApp() {
        BackHandler.exitApp()
    }

    render() {
        return (
            <View style={{ backgroundColor: Colors.PRIMARY, flex: 1, justifyContent: 'center' }}>
                <Statusbar light={true} backgroundColor={Colors.PRIMARY} />
                <Image
                    source={require('../../assets/images/logo.png')}
                    style={{
                        width: wp('55%'),
                        height: wp('55%'),
                        alignSelf: 'center'
                    }} />
                <Text style={{ alignSelf: 'center', color: Colors.WHITE, fontFamily: IranSansMobile, fontSize: RF(2.5) }}>بروزرسانی جدید موجود است</Text>
                <Text style={{ alignSelf: 'center', color: Colors.WHITE, fontFamily: IranSansMobile, fontSize: RF(2) }}>{this.state.logs}</Text>
                {
                    this.state.isForce ?
                        <Text style={{ alignSelf: 'center', color: Colors.RED, fontFamily: IranSansMobile }}>اجباری!</Text>
                        :
                        null
                }
                {
                    this.state.isDownloaded ?
                        <Ripple
                            onPress={() => this.install()}
                            style={{ alignSelf: 'center', backgroundColor: Colors.SECONDARY, marginTop: wp('3%'), paddingHorizontal: wp('30%'), paddingVertical: wp('2%'), borderRadius: wp('2%'), }}>
                            <Text style={{ alignSelf: 'center', color: Colors.WHITE, fontFamily: IranSansMobile }}>نصب</Text>

                        </Ripple>
                        :
                        <Ripple
                            disabled={this.state.downloading}
                            onPress={() => this.download()}
                            style={{ alignSelf: 'center', backgroundColor: Colors.SECONDARY, marginTop: wp('3%'), paddingHorizontal: wp('30%'), paddingVertical: wp('2%'), borderRadius: wp('2%'), }}>
                            <Text style={{ alignSelf: 'center', color: Colors.WHITE, fontFamily: IranSansMobile }}>
                                {
                                    this.state.downloading ?
                                        'در حال دانلود'
                                        :
                                        'دانلود از کافه بازار'
                                }
                            </Text>

                        </Ripple>
                }
                {
                    this.state.downloading ?
                        <View
                            style={{ alignSelf: 'center', marginTop: wp('3%'), paddingHorizontal: wp('30%'), paddingVertical: wp('2%'), borderRadius: wp('2%'), }}
                        >
                            <Text style={{ alignSelf: 'center', color: '#FFB822', fontFamily: IranSansMobile }}>{this.state.downloadProgress} %</Text>
                        </View>
                        :
                        null
                }

                {
                    !this.state.isForce ?
                        <Ripple
                            onPress={() => this.skip()}
                            style={{ alignSelf: 'center', backgroundColor: Colors.SECONDARY, marginTop: wp('3%'), paddingHorizontal: wp('30%'), paddingVertical: wp('2%'), borderRadius: wp('2%'), }}>
                            <Text style={{ alignSelf: 'center', color: Colors.WHITE, fontFamily: IranSansMobile }}>بعدا نصب می کنم</Text>
                        </Ripple>
                        :
                        <Ripple
                            onPress={() => this.exitApp()}
                            style={{ alignSelf: 'center', backgroundColor: Colors.SECONDARY, marginTop: wp('3%'), paddingHorizontal: wp('30%'), paddingVertical: wp('2%'), borderRadius: wp('2%'), }}>
                            <Text style={{ alignSelf: 'center', color: Colors.WHITE, fontFamily: IranSansMobile }}>خروج</Text>
                        </Ripple>
                }

            </View>
        )
    }
}
