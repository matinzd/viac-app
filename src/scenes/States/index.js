import React, { Component } from 'react';
import { View, StyleSheet, Text, TextInput, FlatList, I18nManager } from 'react-native'
import _ from 'lodash'
import states from '../../constants/data/states.json'
import { Toolbar } from 'react-native-material-ui'
import { Statusbar } from '../../components/viac';
import NavigationController from '../../controllers/NavigationController.js';
import Colors from '../../constants/Colors.js';
import Ripple from 'react-native-material-ripple'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { IranYekanMobile } from '../../constants/Fonts.js';

export default class States extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: _.filter(states, { parentId: null }),
            temp: _.filter(states, { parentId: null })
        }
    }

    onSubmitEditing = () => {

    }

    onSearchClicked = () => {

    }

    onSearchTextChanged = (text) => {
        let temp = _.filter(this.state.temp, (o) => {
            if (o.stateName.search(text) >= 0) {
                return o
            }
        })
        this.setState({
            data: temp
        })
    }

    render() {
        return (
            <View style={styles.container} >
                <Statusbar light={true} backgroundColor={Colors.PRIMARY} />
                <Toolbar
                    ref={ref => this.toolbar = ref}
                    leftElement={I18nManager.isRTL ? "arrow-forward" : "arrow-back"}
                    searchable={{
                        autoFocus: true,
                        placeholder: 'جست و جو ...',
                        onSubmitEditing: this.onSubmitEditing.bind(this),
                        onSearchPressed: this.onSearchClicked.bind(this),
                        onChangeText: this.onSearchTextChanged.bind(this),
                    }}
                    centerElement="انتخاب شهر"
                    onLeftElementPress={() => {
                        NavigationController.goBack()
                    }}
                    onRightElementPress={(label) => { NavigationController.openDrawer() }}
                />
                <FlatList
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    data={this.state.data}
                    renderItem={({ item }) => (
                        <Ripple style={styles.itemContainer} >
                            <Text style={styles.textStyle} >{item.stateName}</Text>
                        </Ripple>
                    )}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '',
    },
    itemContainer: {
        paddingVertical: wp('3%'),
        paddingHorizontal: wp('5%'),
        marginVertical: wp('1%'),
        marginHorizontal: wp('3%'),
        elevation: 3,
        backgroundColor: Colors.WHITE
    },
    textStyle: {
        fontFamily: IranYekanMobile,
        color: Colors.PRIMARY
    }
})