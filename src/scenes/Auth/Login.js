import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Keyboard, ToastAndroid, ActivityIndicator } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { IranSansMobile, IranSansMobileBold } from '../../constants/Fonts';
import { TextField } from 'react-native-material-textfield';
import AsyncStorage from '@react-native-community/async-storage'
import Ripple from 'react-native-material-ripple';
import * as Animatable from 'react-native-animatable';
import NavigationController from '../../controllers/NavigationController';
import Colors from '../../constants/Colors';
import UserAPI from '../../webservices/UserAPI';
import { Statusbar } from '../../components/viac';
import ViacToast from '../../controllers/ViacToast';


const mobileNumberRegex = /^0?9[0-9]{9}$/
const verificationCodeRegex = /^[0-9]{5}$/
const TIMER = 120
class Login extends Component {


    constructor(props) {
        super(props)
        this.state = {
            mobileNumberError: '',
            mobileNumber: '',
            ismobileNumberValid: false,
            isVerificationCodeValid: false,
            verificationCodeVisible: false,
            verificationCode: '',
            buttonText: 'دریافت کد فعال سازی',
            timer: TIMER,
            verificationCodeError: '',
            authState: '',
            isLoading: false
        }
    }

    componentDidMount() {

    }

    componentWillUnmount() {
        clearInterval(this.verificationCodeTimer)
    }

    isMobileNumberValid(mobileNumber) {
        this.setState({ mobileNumber })
        if (!mobileNumberRegex.test(mobileNumber) && mobileNumber !== '') {
            this.setState({
                mobileNumberError: 'فرمت شماره همراه صحیح نیست',
                ismobileNumberValid: false
            })
        } else {
            this.setState({
                mobileNumberError: '',
                ismobileNumberValid: false
            })
        }
        if (mobileNumberRegex.test(mobileNumber)) {
            this.setState({
                ismobileNumberValid: true,
            })
        }
    }

    isVerificationCodeValid(verificationCode) {
        this.setState({ verificationCode })
        this.setState({
            isVerificationCodeValid: verificationCodeRegex.test(verificationCode) ? true : false
        })
    }

    login() {
        if (!this.state.ismobileNumberValid) {
            this.mobileNumberShakeRef.shake(1000)
            return
        }
        if (!this.state.isVerificationCodeValid && this.state.authState !== '') {
            if (this.verificationCodeRef !== undefined) this.verificationCodeRef.shake(1000)
            return
        }
        if (this.state.authState === 'login') {
            this.setState({ isLoading: true })
            UserAPI.validate(this.state.mobileNumber, this.state.verificationCode)
                .then(async response => {
                    this.setState({ isLoading: false })
                    if (response.data.success) {
                        AsyncStorage.setItem('userToken', String(response.data.data.token)).then(() => {
                            NavigationController.navigate('SplashScreen')
                        })
                        ViacToast.showToast(response.data.message, 2000)
                    } else {
                        ViacToast.showToast(response.data.message, 2000)
                    }
                })
                .catch(({ serverMessage }) => {
                    this.setState({ isLoading: false })
                    ViacToast.showToast(serverMessage, 2000)
                })
        }
        

        if (this.state.authState === 'retry' || this.state.authState === '') {
            this.setState({ isLoading: true })
            UserAPI.signUp(this.state.mobileNumber).then(response => {
                this.setState({
                    verificationCodeVisible: true,
                    isLoading: false
                })
                if (response.data.success) {
                    this.setTimer()
                }
            })
        }
    }

    setTimer() {
        this.verificationCodeTimer = setInterval(() => {
            if (this.state.timer > 0) {
                this.setState({ timer: this.state.timer - 1 })
                this.setState({
                    verificationCodeError: `کد فعالسازی تا ${this.state.timer} ثانیه دیگر اعتبار دارد.`,
                    buttonText: 'ورود',
                    authState: 'login'
                })
            } else {
                clearInterval(this.verificationCodeTimer)
                this.setState({
                    verificationCodeError: '',
                    timer: TIMER,
                    buttonText: 'دریافت مجدد کد فعال سازی',
                    authState: 'retry'
                })
            }
        }, 1000)
    }

    render() {
        return (
            <View style={styles.inputContainer}>
                <Statusbar light={true} backgroundColor={Colors.PRIMARY} />
                <Animatable.View ref={ref => this.mobileNumberShakeRef = ref} useNativeDriver={true} >
                    <TextField
                        label='شماره همراه'
                        ref={ref => this.mobileNumberRef = ref}
                        error={this.state.mobileNumberError}
                        titleTextStyle={styles.inputLabelStyle}
                        keyboardType='phone-pad'
                        maxLength={11}
                        onChangeText={mobileNumber => this.isMobileNumberValid(mobileNumber)}
                        style={[styles.inputLabelStyle, {textAlign:'center'}]}
                        labelTextStyle={styles.inputLabelStyle}
                        onSubmitEditing={() => this.login()}
                        editable={this.state.authState === 'retry' || this.state.authState === 'login' ? false : true}
                    />
                </Animatable.View>

                {
                    this.state.verificationCodeVisible ?
                        <Animatable.View ref={ref => this.verificationCodeRef = ref} useNativeDriver={true} >
                            <TextField
                                errorColor='#49ade8'
                                titleTextStyle={styles.inputLabelStyle}
                                error={this.state.verificationCodeError}
                                label='کد فعالسازی'
                                maxLength={5}
                                keyboardType='decimal-pad'
                                onSubmitEditing={() => this.login()}
                                onChangeText={verificationCode => this.isVerificationCodeValid(verificationCode)}
                                labelTextStyle={styles.inputLabelStyle}
                                inputContainerStyle={{ direction: 'rtl' }}
                            />
                        </Animatable.View>
                        :
                        null
                }


                {
                    this.state.isLoading ?
                        <ActivityIndicator style={{ marginTop: hp('2%') }} />
                        :
                        <Ripple
                            style={styles.verifyButtonStyle}
                            onPress={() => this.login()}
                        >
                            <Text style={styles.buttonTextStyle}>{this.state.buttonText}</Text>
                        </Ripple>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    inputContainer: {
        marginHorizontal: wp('10%')
    },
    verifyButtonStyle: {
        backgroundColor: Colors.PRIMARY,
        justifyContent: 'center',
        height: hp('7%'),
        borderRadius: 5,
        marginTop: hp('5%'),
    },
    inputLabelStyle: {
        fontFamily: IranSansMobile,
    },
    buttonTextStyle: {
        color: Colors.WHITE,
        alignSelf: 'center',
        fontFamily: IranSansMobile,
    }
})

export default Login