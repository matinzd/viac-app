import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Keyboard,ImageBackground } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import Statusbar from '../Statusbar';
import { IranSansMobileBold } from '../../constants/Fonts';
import * as Animatable from 'react-native-animatable';
import Login from './Login';
import Colors from '../../constants/Colors';
class Authentication extends Component {


    constructor(props) {
        super(props)
        this.state = {
            title: 'ورود / ثبت نام',
            showLogo: true
        }
    }

    componentDidMount() {
        this.onKeyboardHide = Keyboard.addListener('keyboardDidHide', () => {
            this.setState({
                showLogo: true
            })
        })

        this.onKeyboardShow = Keyboard.addListener('keyboardDidShow', () => {
            this.setState({
                showLogo: false
            })
        })
    }

    componentWillUnmount() {
        this.onKeyboardHide.remove()
        this.onKeyboardShow.remove()
    }

    render() {
        return (
            // <ImageBackground source={require('../../assets/images/image_bg.png')} style={styles.container}></ImageBackground>
            <View style={styles.container}>
                {/* <Statusbar
                    backgroundColor={styles.container.backgroundColor}
                    light={true} /> */}
                <View style={styles.loginContainerParent}>
                    <Animatable.View
                        duration={1000}
                        easing="ease-in"
                        useNativeDriver={true}
                        animation='slideInDown'
                        style={styles.loginContainer} >
                        
                        <View style={styles.titleContainer}>
                            <Text style={styles.titleStyle}>{this.state.title}</Text>
                        </View>
                        <Login />
                        
                    </Animatable.View>
                </View>
                <Image source={require('../../assets/images/logo.png')} style={styles.image} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.PRIMARY,
        justifyContent: 'center'
    },
    loginContainerParent:{
        flex:.75,
        justifyContent:'center',
    },
    loginContainer: {
        backgroundColor: Colors.WHITE,
        alignSelf: 'center',
        width: wp('85%'),
        marginTop:hp('10%'),
        borderRadius: wp('1%'),
        paddingBottom:hp('2%')
    },
    titleContainer: {
        width: wp('40%'),
        height: hp('7%'),
        backgroundColor: Colors.WHITE,
        borderRadius: 5,
        alignSelf: 'center',
        marginTop: -hp('3.5%'),
        borderColor: '#263238',
        borderWidth: 2,
        justifyContent: 'center'
    },
    titleStyle: {
        alignSelf: 'center',
        fontFamily: IranSansMobileBold,
        color: Colors.PRIMARY
    },
    image: {
        alignSelf:'center',
        width: wp('30%'),
        height: wp('30%'),
        flex:.25
    }
})

export default Authentication