import React, { Component } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { View, Text, Image, I18nManager, StyleSheet } from 'react-native'
import Colors from '../constants/Colors';
import { IranSansMobile, IranSansMobileBold } from '../constants/Fonts';
import Ripple from 'react-native-material-ripple';
import MapView, { Marker, MapViewAnimated } from 'react-native-maps'
import _ from 'lodash'
import mapStyle from './test.json'
import RF from "react-native-responsive-fontsize"
import { setStore, getStore } from 'trim-redux';
import Modal from 'react-native-modal';
import { Statusbar } from '../components/viac';
import NavigationController from '../controllers/NavigationController';
import { Toolbar } from 'react-native-material-ui'
import Color from '../styles/Color';

export default class Test extends Component {

    constructor(props) {
        super(props)

        this.state = {
            rulesText: '',
            warningText: '',
            safetyText: ''
        }
    }

    componentDidMount() {

    }

    render() {
        return (
            <View style={styles.container}>
                <Statusbar light={false} backgroundColor={Colors.WHITE} />
                <Toolbar
                    ref={ref => this.toolbar = ref}
                    leftElement={I18nManager.isRTL ? "arrow-forward" : "arrow-back"}
                    centerElement="قوانین ویاک"
                    onLeftElementPress={() => {
                        NavigationController.goBack()
                    }}
                    onRightElementPress={(label) => { NavigationController.openDrawer() }}
                />
                <View style={styles.textContainer}>
                    <Text style={styles.textTitleStyle}>متن قوانین : </Text>
                    <Text style={styles.textContentStyle}>{this.state.rulesText}</Text>
                    <Text style={styles.textTitleStyle}>هشدار ها : </Text>
                    <Text style={styles.textContentStyle}>{this.state.warningText}</Text>
                    <Text style={styles.textTitleStyle}>امنیت : </Text>
                    <Text style={styles.textContentStyle}>{this.state.safetyText}</Text>
                </View>
                <View style={styles.bottomContainer}>
                    <Ripple style={{ backgroundColor: Colors.WHITE, justifyContent: 'center', borderColor: Color.BUTTON_ACCEPT, borderWidth: 1, borderRadius: wp('1%'), padding: 10 }}>
                        <Text style={[styles.textContentStyle, { color: Color.BUTTON_ACCEPT }]}>موافق نیستم</Text>
                    </Ripple>
                    <Ripple style={{ backgroundColor: Color.BUTTON_ACCEPT, borderRadius: wp('1%'), justifyContent: 'center', padding: 10, marginStart: wp('5%') }}>
                        <Text style={[styles.textContentStyle, { color: Colors.WHITE }]}>با قوانین موافقم</Text>
                    </Ripple>
                </View>
            </View>
        )
    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    textContainer: {
        padding: wp('5%')
    },
    bottomContainer: {
        height: hp('8%'),
        bottom: 0,
        position: 'absolute',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: wp('3%'),
    },
    textContentStyle: {
        fontFamily: IranSansMobileBold,
        color: Colors.PRIMARY,
        fontSize: RF(2)
    },
    textTitleStyle: {
        fontFamily: IranSansMobileBold,
        color: Colors.PRIMARY,
        fontSize: RF(2.5)
    }
})
