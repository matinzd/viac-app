import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import RF from 'react-native-responsive-fontsize';
import { IranYekanMobile } from '../../constants/Fonts';
import { minmizeText } from '../../controllers/HelperMethod';
import Colors from '../../constants/Colors';
class ListItem extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={styles.listItemContainer}>
                <View style={styles.listItemCover}>
                    <FastImage source={this.props.data.pix ? { uri: this.props.data.baseUrl + '/' + this.props.data.pix.url } : require('../../assets/images/default_img.png')} style={styles.cover} />
                    <View style={{ flexDirection: 'row-reverse', position: 'absolute', backgroundColor: 'transparent', width: wp('35%'), height: wp('6%'), justifyContent: 'flex-end', alignSelf: 'flex-end', padding: wp('1%') }}>
                        {
                            this.props.data.ladder.label !== null ?
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ width: wp('10%'), height: wp('6%'), backgroundColor: Colors.ORANGETAG }}>
                                        <Text style={{ color: Colors.WHITE, alignSelf: 'center', fontFamily: IranYekanMobile, justifyContent: 'center' }}>{this.props.data.ladder.label}</Text>
                                    </View>
                                </View>
                                :
                                null
                        }
                        {
                            this.props.data.status !== undefined ?
                                <View style={{ flexDirection: 'row', marginRight:4 }}>
                                    <View style={{ width: wp('20%'), height: wp('6%'), backgroundColor: Colors.PRIMARY }}>
                                        <Text style={{ color: Colors.WHITE, alignSelf: 'center', fontFamily: IranYekanMobile, justifyContent: 'center' }}>{this.props.data.status}</Text>
                                    </View>
                                </View>
                                :
                                null
                        }
                    </View>
                </View>
                <View style={styles.listItemDetailContainer}>
                    <Text style={[styles.basicText, styles.headerText]}>{this.props.data.title}</Text>
                    <Text style={[styles.basicText, styles.descriptionText]}>{minmizeText(this.props.data.briefDescription, 150)}</Text>
                    <Text style={[styles.basicText, styles.timeAgoText]}>{this.props.data.timeAgo}</Text>
                </View>
            </View>
        )
    }

}
const styles = StyleSheet.create({
    listItemContainer: {
        flexDirection: 'row',
        width: wp('100%'),
        height: hp('25%'),
        marginBottom: hp('2%'),
        elevation: 5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowRadius: 3.84,
        shadowOpacity: 0.25,
    },
    listItemCover: {
        backgroundColor: '#333',
        overflow: 'hidden'
    },
    cover: {
        flex: 1,
        aspectRatio: 1,
    },
    listItemDetailContainer: {
        flex: 2
    },
    basicText: {
        textAlign: 'left',
        fontFamily: IranYekanMobile,
        paddingRight: wp('2%'),
        paddingLeft: wp('2%'),
        color: '#333',
    },
    headerText: {
        paddingTop: hp('.5%'),
        paddingBottom: hp('.5%'),
        fontSize: RF(2.5),
        flex: .15
    },
    descriptionText:
    {
        color: '#666',
        paddingTop: hp('.5%'),
        paddingBottom: hp('.5%'),
        fontSize: RF(2.1),
        overflow: 'hidden',
        lineHeight: RF(3),
        flex: .7
    },
    timeAgoText:
    {
        textAlign: 'right',
        color: '#999',
        fontSize: RF(2),
        flex: .15
    },
    gridItemContainer: {
        width: wp('47%'),
        height: hp('37%'),
        marginBottom: hp('1%'),
        marginTop: hp('1%'),
        elevation: 5,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowRadius: 3.84,
        shadowOpacity: 0.25,
    },
    gridItemContainerOdd: {
        marginStart: wp('1%'),
        marginEnd: wp('2%')
    },
    gridItemContainerEven: {
        marginStart: wp('2%'),
        marginEnd: wp('1%')
    },
    gridItemCover: {
        height: wp('47%'),
        overflow: 'hidden',
    },
    gridItemDetail: {
        flexGrow: 1,
    },
    gridCover: {
        flex: 1,
        width: wp('100%'),
        aspectRatio: 1
    },
    gridTextView: {
        justifyContent: 'center',
        flexGrow: 1
    },
    gridHeaderText: {
        textAlign: 'center',
        fontSize: RF(2.5),
    },
    gridTimeAgoText:
    {
        textAlign: 'center',
        color: '#999',
        fontSize: RF(2),
    },
});


class GridItem extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <View style={[styles.gridItemContainer, this.props.index % 2 == 0 ? styles.gridItemContainerEven : styles.gridItemContainerOdd]}>
                <View style={styles.gridItemCover}>
                    <FastImage source={this.props.data.pix ? { uri: this.props.data.baseUrl + '/' + this.props.data.pix.url } : require('../../assets/images/default_img.png')} style={styles.gridCover} />
                    {
                        this.props.data.ladder.label !== null ?
                            <View style={{ position: 'absolute', flexDirection: 'row' }}>
                                <View style={{ width: wp('10%'), height: wp('6%'), backgroundColor: Colors.ORANGETAG }}>
                                    <Text style={{ color: Colors.WHITE, alignSelf: 'center', fontFamily: IranYekanMobile, justifyContent: 'center' }}>{this.props.data.ladder.label}</Text>
                                </View>
                            </View>
                            :
                            null
                    }
                </View>
                <View style={styles.gridItemDetail}>
                    <View style={styles.gridTextView}>
                        <Text style={[styles.basicText, styles.gridHeaderText]}>{minmizeText(this.props.data.title, 20)}</Text>
                    </View>
                    <View style={styles.gridTextView}>
                        <Text style={[styles.basicText, styles.gridTimeAgoText]}>{this.props.data.timeAgo}</Text>
                    </View>
                </View>
            </View>
        )
    }
}

export {
    GridItem,
    ListItem
}