import React, { Component } from 'react'
import { View, Text, FlatList, StyleSheet, Image, RefreshControl } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Colors from '../../../constants/Colors';
import Ripple from 'react-native-material-ripple';
import { FloatingAction } from '../../../components/FAB/src';
import { IranYekanMobile, IranYekanMobileBold } from '../../../constants/Fonts';
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import NavigationController from '../../../controllers/NavigationController';
import AdvertisingAPI from '../../../webservices/AdvertisingAPI';
import { BASE_URL } from '../../../webservices/BaseAdapter';
import { connect, getStore } from 'trim-redux';
import FastImage from 'react-native-fast-image';
import { ListItem, GridItem } from '../../Items/AdvertismentItemL'
import { TouchableRipple } from 'react-native-paper';
import { TouchableOpacity } from 'react-native-gesture-handler';
import ViacToast from '../../../controllers/ViacToast';

class AdvertisementList extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isGridView: false,
            actions: [
                {
                    text: "افزودن تبلیغ",
                    name: "add_advertisement",
                    position: 2,
                    icon: <MCIcons name="plus" size={wp('5%')} color={Colors.WHITE} />
                },
                {
                    text: "نمایش به صورت پنجره ای",
                    name: "grid_list",
                    position: 1,
                    icon: <MCIcons name="grid" size={wp('5%')} color={Colors.WHITE} />
                }
            ],
            title: '',
            data: [],
            briefDescription: '',
            fullDescription: '',
            refreshing: false
        }
        this.offset = 0
    }

    handleItemPress = (itemName) => {
        this.handleAddOrEditAdvertisement()
    }

    handleAddOrEditAdvertisement() {
        if (getStore('userInfo').isUserLoggedIn) {
            NavigationController.navigate('AddOrEditAdvertisement')
        } else {
            NavigationController.navigate('Authentication')
        }
    }

    handleGridListView() {
        if (!this.state.isGridView) {
            this.state.actions = [
                {
                    text: "افزودن تبلیغ",
                    name: "add_advertisement",
                    position: 2,
                    icon: <MCIcons name="plus" size={wp('5%')} color={Colors.WHITE} />
                },
                {
                    text: "نمایش به صورت لیست",
                    name: "grid_list",
                    position: 1,
                    icon: <MCIcons name="view-list" size={wp('5%')} color={Colors.WHITE} />
                }
            ]
        } else {
            this.state.actions = [
                {
                    text: "افزودن تبلیغ",
                    name: "add_advertisement",
                    position: 2,
                    icon: <MCIcons name="plus" size={wp('5%')} color={Colors.WHITE} />
                },
                {
                    text: "نمایش به صورت پنجره ای",
                    name: "grid_list",
                    position: 1,
                    icon: <MCIcons name="grid" size={wp('5%')} color={Colors.WHITE} />
                }
            ]
        }
        this.setState({
            isGridView: !this.state.isGridView,
            actions: this.state.actions
        })
    }

    componentDidMount() {
        this.getData()
    }

    getData() {
        this.setState({
            refreshing: true
        })
        AdvertisingAPI.getAll()
    }

    componentWillReceiveProps(nextProps) {
        console.log('LOG LIST ', getStore('adsIsGridView'))
        if (this.props.advertisements != nextProps.advertisements) {
            console.log(nextProps)
            let data = nextProps.advertisements;
            this.setState({
                data: data.data,
                refreshing: false
            })
        }

        if (nextProps.adsIsGridView != this.state.isGridView) {
            this.setState({
                isGridView: nextProps.adsIsGridView
            })
        }
    }

    _onScroll = (event) => {

        // this.props.navigation.setParams({ tabBarVisible: false })

        // var currentOffset = event.nativeEvent.contentOffset.y;
        // var currentHeight = parseInt(hp('50%'))
        // var direction = currentOffset > this.offset ? 'down' : 'up';
        // this.offset = currentOffset;
        // console.log('Current offset ', currentOffset)
        // if (direction === 'up' || currentOffset < currentHeight ) {
        //     this.props.navigation.setParams({ tabBarVisible: true })
        // } else {
        //     this.props.navigation.setParams({ tabBarVisible: false })
        // }
    }


    onAdvertisementPress(id) {
        NavigationController.navigate('AdvertisementDetails', { id });
    }

    render() {
        return (
            <View style={{ backgroundColor: '#f1f1f1', width: wp('100%'), flex: 1 }}>
                <FlatList
                    style={styles.flatList}
                    data={this.state.data}
                    refreshControl={
                        <RefreshControl onRefresh={() => this.getData()} refreshing={this.state.refreshing} />
                    }
                    onEndReached={() => {
                        console.log('On end reached')
                    }}
                    onScroll={this._onScroll.bind(this)}
                    key={this.state.isGridView ? 1 : 2}
                    numColumns={this.state.isGridView ? 2 : 1}
                    ListEmptyComponent={() => (
                        !this.state.refreshing ?
                            <View style={{ width: wp('100%'), marginTop: wp('4%'), }}>
                                <Text style={[styles.itemTextStyle, { textAlign: 'center' }]}>متاسفانه آگهی مطابق درخواست شما یافت نشد 😢</Text>
                            </View>
                            :
                            null
                    )}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item, index }) => (
                        <TouchableRipple onPress={() => this.onAdvertisementPress(item.id)}>
                            {
                                this.state.isGridView ? (
                                    <GridItem index={index} data={{ title: item.title, ladder: item.ladder, baseUrl: BASE_URL, pix: item.pix, briefDescription: item.briefDescription, timeAgo: item.timeAgo }} />
                                ) :
                                    <ListItem data={{ title: item.title, ladder: item.ladder, baseUrl: BASE_URL, pix: item.pix, briefDescription: item.briefDescription, timeAgo: item.timeAgo, }} />
                            }
                        </TouchableRipple>
                        // <Ripple
                        //     style={[this.state.isGridView ? styles.gridViewContainer : styles.listItemContainer, { flexDirection: this.state.isGridView ? 'column' : 'row' }, this.state.isGridView ? { marginStart: index % 2 === 1 ? wp('5%') : 0 } : {}]}
                        //     onPress={() => this.onAdvertisementPress(item.id)}
                        // >
                        //     <View style={styles.itemParent}>
                        //         <FastImage source={item.pix ? { uri: BASE_URL + '/' + item.pix.url } : require('../../../assets/images/default_img.png')} style={{ height: hp('20%'), width: this.state.isGridView ? wp('45%') : hp('20%') }} />
                        //         <View style={{ flexDirection: 'row', position: 'absolute', backgroundColor: 'transparent', width: wp('22%'), height: wp('6%'), justifyContent: 'flex-end', alignSelf: 'flex-end', padding: wp('1%') }}>
                        //             {
                        //                 item.ladder.label !== null ?
                        //                     <View>
                        //                         <View style={{ width: wp('10%'), height: wp('6%'), backgroundColor: Colors.ORANGETAG }}>
                        //                             <Text style={{ color: Colors.WHITE, alignSelf: 'center', fontFamily: IranYekanMobile, justifyContent: 'center' }}>فوری</Text>
                        //                         </View>
                        //                         <View style={{ width: wp('10%'), marginStart: wp('1%'), height: wp('6%'), backgroundColor: Colors.ORANGETAG }}>
                        //                             <Text style={{ color: Colors.WHITE, alignSelf: 'center', fontFamily: IranYekanMobile, justifyContent: 'center' }}>ویژه</Text>
                        //                         </View>
                        //                     </View>
                        //                     :
                        //                     null
                        //             }
                        //         </View>
                        //     </View>
                        //     <View style={{ width: this.state.isGridView ? wp('45') : wp('55%'), height: this.state.isGridView ? hp('8%') : hp('20%'), padding: wp('2%'), justifyContent: 'center' }}>
                        //         <Text style={[styles.itemTextStyle, { fontFamily: IranYekanMobileBold, fontSize: wp('4%'), textAlign: this.state.isGridView ? 'center' : 'auto' }]} >{item.title}</Text>
                        //         {
                        //             !this.state.isGridView ?
                        //                 <Text numberOfLines={3} allowFontScaling={false} ellipsizeMode="tail" style={[styles.itemTextStyle, { fontSize: wp('3%') }]} >{item.briefDescription}</Text>
                        //                 :
                        //                 null
                        //         }
                        //         <Text style={[styles.itemTextStyle, { color: '#d5d5d5', fontSize: wp('3%'), textAlign: this.state.isGridView ? 'center' : 'auto' }]}>{item.timeAgo}</Text>
                        //     </View>
                        //     <View style={styles.itemDivider} />

                        // </Ripple>


                    )}
                />
                <FloatingAction
                    floatingIcon={
                        <MCIcons name="plus" size={wp('8%')} color={Colors.WHITE} />
                    }
                    // onPressItem={this.handleItemPress.bind(this)}
                    // actions={this.state.actions}
                    onPressMain={this.handleItemPress.bind()}
                    position="left"
                // style={{ backgroundColor: '#3ab', position:'absolute', bottom:10,elevation: 8  }}
                />
            </View>
        )
    }


}


const styles = StyleSheet.create({
    flatList: {

    },
    listItemContainer: {
        height: hp('20%'),
        width: wp('100%'),
        elevation: 5,
        marginTop: wp('.5%'),
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        marginBottom: hp('1%')
    },
    itemParent: {
        padding: 5 | 5,
    },
    itemDivider: {
        height: 5,
        backgroundColor: Colors.Grays
    },
    gridViewContainer: {
        height: wp('50%'),
        width: wp('45%'),
        elevation: 5,
        backgroundColor: Colors.WHITE,
        marginTop: wp('4%'),
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
    },
    itemTextStyle: {
        fontFamily: IranYekanMobile,
        color: Colors.PRIMARY
    }
})


const mapStateToProps = state => ({ advertisements: state.advertisements, adsIsGridView: state.adsIsGridView })
export default connect(mapStateToProps)(AdvertisementList)