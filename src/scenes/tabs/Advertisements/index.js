import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign';
import { Toolbar } from 'react-native-material-ui';
import { IranSansMobile, IranYekanMobile } from '../../../constants/Fonts';
import Colors from '../../../constants/Colors';
import NavigationController from '../../../controllers/NavigationController';
import { Statusbar } from '../../../components/viac';
import AdvertisementList from './AdvertisementList';
import RandomTagsList from './RandomTagsList';
import BottomSortAndFilter from './BottomSortAndFilter';
import AdvertisingAPI from '../../../webservices/AdvertisingAPI';
import { getStore, setStore } from 'trim-redux';
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import Ripple from 'react-native-material-ripple';
import RF from 'react-native-responsive-fontsize';
import ViacToast from '../../../controllers/ViacToast';

class Advertisements extends Component {

    // static navigationOptions = ({ navigation }) => ({
    //     tabBarColor: Colors.PRIMARY,
    //     tabBarIcon: <Icon name="notification" size={25} color="#fff" />,
    //     tabBarLabel: <Text style={{ fontFamily: IranSansMobile }} >آگهی ها</Text>,
    //     tabBarVisible: navigation.getParam('tabBarVisible') === false ? false : true
    // });

    constructor(props) {
        super(props)
        this.state = {
            search: '',
            adsIsGridView: getStore('adsIsGridView')
        }
    }

    componentDidMount() {
        console.log(this.props.navigation)
    }

    onSearchClicked = () => {
        console.log('On search clicked')
    }

    onSubmitEditing = () => {
        let selectedTags = getStore('selectedTags')
        let selectedStates = getStore('selectedStates')
        let selectedCities = getStore('selectedCities')
        let selectedCategories = getStore('selectedCategories')
        let textSearch = getStore('textSearch')
        AdvertisingAPI.getAll(selectedTags, selectedStates, selectedCities, selectedCategories, textSearch)
    }

    onSearchTextChanged = (text) => {
        setStore({
            textSearch: text
        })
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
                <Statusbar light={false} backgroundColor={Colors.WHITE} />
                <Toolbar
                    ref={ref => this.toolbar = ref}
                    leftElement="menu"
                    searchable={{
                        autoFocus: true,
                        placeholder: 'جست و جو ...',
                        onSubmitEditing: this.onSubmitEditing.bind(this),
                        onSearchPressed: this.onSearchClicked.bind(this),
                        onChangeText: this.onSearchTextChanged.bind(this),
                    }}
                    rightElement={
                        <Ripple onPress={() => {
                            setStore({
                                adsIsGridView: !getStore('adsIsGridView')
                            })
                        }} style={{ justifyContent: 'center' }}>
                            {
                                getStore('adsIsGridView') ?
                                    <MCIcons name="view-list" size={widthPercentageToDP('5%')} color={Colors.SECONDARY} />
                                    :
                                    <MCIcons name="grid" size={widthPercentageToDP('5%')} color={Colors.SECONDARY} />
                            }
                        </Ripple>
                    }
                    centerElement={
                        <Text style={{alignSelf: 'flex-start', color: Colors.SECONDARY, fontFamily: IranYekanMobile, fontSize: RF(2.5)}}>آگهی ها</Text>
                    }
                    currentRoute='Tabs'
                    onLeftElementPress={() => {
                        NavigationController.openDrawer()
                    }}
                    onRightElementPress={(label) => { NavigationController.openDrawer() }}
                />
                {/* <Ripple onPress={() => NavigationController.navigate('AdvertisementDetails')} >
                    <Text style={{ fontFamily: IranSansMobileBold }} >جزییات آگهی</Text>
                </Ripple> */}
                <RandomTagsList {...this.props} />
                <AdvertisementList {...this.props} />
                <BottomSortAndFilter {...this.props} />
            </View>
        )
    }
}


const styles = StyleSheet.create({

})


export default Advertisements