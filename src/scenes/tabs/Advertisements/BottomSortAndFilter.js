import React, { Component } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { View, Text, StyleSheet } from 'react-native'
import { setStore } from 'trim-redux';
import AdvertisementsFilter from './AdvertisementsFilter';
import { BottomDrawer } from '../../../components/viac';
import Colors from '../../../constants/Colors';
import { IranSansMobile } from '../../../constants/Fonts';

export default class BottomSortAndFilter extends Component {

    constructor(props) {
        super(props)
    }

    componentDidMount(){
        
    }

    onIndexChange(index) {
        this.setState({ index })
    }

    componentWillReceiveProps() {
        
    }

    render() {
        return (

            <BottomDrawer
                ref={ref => {
                    this.advertisementBottomSheet = ref
                    setStore({
                        advertisementBottomSheet: ref
                    })
                }}
                roundedEdges={true}
                shadow={true}
                downDisplay={hp('80%')}
                startUp={false}
                offset={wp('10%')}
                containerHeight={hp('50%')}
                >
                
                <AdvertisementsFilter {...this.props} />
            </BottomDrawer>
        )
    }

}



const styles = StyleSheet.create({
    bottomDrawerContainer: {
        backgroundColor: Colors.SECONDARY
    },
    bottomTextStyle: {
        fontFamily: IranSansMobile,
        fontSize: wp('3%'),
        color: Colors.WHITE,
        alignSelf: 'center'
    }
})
