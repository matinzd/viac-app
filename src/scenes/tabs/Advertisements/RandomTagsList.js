import React, { Component } from 'react'
import { View, Text, FlatList, StyleSheet, Image, RefreshControl, I18nManager } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Colors from '../../../constants/Colors';
import Ripple from 'react-native-material-ripple';
import { IranYekanMobile, IranYekanMobileBold } from '../../../constants/Fonts';
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { getStore, connect, setStore } from 'trim-redux';
import TagAPI from '../../../webservices/TagAPI';
import { ActivityIndicator } from 'react-native-paper';
import _ from 'lodash'
import Color from '../../../styles/Color';
import AdvertisingAPI from '../../../webservices/AdvertisingAPI';
import RF from 'react-native-responsive-fontsize';

class RandomTagsList extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isGridView: false,
            randomTags: [],
            loading: false,
            selectedTags: []
        }
    }

    handleItemPress = (itemName) => {
        switch (itemName) {
            case "bt_accessibility":
                alert('add advertisement')
                break;
            case "grid_list":
                this.setState({
                    isGridView: !this.state.isGridView
                })
                break;
            default:
                break;
        }
    }

    componentWillMount() {
        this.setState({ loading: true })
        TagAPI.getRandom().then(response => {
            this.setState({
                randomTags: response.data.data,
                loading: false
            })
        }).catch(() => {
            this.setState({
                loading: false
            })
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ selectedTags: nextProps.selectedTags })
    }

    onTagPress(id) {
        let selectedTags = getStore('selectedTags');
        let isSelected = false;
        for (let tag of selectedTags) {
            if (tag === id) {
                isSelected = true
                break;
            }
        }
        console.log('Is selected ', isSelected)
        console.log('Selected tags ', selectedTags)
        if (isSelected) {
            let newTags = []
            for (let tag of selectedTags) {
                if( id !== tag ) {
                    newTags.push(tag)
                }
            }
            setStore({ selectedTags: newTags })
            this.setState({ selectedTags: newTags })
        } else {
            let newTags = getStore('selectedTags')
            newTags.push(id)
            setStore({ selectedTags: newTags })
            this.setState({ selectedTags: newTags })
        }
        AdvertisingAPI.getAll()
    }

    onFilterPress = () => {
        let advertisementBottomSheet = getStore('advertisementBottomSheet');
        advertisementBottomSheet.toggleDrawerState()
    }

    render() {
        return (
            <View style={{ height: null, marginVertical: wp('2%') }}>

                {
                    this.state.loading ?
                        <ActivityIndicator color={Colors.PRIMARY} size={15} />
                        :
                        <FlatList
                            data={this.state.randomTags}
                            ListHeaderComponent={() => (
                                <Ripple style={styles.tagItem} onPress={this.onFilterPress.bind(this)} >
                                    <MCIcons name="filter" size={wp('5%') } />
                                    <Text style={styles.tagText}>فیلتر کردن</Text>
                                </Ripple>
                            )}
                            style={{ alignSelf: 'flex-start' }}
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item, index }) =>
                                (
                                    <Ripple onPress={() => { this.onTagPress(item.id) }} style={[{backgroundColor: this.state.selectedTags.includes(item.id) ? Colors.PRIMARY : Colors.WHITE } , styles.tagItem]}  >
                                        <Text style={[styles.tagText, { color: this.state.selectedTags.includes(item.id) ? Colors.WHITE : Colors.PRIMARY, marginStart: wp('1%') }]}>{item.title}</Text>
                                        {
                                            this.state.selectedTags.includes(item.id) ?
                                                <MCIcons name="close-circle" color={Color.BUTTON_ACCEPT} size={wp('4%')} style={{ marginStart: wp('2%') }} />
                                                :
                                                null
                                        }
                                    </Ripple>
                                )
                            }
                        />
                }
            </View>
        )
    }
}

const data = [
    {
        name: 'فوتبال',
        id: 1
    },
    {
        name: 'استخر',
        id: 2
    },
    {
        name: 'سالن والیبال',
        id: 3
    },
    {
        name: 'فوتبال',
        id: 1
    },
    {
        name: 'استخر',
        id: 2
    },
    {
        name: 'سالن والیبال',
        id: 3
    },
    {
        name: 'فوتبال',
        id: 1
    },
    {
        name: 'استخر',
        id: 2
    },
    {
        name: 'سالن والیبال',
        id: 3
    }
]

const styles = StyleSheet.create({
   
    tagItem:{
        paddingHorizontal: wp('4%'),
        paddingVertical: wp('1%'),
        borderWidth: 1,
        borderColor: Colors.PRIMARY,
        borderRadius: hp('1%'),
        flexDirection: 'row',
        marginStart: wp('3%'),
        height: hp('6%'),
        alignItems: 'center'
       
    },
    tagText:
    {
        fontFamily: IranYekanMobile,
        fontSize: RF(2.2),
        color: Colors.PRIMARY,
        marginStart: wp('1%')
    }
})

const mstp = state => ({ selectedTags: state.selectedTags })
export default connect(mstp)(RandomTagsList)