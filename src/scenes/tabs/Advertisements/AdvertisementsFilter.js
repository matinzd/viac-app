import React, { Component } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { View, Text, StyleSheet, ScrollView, KeyboardAvoidingView } from 'react-native'
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';
// import { setStore } from 'trim-redux';
import { TextField } from 'react-native-material-textfield';
import { Checkbox } from 'react-native-material-ui';
import {
    Chip,
    Selectize
} from 'react-native-material-selectize';
import Ripple from 'react-native-material-ripple';
import { getStore, setStore, connect } from 'trim-redux';
import Colors from '../../../constants/Colors';
import { IranSansMobile } from '../../../constants/Fonts';
import CategoryAPI from '../../../webservices/CategoryAPI';
import TagAPI from '../../../webservices/TagAPI';
import AdvertisingAPI from '../../../webservices/AdvertisingAPI';

class AdvertisementsFilter extends Component {

    constructor(props) {
        super(props)
        // this.state = {
        // index: 0,
        // routes: [
        // { key: 'sort', title: 'مرتب سازی' },
        //     { key: 'filter', title: 'فیلتر کردن' },
        // ],
        // isTabBarVisible: true

        // };
        this.state = {
            names: [
                'اسختر', 'والیبال'
            ],
            text: '',
            hasImage: true,
            categories: [],
            tags: [],
            selectedTags: getStore('selectedTags')
        }
    }

    componentDidMount() {
        CategoryAPI.getAll().then((response) => {
            let categories = response.data.data;
            categories.push({
                id: '',
                name: 'همه'
            })
            this.setState({
                categories
            })
        })
        TagAPI.getAll().then((response) => {
            this.setState({
                tags: response.data.data
            })
        })
    }

    onIndexChange(index) {

    }

    componentWillReceiveProps(np) {
        console.log('FILTER PROPS ', np)
        this.setState({ selectedTags: getStore('selectedTags') })
    }


    onSubmitPress = () => {
        let advertisementBottomSheet = getStore('advertisementBottomSheet')
        let selectedTags = getStore('selectedTags')
        let selectedStates = getStore('selectedStates')
        let selectedCities = getStore('selectedCities')
        let selectedCategories = getStore('selectedCategories')
        let textSearch = getStore('textSearch')
        AdvertisingAPI.getAll(selectedTags, selectedStates, selectedCities, selectedCategories, textSearch).then(() => {
            advertisementBottomSheet.closeBottomDrawer()
        })
    }

    render() {
        return (
            <View>
                <ScrollView
                    keyboardShouldPersistTaps='handled'
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}>
                    <View style={{ flex: 1, padding: wp('8%'), marginBottom: 40, justifyContent: 'center' }}>
                        <KeyboardAvoidingView behavior="padding">
                            <Selectize
                                textInputProps={{ placeholder: 'به طور مثال فوتبال ...' }}
                                labelStyle={{ fontFamily: IranSansMobile }}
                                // containerStyle={{ fontFamily: IranSansMobile }}
                                label='انتخاب رشته ورزشی'
                                selectedItems={getStore('selectedCategories')}
                                ref={c => (this._childEmailField = c)}
                                items={this.state.categories}
                                onChangeSelectedItems={(item) => {
                                    setStore({
                                        selectedCategories: item.result
                                    })
                                }}
                                renderRow={(id, onPress, item) => (
                                    <Ripple
                                        onPress={onPress}
                                        style={{
                                            paddingVertical: 8,
                                            paddingHorizontal: 10,
                                            backgroundColor: '#EEE',
                                            height: hp('5%'),
                                            width: wp('90%'),
                                            justifyContent: 'center'
                                        }}
                                    >
                                        <Text style={{ fontFamily: IranSansMobile, color: Colors.PRIMARY }} >{item.name}</Text>
                                    </Ripple>
                                )}
                            />

                            <Selectize
                                textInputProps={{ placeholder: 'برای مثال تفریح ...' }}
                                labelStyle={{ fontFamily: IranSansMobile }}
                                // containerStyle={{ fontFamily: IranSansMobile }}
                                label='انتخاب تگ'
                                selectedItems={this.state.selectedTags}
                                onChangeSelectedItems={(item) => {
                                    setStore({
                                        selectedTags: item.result
                                    })
                                }}
                                ref={c => (this._childEmailField = c)}
                                items={this.state.tags}
                                renderRow={(id, onPress, item) => (
                                    <Ripple
                                        onPress={onPress}
                                        style={{
                                            paddingVertical: 8,
                                            paddingHorizontal: 10,
                                            backgroundColor: '#EEE',
                                            height: hp('5%'),
                                            width: wp('90%'),
                                            justifyContent: 'center'
                                        }}
                                    >
                                        <Text style={{ fontFamily: IranSansMobile, color: Colors.PRIMARY }} >{item.title}</Text>
                                    </Ripple>
                                )}
                            />

                            {/* <Selectize
                                textInputProps={{ placeholder: 'برای مثال نردبان یک ...' }}
                                labelStyle={{ fontFamily: IranSansMobile }}
                                // containerStyle={{ fontFamily: IranSansMobile }}
                                label='انتخاب نردبان'
                                ref={c => (this._childEmailField = c)}
                                items={nardeban}
                                renderRow={(id, onPress, item) => (
                                    <Ripple
                                        onPress={onPress}
                                        style={{
                                            paddingVertical: 8,
                                            paddingHorizontal: 10,
                                            backgroundColor: Colors.SECONDARY,
                                            height: hp('5%'),
                                            width: wp('90%'),
                                            justifyContent: 'center'
                                        }}
                                    >
                                        <Text style={{ fontFamily: IranSansMobile, color: Colors.WHITE }} >{id}</Text>
                                    </Ripple>
                                )}
                            /> */}
                        </KeyboardAvoidingView>
                        <View style={{ height: hp('5%') }}>
                            <Checkbox label="فقط نمایش عکس دار ها" value="hasImage" onCheck={() => { this.setState({ hasImage: !this.state.hasImage }) }} checked={this.state.hasImage} />
                        </View>
                        <Ripple style={[styles.verifyButtonStyle]} onPress={this.onSubmitPress.bind(this)} >
                            <Text style={styles.buttonTextStyle} >اعمال</Text>
                        </Ripple>
                    </View>

                </ScrollView>

            </View>

        )
    }

}

const styles = StyleSheet.create({
    inputContainer: {
        marginHorizontal: wp('10%')
    },
    verifyButtonStyle: {
        backgroundColor: Colors.PRIMARY,
        justifyContent: 'center',
        width: '50%',
        height: hp('5%'),
        borderRadius: wp('2%'),
        marginHorizontal: wp('10%'),
        marginTop: hp('2%'),
        alignSelf: 'center'
    },
    inputLabelStyle: {
        fontFamily: IranSansMobile,
    },
    buttonTextStyle: {
        color: Colors.WHITE,
        alignSelf: 'center',
        fontFamily: IranSansMobile
    }
})


const mstp = state => ({ selectedTags: state.selectedTags })
export default connect(mstp)(AdvertisementsFilter)