import React, { Component } from 'react';
import { View, StyleSheet, Text, TextInput, FlatList, StatusBar, Image, I18nManager, Picker, ScrollView, PermissionsAndroid, TouchableOpacity, Linking, BackHandler } from 'react-native'
import { Toolbar, RadioButton } from 'react-native-material-ui'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Statusbar } from '../../../components/viac';
import { IranSansMobile, IranYekanMobile, IranSansMobileBold } from '../../../constants/Fonts.js';
import Colors from '../../../constants/Colors.js';
import { TextField } from 'react-native-material-textfield';
import {
    Chip,
    Selectize
} from 'react-native-material-selectize';
import Ripple from 'react-native-material-ripple';
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SLIcons from 'react-native-vector-icons/SimpleLineIcons';
// import ImagePicker from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';
import { ProcessingManager } from 'react-native-video-processing';
import _ from 'lodash'
import Video from 'react-native-video'

import NavigationController from '../../../controllers/NavigationController';
import CategoryAPI from '../../../webservices/CategoryAPI';
import TagAPI from '../../../webservices/TagAPI';
import states from '../../../constants/data/states.json'
import AdvertisingAPI from '../../../webservices/AdvertisingAPI';
import { getStore, setStore, connect } from 'trim-redux';
import { ProgressDialog } from 'react-native-simple-dialogs';
import Modal from "react-native-modal";
import StateModal from '../../../modals/StateModal';
import LocationModal from '../../../modals/LocationModal';
import RF from 'react-native-responsive-fontsize';
import Toast from 'react-native-custom-toast';
import ViacToast from '../../../controllers/ViacToast';
import { BASE_URL } from '../../../webservices/BaseAdapter';
import Color from '../../../styles/Color';
import LadderAPI from '../../../webservices/LadderAPI';
import { ActivityIndicator } from 'react-native-paper';
import IABHandler from '../../../controllers/IABHandler';
import ResumeAPI from '../../../webservices/ResumeAPI';



class AddAdvertisement extends Component {

    constructor(props) {
        super(props)
        this.state = {
            images: [],
            id: null,
            categories: [],
            tags: [],
            selectedCategoryId: 0,
            selectedCity: 0,
            title: '',
            briefDescription: '',
            fullDescription: '',
            latitude: 0,
            longitude: 0,
            cityId: 0,
            selectedTags: [],
            ladders: [],
            ladderDescription: '',
            selectedLadderId: '',
            address: '',
            states: _.filter(states, (o) => { return o !== null }),
            loading: false,
            isHashtagHelperVisible: false,
            step: 1,
            maxStep: 3,
            titleFocus: false,
            briefDescriptionFocus: false,
            descriptionFocus: false,
            arrResume: [],
            arrBaseResume: [],
            isResumeModalVisible: false,
            arrSelectedResume: [],
            videoData: null,
            ladder: null,
            loadingTitle: 'در حال ارسال آگهی'

        }
        this.imageFormData = new FormData()
        this.videoFormData = new FormData()
    }

    componentWillMount() {

        if (this.props.navigation.getParam('type') === 'edit') {

            let defaultValues = this.props.navigation.getParam('advertisementDetail')
            
            this.setState({
                id: defaultValues.id,
                selectedCategoryId: defaultValues.categoryId,
                title: defaultValues.title,
                briefDescription: defaultValues.briefDescription,
                fullDescription: defaultValues.fullDescription,
                address: defaultValues.address,
                latitude: defaultValues.latitude,
                longitude: defaultValues.longitude,
                maxStep: defaultValues.ladder.pri === 5 ? 4 : 3,
                ladder: defaultValues.ladder,
            })
            if (defaultValues.pix !== undefined && defaultValues.pix !== null) {
                defaultValues.pix.map(item => {
                    this.state.images.push({
                        imagePath: BASE_URL + '/' + item.url
                    })
                })
            }
            console.log(this.state.images)
            this.setState({
                images: this.state.images
            })
        }
        LadderAPI.getAll().then((response) => {
            this.setState({
                ladders: response.data.data
            })
        })
        CategoryAPI.getAll().then((response) => {
            let categories = response.data.data;
            this.setState({
                categories
            })
        })
        TagAPI.getAll().then((response) => {
            this.setState({
                tags: response.data.data
            })
        })

        ResumeAPI.getAll("", "")
            .then(response => {
                this.setState({ arrResume: response.data.data.data, arrBaseResume: response.data.data.data })
            }).catch((err) => {
                this.setState({ loading: false })
                if (err.serverMessage.response.data.message) {
                    ViacToast.showToast(err.serverMessage.response.data.message, 2000)
                }
            })

        this.backListener = BackHandler.addEventListener('hardwareBackPress', () => {
            if (this.state.step != 1) {
                this.prevStep()
                return true
            } else {
                return false
            }
        })
    }

    onSubmitEditing = () => {

    }

    onSearchClicked = () => {

    }

    componentWillUnmount() {
        this.backListener.remove()
    }

    onSearchTextChanged = (text) => {
        let temp = _.filter(this.state.temp, (o) => {
            if (o.stateName.search(text) >= 0) {
                return o
            }
        })
        this.setState({
            data: temp
        })
    }

    options = {
        title: 'انتخاب عکس',
        storageOptions: {
            skipBackup: true,
            path: 'images',
        },
        chooseFromLibraryButtonTitle: 'انتخاب از گالری',
        takePhotoButtonTitle: 'گرفتن عکس جدید'
    };


    pickImage() {
        this.grantPermission().then((isGranted) => {
            let isCameraGranted = isGranted["android.permission.CAMERA"]
            let isWriteStorageGranted = isGranted["android.permission.WRITE_EXTERNAL_STORAGE"]
            let isReadStorageGranted = isGranted["android.permission.READ_EXTERNAL_STORAGE"]
            if (isWriteStorageGranted || isCameraGranted) {
                ImagePicker.openPicker({ width: 400, height: 300, cropperToolbarTitle: 'ویرایش عکس', cropping: true, compressImageQuality: 0.7, })
                    .then((response) => {
                        console.log('Response = ', response);

                        // You can also display the image using data:
                        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                        let canAdd = true
                        this.state.images.map((item) => {
                            if (response.path === item.path) {
                                canAdd = false
                            }
                        })
                        if (canAdd && this.state.images.length < 4) {
                            this.state.images.push({
                                imagePath: response.path,
                                path: response.path,
                                uri: response.uri,
                                type: response.type,
                                fileName: response.fileName
                            })
                            this.imageFormData.append('pictures[]', {
                                uri: response.path,
                                type: response.mime,
                                name: response.path.substr(response.path.lastIndexOf('/') + 1)
                            })
                            this.setState({
                                images: this.state.images
                            })
                            console.log('Form data added ', this.imageFormData)
                        } else if (!canAdd) {
                            ViacToast.showToast('عکس تکراری است', 2000)
                        } else if (this.state.images.length >= 4 && this.state.ladder.pri !== 5) {
                            ViacToast.showToast('شما بیشتر از 4 عکس نمیتوانید اضافه کنید!', 2000)
                        } else if (this.state.images.length >= 8 && this.state.ladder.pri === 5) {
                            ViacToast.showToast('شما بیشتر از 8 عکس نمیتوانید اضافه کنید!', 2000)
                        }

                    })
            } else {
                ViacToast.showToast('دسترسی به حاظه داده نشد', 2000)
            }
        })
    }

    async grantPermission() {
        return await PermissionsAndroid.requestMultiple(
            [
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.CAMERA
            ]
        )
    }

    /**
     * 
     * @param {string} description 
     */
    extractHashtags(description) {
        var hashtagGlobalRegex = new RegExp(/#[^\s!@#$%^&*()=+./,\[{\]};:'"?><]+/g);
        this.hashtags = description.match(hashtagGlobalRegex)
    }

    onDeleteImage(item) {
        let newData = _.filter(this.state.images, (o) => {
            if (!(o.path === item.path)) {
                return true
            }
        })
        this.imageFormData = new FormData()
        newData.map(item => {
            this.imageFormData.append('pictures[]', {
                uri: item.uri,
                type: item.type,
                name: item.fileName
            })
        })
        this.setState({
            images: newData
        })
        console.log('Form data after delete ', this.imageFormData)
    }

    onAdvertiseSubmit() {
        let location = getStore('location');
        let submittingFormData = _.cloneDeep(this.imageFormData);
        if (this.props.navigation.getParam('type', '') === 'edit') {
            submittingFormData.append('id', this.state.id)
        }
        submittingFormData.append('title', this.state.title)
        submittingFormData.append('address', this.state.address)
        submittingFormData.append('briefDescription', this.state.briefDescription)
        submittingFormData.append('fullDescription', this.state.fullDescription)
        submittingFormData.append('categoryId', this.state.selectedCategoryId)
        submittingFormData.append('ladderId', this.state.selectedLadderId)
        submittingFormData.append('latitude', location.latitude)
        submittingFormData.append('longitude', location.longitude)
        submittingFormData.append('cityId', getStore('selectedStateId'))
        if (this.hashtags && this.hashtags.length) {
            for (let i = 0; i < this.hashtags.length; i++) {
                submittingFormData.append('tags[]', this.hashtags[i].substr(1))
            }
        }
        this.setState({
            loading: true,
            loadingTitle: 'در حال ارسال آگهی'
        })
        AdvertisingAPI.insertUpdate(submittingFormData)
            .then(response => {
                console.log('HERE')
                this.setState({
                    loading: false,
                })
                response = response.data
                ViacToast.showToast(response.message, 3000)
                if (response.data.payUrl && response.success) {
                    Linking.openURL(BASE_URL + '/' + response.data.payUrl)
                }
                if (response.success) {
                    if (this.state.maxStep === 4) {
                        this.createResumeAndVideoFormData()
                        this.setState({
                            loading: true,
                            loadingTitle: 'در حال ارسال ویدیو'
                        })
                        AdvertisingAPI.updateForFifthLadder(this.videoFormData)
                            .then(result => {
                                this.setState({
                                    loading: false,
                                })
                                if (result.data.success) {
                                    NavigationController.goBack()
                                    if (this.props.navigation.getParam('type', '') === 'edit') {
                                        NavigationController.navigate('ManageAd', { id: this.state.id })
                                    } else {
                                        NavigationController.navigate('MyAds')
                                    }
                                } else {
                                    ViacToast.showToast(response.message, 3000)
                                }
                            })
                    } else {
                        NavigationController.goBack()
                        if (this.props.navigation.getParam('type', '') === 'edit') {
                            NavigationController.navigate('ManageAd', { id: this.state.id })
                        } else {
                            NavigationController.navigate('MyAds')
                        }
                    }
                } else {
                    ViacToast.showToast(response.message, 3000)
                }
            }).catch(response => {
                this.setState({
                    loading: false
                })
                response = response.data
                ViacToast.showToast(response.message, 3000)
                if (response.success) {
                    NavigationController.goBack()
                } else {
                    ViacToast.showToast('خطا در ارسال آگهی', 3000)
                }
            })

    }

    renderStepOne() {
        return (
            <View style={{ paddingHorizontal: 16 }}>
                <Text style={[styles.textStyle, { marginTop: 8 }]}>عنوان آگهی</Text>
                <TextInput
                    style={{
                        fontFamily: IranYekanMobile,
                        borderBottomColor: this.state.titleFocus ? Colors.SECONDARY : Colors.PRIMARY,
                        borderBottomWidth: 1,
                        height: 48,
                        textAlign: 'right',
                    }}
                    onFocus={() => this.setState({ titleFocus: true })}
                    onBlur={() => this.setState({ titleFocus: false })}
                    value={this.state.title}
                    onChangeText={title => this.setState({ title })}
                    titleTextStyle={styles.inputLabelStyle}
                    labelTextStyle={styles.inputLabelStyle}
                />
                <Text style={[styles.textStyle, { marginVertical: 4, fontSize: RF(1.8), color: Colors.Grays }]}>عنوان آگهی بسیار مهم است لطفا به موارد مهم اشاره فرمایید</Text>
                <Text style={[styles.textStyle, { marginVertical: wp('2%') }]}>رشته ورزشی</Text>
                <Picker
                    label='لطفا رشته ورزشی را برای آگهی خود انتخاب کنید'
                    itemStyle={{ fontFamily: IranSansMobile }}
                    selectedValue={this.state.selectedCategoryId}
                    onValueChange={(itemValue, itemIndex) => {
                        console.log(itemValue, itemIndex)
                        this.setState({
                            selectedCategoryId: itemValue
                        })
                    }}
                    style={{ borderWidth: 2, borderColor: Colors.PRIMARY }}
                    mode="dialog"
                >
                    {
                        this.state.categories.map(item => (
                            <Picker.Item value={item.id} key={item.id} label={item.name} />
                        ))
                    }
                </Picker>
                <Text style={[styles.textStyle, { marginTop: 8 }]}>شرح کوتاه</Text>
                <TextInput
                    style={{
                        fontFamily: IranYekanMobile,
                        borderBottomColor: this.state.briefDescriptionFocus ? Colors.SECONDARY : Colors.PRIMARY,
                        borderBottomWidth: 1,
                        minHeight: 48,
                        textAlign: 'right'
                    }}
                    onFocus={() => this.setState({ briefDescriptionFocus: true })}
                    onBlur={() => this.setState({ briefDescriptionFocus: false })}
                    value={this.state.briefDescription}
                    onChangeText={briefDescription => this.setState({ briefDescription })}
                    titleTextStyle={styles.inputLabelStyle}
                    labelTextStyle={styles.inputLabelStyle}
                />
                <Text style={[styles.textStyle, { marginVertical: 4, fontSize: RF(1.8), color: Colors.Grays }]}>
                    شرح کوتاه و نکات کلیدی خود را به صورت مختصر توضیح دهید
                </Text>
                <Text style={[styles.textStyle, { marginTop: 8 }]}>توضیحات آگهی</Text>
                <TextInput
                    value={this.state.fullDescription}
                    label='شرح کامل آگهی'
                    titleTextStyle={styles.inputLabelStyle}
                    underlineColorAndroid={'#FFFFFF00'}
                    multiline={true}
                    numberOfLines={5}
                    onChangeText={fullDescription => {
                        this.setState({ fullDescription })
                        this.extractHashtags(fullDescription)
                    }}
                    onFocus={() => this.setState({ descriptionFocus: true })}
                    onBlur={() => this.setState({ descriptionFocus: false })}
                    style={{
                        textAlignVertical: 'top',
                        textAlign: 'right',
                        fontSize: RF(2.5),
                        marginTop: hp('1%'),
                        alignSelf: 'center',
                        // height: 10,
                        minHeight: hp('10%'),
                        width: '100%',
                        backgroundColor: '#FFFFFF00',
                        fontFamily: IranYekanMobile,
                        borderColor: this.state.descriptionFocus ? Colors.SECONDARY : Colors.PRIMARY,
                        borderWidth: 1,
                        padding: 4,
                        lineHeight: hp('3%'),
                        marginVertical: wp('2%'),
                        paddingHorizontal: wp('2%')
                    }}
                    labelTextStyle={styles.inputLabelStyle}
                />
                <Text style={[styles.textStyle, { marginVertical: 4, fontSize: RF(1.8), color: Colors.Grays }]}>
                    جزییات و نکات آگهی خود را با دقت بنویسید
                </Text>
                <Text style={{ color: Colors.BlueSky, fontFamily: IranSansMobile, fontSize: RF(2) }}>شما می توانید از هشتگ در متن استفاده کنید به طور مثال #فوتبال</Text>
                {/* <TouchableOpacity onPress={() => this.setState({ isHashtagHelperVisible: true })} >
                    <Text style={{ color: Colors.SECONDARY, fontFamily: IranSansMobile, fontSize: RF(2.5) }}>توضیحات بیشتر</Text>
                </TouchableOpacity> */}
            </View>
        )
    }

    renderStepTwo() {
        return (
            <View style={{ marginVertical: 8, paddingHorizontal: 16 }}>
                <Text style={[styles.textStyle, { marginTop: 8 }]}>عکس آگهی</Text>
                <Text style={[styles.textStyle, { marginVertical: 4, fontSize: RF(1.8), color: Colors.Grays }]}>با افزودن عکس شانس دیدن آگهی شما بیشتر می شود</Text>
                <FlatList
                    inverted
                    showsHorizontalScrollIndicator={false}
                    horizontal={true}
                    ListHeaderComponent={() => (
                        <Ripple onPress={() => this.pickImage()} style={{ justifyContent: 'center', marginStart: wp('2%'), width: wp('25%'), height: wp('25%'), borderWidth: 1, borderColor: Colors.LIGHTGREY, backgroundColor: Colors.WHITE }} >
                            <MCIcons name="image-plus" size={wp('10%')} style={{ alignSelf: 'center' }} />
                            <Text style={[styles.textStyle, { alignSelf: 'center', color: Colors.Grays }]}>افزودن عکس</Text>
                        </Ripple>
                    )}
                    data={this.state.images}
                    renderItem={({ item }) => (
                        <View style={{ justifyContent: 'flex-end', marginStart: wp('2%'), width: wp('25%'), height: wp('25%'), borderWidth: 1, borderColor: Colors.LIGHTGREY }} >
                            <Image source={{ uri: item.imagePath }} size={wp('10%')} style={{ width: wp('25%'), height: wp('25%') }} />
                            <Ripple
                                onPress={() => this.onDeleteImage(item)}
                                style={{ position: 'absolute', alignSelf: 'flex-end', padding: wp('1%'), marginTop: wp('2%') }}>
                                <MCIcons name="close-circle" color={Colors.RED} size={20} />
                            </Ripple>
                        </View>
                    )}
                />
            </View>
        )
    }

    renderStepThree() {
        return (
            <View style={{ marginTop: 8, paddingHorizontal: 16 }}>
                <Text style={[styles.textStyle, { marginVertical: wp('2%') }]}>انتخاب شهر</Text>
                <TouchableOpacity onPress={() => setStore({ isStateModalVisible: true })}>
                    <Text style={{ fontFamily: IranSansMobile }}>
                        {
                            getStore('isStateSelected') ?
                                getStore('selectedStateString')
                                :
                                'شهر انتخاب نشده است'
                        }
                    </Text>
                </TouchableOpacity>
                <TextField
                    label='آدرس'
                    style={{
                        fontFamily: IranYekanMobile,
                    }}
                    value={this.state.address ? this.state.address : ''}
                    onChangeText={address => this.setState({ address })}
                    affixTextStyle={{ fontSize: RF(2.5) }}
                    placeholder="آدرس مربوط به آگهی را به صورت متنی وارد کنید"
                    titleTextStyle={styles.inputLabelStyle}
                    labelTextStyle={styles.inputLabelStyle}
                />
                <TouchableOpacity onPress={() => setStore({ isLocationModalVisible: true })} >
                    <Text style={{ color: Colors.SECONDARY, fontFamily: IranSansMobile, fontSize: RF(2) }}>انتخاب موقعیت از روی نقشه</Text>
                </TouchableOpacity>
                <LocationModal />

            </View>
        )
    }

    toggleModal() {
        if (!this.state.isResumeModalVisible && this.state.arrSelectedResume.length >= 3) {
            ViacToast.showToast('حداکثر 3 رزومه قابل انتخاب است.', 2000)
            return false;
        }
        this.setState({ isResumeModalVisible: !this.state.isResumeModalVisible, arrResume: this.state.arrBaseResume })
    }

    pickResume(item) {
        if (this.state.arrSelectedResume.length >= 3) {
            ViacToast.showToast('حداکثر 3 رزومه قابل انتخاب است.', 2000)
            return false;
        }
        let prevSelectedResume = this.state.arrSelectedResume
        prevSelectedResume.push(item)
        console.log('Resume ', prevSelectedResume)
        this.setState({ arrSelectedResume: prevSelectedResume, isResumeModalVisible: false })
    }

    dropResume(item) {
        if (this.state.arrSelectedResume.length === 0)
            return false;
        let arr = this.state.arrSelectedResume;
        for (let i = 0; i < arr.length; i++) {
            if (item.id === arr[i].id) {
                arr.splice(i, 1);
            }
        }
        this.setState({ arrSelectedResume: arr })
    }

    resumeFilter(txt) {
        if (this.searchWaiting)
            clearTimeout(this.searchWaiting)
        this.searchWaiting = setTimeout(() => {
            ResumeAPI.getAll("", txt)
                .then(response => {
                    this.setState({ arrResume: response.data.data.data })
                }).catch((err) => {
                    this.setState({ loading: false })
                    if (err.serverMessage.response.data.message) {
                        ViacToast.showToast(err.serverMessage.response.data.message, 2000)
                    }
                })
        }, 1000)
    }
    renderStepFourModal() {
        return (
            <Modal
                useNativeDriver={true}
                swipeDirection={'down'}
                // onSwipeComplete={() => this.setState({ isResumeModalVisible: false })}
                onBackButtonPress={() => { this.setState({ isResumeModalVisible: false }) }}
                onBackdropPress={() => { this.setState({ isResumeModalVisible: false }) }}
                isVisible={this.state.isResumeModalVisible}
                style={{ margin: 0, marginTop: hp('10%'), backgroundColor: Colors.WHITE, flexDirection: 'column', }}>
                <Text style={[styles.faFont, { paddingHorizontal: 8, borderBottomColor: Colors.PRIMARY, borderBottomWidth: 1, height: 48, lineHeight: 48 }]}>انتخاب روزمه : </Text>
                <TextInput
                    placeholder='جستجو...'
                    style={[styles.faFont, { paddingHorizontal: 8, textAlign: 'right', borderColor: Colors.PRIMARY, borderWidth: 1, margin: 8, borderRadius: 8, height: 48 }]}
                    onChangeText={(txt) => { this.resumeFilter(txt) }} />
                <FlatList
                    data={this.state.arrResume}
                    renderItem={({ item }) => {
                        return (
                            <Ripple style={{ flex: 1, justifyContent: 'center', height: 48 }} onPress={() => { this.pickResume(item) }}>
                                <Text style={[styles.faFont, { textAlign: 'center' }]}>{item.user.name + " " + item.user.lastName}</Text>
                            </Ripple>
                        )
                    }}
                />
            </Modal>
        )
    }



    createResumeAndVideoFormData() {
        this.videoFormData = new FormData()
        this.state.arrSelectedResume.map(item => {
            this.videoFormData.append('resumeIds[]', item.id)
        })
        if (this.state.videoData) {
            this.videoFormData.append('video', this.state.videoData)
        }
        this.videoFormData.append('id', this.state.id)
    }

    pickVideo() {

        ImagePicker.openPicker({
            mediaType: "video",

        }).then(async (video) => {
            console.log('VIDEO ', video);
            let info = await ProcessingManager.getVideoInfo(video.path)
            console.log('INFO VIDEO', info);
            if (info.duration <= 60) {
                this.setState({
                    loading: true,
                    loadingTitle: 'در حال فشرده سازی ویدیو'
                })
                const options = {
                    width: video.width,
                    height: video.height,
                    bitrateMultiplier: 10,
                    minimumBitrate: 300000,
                    removeAudio: false, // default is false
                };
                ProcessingManager.compress(video.path, options)
                    .then(data => {
                        this.setState({
                            videoData: {
                                uri: data.source,
                                type: video.mime,
                                name: data.source.substr(data.source.lastIndexOf('/') + 1)
                            }
                        })
                        this.setState({
                            loading: false,
                        })
                        ViacToast.showToast('ویدیو با موفقیت افزوده شد :)', 2000)
                    })
                    .catch(() => {
                        this.setState({
                            loading: false,
                        })
                        ViacToast.showToast('خطا در فشرده سازی ویدیو', 2000)
                    })

            } else {
                setTimeout(() => {
                    ViacToast.showToast('ویدیو شما نباید بیشتر از 1 دقیقه باشد!', 3000)
                }, 2000)
            }
        })
            .catch(() => {
                this.setState({
                    loading: false,
                })
                ViacToast.showToast('خطا در افزودن ویدیو', 2000)
            })
    }

    renderStepFour() {
        return (
            <View style={{ flex: 1, marginTop: 8, paddingHorizontal: 16 }}>
                <Ripple style={{ borderWidth: 1, borderColor: '#333', textAlign: 'center', aspectRatio: 16 / 9, justifyContent: 'center', alignItems: 'center', overflow:'hidden' }} onPress={() => { this.pickVideo() }}>
                    {
                        this.state.videoData ?
                            <Video muted={true} source={{ uri: this.state.videoData.uri }} style={{ aspectRatio: 16 / 9, width: '100%', height: 200 }} resizeMode='contain' />
                            :
                            <Text style={[styles.faFont, {}]}>انتخاب ویدیو</Text>
                    }
                </Ripple>
                <Ripple onPress={() => { this.toggleModal() }} style={{ borderBottomColor: '#333', borderBottomWidth: 1, paddingVertical: 8, }}>
                    <Text style={[{ textAlign: 'center', color: '#666' }, styles.faFont]}>انتخاب رزومه</Text>
                </Ripple>
                <FlatList
                    data={this.state.arrSelectedResume}
                    style={{ flex: 1, backdropColor: 'red' }}
                    renderItem={({ item }) => {
                        return (
                            <View style={{ borderBottomColor: '#666', borderBottomWidth: 1, padding: 4, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={[styles.faFont, { flex: 1 }]}>{item.user.name + " " + item.user.lastName}</Text>
                                <Ripple onPress={() => { this.dropResume(item) }}>
                                    <MCIcons name="close" style={{ fontSize: 18, color: '#d50000' }} />
                                </Ripple>
                            </View>
                        )
                    }}
                />
                {this.renderStepFourModal()}
            </View>
        )
    }

    // renderStepFour() {
    //     return (
    //         <View style={{ flex: 1 }}>
    //             <Text style={[styles.textStyle, { marginVertical: wp('2%') }]}>انتخاب پلکان</Text>
    //             <Text style={[styles.textStyle, { marginVertical: wp('2%'), fontSize: RF(1.8), color: Colors.Grays }]}>
    //                 با انتخاب پیکان مناسب آگهی خود را در معرض دید کاربران قرار دهید
    //             </Text>

    //             <FlatList
    //                 contentContainerStyle={{ flex: 1, width: '100%' }}
    //                 data={this.state.ladders}
    //                 ListEmptyComponent={
    //                     <ActivityIndicator size='small' color={Color.BUTTON_ACCEPT} />
    //                 }
    //                 renderItem={({ item, index }) => (
    //                     <View style={{ width: '95%', alignSelf: 'center', height: null, elevation: 5, backgroundColor: Colors.WHITE, margin: wp('2%') }}>
    //                         <View style={{ flexDirection: 'row' }}>
    //                             <RadioButton
    //                                 key={item.id.toString()}
    //                                 label={item.name}
    //                                 checked={this.state.selectedLadderId === item.id}
    //                                 value={item.id}
    //                                 onCheck={() => {
    //                                     console.log('Selected ladder id', item.id)
    //                                     console.log('Selected ladder id', this.state.ladders)
    //                                     this.state.selectedLadderId = item.id
    //                                     this.setState({
    //                                         selectedLadderId: item.id,
    //                                         ladderDescription: _.find(this.state.ladders, { id: item.id }).description
    //                                     })
    //                                 }}
    //                                 onSelect={(value) => {
    //                                     this.state.selectedLadderId = value
    //                                     this.setState({ selectedLadderId: value, ladderDescription: _.find(this.state.ladders, { id: value }).description })
    //                                 }}
    //                             />
    //                             <Text style={[styles.textStyle, { marginVertical: wp('2%'), padding: wp('2%'), color: Colors.RED }]}>
    //                                 {
    //                                     item.price === 0 ?
    //                                     'رایگان'
    //                                     :
    //                                     `${item.price} تومان`
    //                                 }
    //                             </Text>
    //                         </View>
    //                         <View style={{width: '95%', backgroundColor: Colors.LIGHTGREY, height: 0.5, alignSelf: 'center'}} />
    //                         <Text style={[styles.textStyle, { marginVertical: wp('2%'), padding: wp('2%'), fontSize: RF(1.8), color: Colors.SECONDARY }]}>
    //                             {item.description}
    //                         </Text>
    //                     </View>
    //                 )}
    //             />

    //         </View>

    //     )
    // }

    nextStep() {
        if (this.state.step < this.state.maxStep) {
            this.setState({
                step: this.state.step + 1
            })
        }
    }

    prevStep() {
        if (this.state.step > 1) {
            this.setState({
                step: this.state.step - 1
            })
        }
    }

    render() {
        return (
            <View style={styles.container} >
                <Statusbar light={false} backgroundColor={Colors.WHITE} />
                <Toolbar
                    ref={ref => this.toolbar = ref}
                    leftElement={I18nManager.isRTL ? "arrow-forward" : "arrow-back"}
                    centerElement={this.props.navigation.getParam('type') !== 'edit' ? 'افزودن آگهی' : 'ویرایش آگهی'}
                    onLeftElementPress={() => {
                        NavigationController.goBack()
                    }}
                    onRightElementPress={(label) => { NavigationController.openDrawer() }}
                />

                <ScrollView
                    keyboardShouldPersistTaps="always"
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    style={styles.inputContainer}>
                    <View style={{}}>
                        {this.state.step === 1 ? this.renderStepOne() : null}
                        {this.state.step === 2 ? this.renderStepTwo() : null}
                        {this.state.step === 3 ? this.renderStepThree() : null}
                        {this.state.step === 4 ? this.renderStepFour() : null}

                        {
                            this.state.step === this.state.maxStep ?
                                <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'space-between', backdropColor: 'blue' }}>
                                    {/* <Ripple style={{ backgroundColor: Colors.SECONDARY, padding: 5, borderRadius: wp('2%') }}>
                                <Text style={styles.buttonTextStyle}>ذخیره به عنوان پیش نویس</Text>
                            </Ripple> */}
                                    <Ripple
                                        onPress={() => this.prevStep()}
                                        style={{ backgroundColor: Color.BUTTON_ACCEPT, flex: 1, height: 48, justifyContent: 'center' }}>
                                        <Text style={styles.buttonTextStyle}>
                                            قبلی
                                        </Text>
                                    </Ripple>
                                    <View style={{ width: 1, backgroundColor: 'white' }} />
                                    <Ripple
                                        onPress={this.onAdvertiseSubmit.bind(this)}
                                        style={{ backgroundColor: Color.BUTTON_ACCEPT, flex: 1, height: 48, justifyContent: 'center' }}>
                                        <Text style={styles.buttonTextStyle}>
                                            {
                                                this.props.navigation.getParam('type') === 'edit' ?
                                                    'ویرایش آگهی'
                                                    :
                                                    'ارسال آگهی'
                                            }
                                        </Text>
                                    </Ripple>

                                </View>
                                :
                                <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'space-between', backdropColor: 'red' }}>
                                    {
                                        this.state.step != 1 ?
                                            <Ripple
                                                onPress={() => this.prevStep()}
                                                style={{ backgroundColor: Color.BUTTON_ACCEPT, flex: 1, height: 48, justifyContent: 'center' }}>
                                                <Text style={styles.buttonTextStyle}>
                                                    قبلی
                                                </Text>
                                            </Ripple>
                                            :
                                            null
                                    }
                                    {
                                        this.state.step !== 1 ? (
                                            <View style={{ width: 1, backgroundColor: 'white' }} />
                                        ) : null
                                    }
                                    <Ripple
                                        onPress={() => this.nextStep()}
                                        style={{ backgroundColor: Color.BUTTON_ACCEPT, flex: 1, height: 48, justifyContent: 'center' }}>
                                        <Text style={styles.buttonTextStyle}>
                                            بعدی
                                        </Text>
                                    </Ripple>
                                </View>
                        }
                    </View>
                </ScrollView>
                <ProgressDialog
                    visible={this.state.loading}
                    title={this.state.loadingTitle}
                    message="لطفا کمی صبر کنید ..."
                >
                    <StatusBar backgroundColor="rgba(0,0,0,0.7)" barStyle="light-content" />
                </ProgressDialog>
                <Modal backdropColor='#000000' backdropOpacity={0.5} style={{ marginHorizontal: wp('10%'), padding: wp('2%'), marginVertical: hp('40%'), borderColor: Colors.PRIMARY, borderWidth: wp('1%'), borderRadius: wp('1%'), backgroundColor: Colors.WHITE }} visible={this.state.isHashtagHelperVisible} onRequestClose={() => this.setState({ isHashtagHelperVisible: false })} onBackdropPress={() => this.setState({ isHashtagHelperVisible: false })} >
                    <StatusBar backgroundColor="rgba(0,0,0,0.7)" barStyle="light-content" />

                    <Text style={{ fontFamily: IranSansMobile, color: Colors.PRIMARY, fontSize: RF(2), lineHeight: wp('6%') }}>شما با هشتگ زدن در توضیحات آگهی خود میتوانید در جست و جو ها بیشتر دیده شوید فقط لطفا از حروف مجاز در هشتگ خود استفاده کنید برای مثال #فوتبال #والیبال_ایران</Text>
                </Modal>
                <StateModal />
                <Toast backgroundColor={Colors.SECONDARY} position="bottom" ref={ref => ViacToast.setToastRef(ref)} style={styles.faFont} />
            </View>
        )
    }
}

const reshte = [
    { id: 'استخر', email: 'john@gmail.com' },
    { id: 'فوتبال', email: 'doe@gmail.com' }
]

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '',
    },
    itemContainer: {
        paddingVertical: wp('3%'),
        paddingHorizontal: wp('5%'),
        marginVertical: wp('1%'),
        marginHorizontal: wp('3%'),
        elevation: 3,
        backgroundColor: Colors.WHITE
    },
    textStyle: {
        fontFamily: IranSansMobile,
        color: Colors.PRIMARY
    },
    inputContainer: {
        // flex: 1
    },
    inputLabelStyle: {
        fontFamily: IranSansMobile,
    },
    faFont: {
        fontFamily: IranSansMobile,
    },
    buttonTextStyle: { color: Colors.WHITE, fontFamily: IranSansMobile, fontSize: RF(2.5), textAlign: 'center' }
})

const mstp = state => ({ isStateSelected: state.isStateSelected, selectedStateString: state.selectedStateString })
export default connect(mstp)(AddAdvertisement)