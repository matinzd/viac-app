import React, { Component } from 'react'
import { View, Text, I18nManager, ActivityIndicator, ScrollView, TextInput, Picker, TouchableOpacity, Keyboard } from 'react-native'
import Colors from '../../../constants/Colors';
import { Toolbar, RadioButton, Checkbox } from 'react-native-material-ui';
import NavigationController from '../../../controllers/NavigationController';
import { Statusbar } from '../../../components/viac';
import { IranYekanMobile, IranSansMobile, IranSansMobileBold } from '../../../constants/Fonts';
import RF from 'react-native-responsive-fontsize';
import Color from '../../../styles/Color';
import MIcons from 'react-native-vector-icons/MaterialIcons'
import Ripple from 'react-native-material-ripple';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Steps from 'react-native-steps';
import ViewPager from "@react-native-community/viewpager";
import FeIcons from 'react-native-vector-icons/Feather'
import AntDIcons from 'react-native-vector-icons/AntDesign'
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import CategoryAPI from '../../../webservices/CategoryAPI';
import { getStore, setStore } from 'trim-redux';
import { connect } from 'trim-redux'
import _ from 'lodash'
import InputScrollView from 'react-native-input-scroll-view';

class SportsInfo extends Component {


    constructor(props) {
        super(props)

        this.state = {
            type: this.props.type,
            categories: [],
            sportTypes: [],
            ages: [],
            selectedAges: [],
            isCheckedAges: [],
            selectedExperiences: [],
            isExperiencesChecked: [],
            matchTypes: [],
            experiences: [],
            coachDegrees: [],
            coachDegree: 0,
            matchType: 0,
            experience: 0,
            sportType: 0,
            age: 0,
            categoryId: 0,
            weight: '0',
            height: '0',
            coach: '',
            degreeOfEducation: '',
            placeName: '',
            major: '',
            aboutMe: '',
        }
        this.initialized = false
    }

    componentDidMount() {
        this.loadEnums()
        CategoryAPI.getAll().then((response) => {
            this.setState({
                categories: response.data.data
            })
        })
    }

    loadEnums() {
        let enums = getStore('enums');
        console.log(enums)
        this.setState({
            sportTypes: enums.sportType,
            ages: enums.ages,
            matchTypes: enums.matchType,
            experiences: enums.experience,
            coachDegrees: enums.coachDegree,
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.initialData && !this.initialized) {
            if (this.props.initialData.info.categoryId != 0) {
                this.initialized = true
            }
            console.log('next props in sport info ', this.props.initialData.info)
            // setTimeout(() => {
            let expArr = []
            let isCheckedExp = []
            let expAge = []
            let isCheckedAge = []
            this.props.initialData.info.selectedExperiences.map(item => {
                expArr.push(parseInt(item.id))
                return item
            })
            this.state.experiences.map((item, index) => {
                var flag = false
                expArr.map(id => {
                    if (id === item.id) {
                        flag = true
                    }
                })
                isCheckedExp.push(flag)
                return item
            })

            this.props.initialData.info.selectedAges.map(item => {
                expAge.push(parseInt(item.id))
                return item
            })
            this.state.ages.map((item, index) => {
                var flag = false
                expAge.map(id => {
                    if (id === item.id) {
                        flag = true
                    }
                })
                isCheckedAge.push(flag)
                return item
            })

            this.setState({

                selectedAges: expAge,
                isCheckedAges: isCheckedAge,
                selectedExperiences: expArr,
                isExperiencesChecked: isCheckedExp,
                coachDegree: this.props.initialData.info.coachDegree,
                matchType: this.props.initialData.info.matchType,
                sportType: this.props.initialData.info.sportType,
                categoryId: this.props.initialData.info.categoryId,
                weight: String(this.props.initialData.info.weight),
                height: String(this.props.initialData.info.height),
                coach: this.props.initialData.info.coach,
                degreeOfEducation: this.props.initialData.info.degreeOfEducation,
                placeName: this.props.initialData.info.placeName,
                major: this.props.initialData.info.major,
                aboutMe: this.props.initialData.info.aboutMe,
            })
            // }, 1000)
        }
    }

    onDataChanged() {
        this.props.onInfoChanged({
            categoryId: this.state.categoryId,
            matchType: this.state.matchType,
            experience: this.state.experience,
            sportType: this.state.sportType,
            age: this.state.age,
            selectedAges: this.state.selectedAges,
            coachDegree: this.state.coachDegree,
            weight: this.state.weight,
            height: this.state.height,
            coach: this.state.coach,
            degreeOfEducation: this.state.degreeOfEducation,
            placeName: this.state.placeName,
            major: this.state.major,
            aboutMe: this.state.aboutMe,
            selectedExperiences: this.state.selectedExperiences,
        })
    }

    renderPlayerFields() {
        return (
            <View>

                {/* <View style={{flexDirection: 'row', justifyContent: 'space-between'}}> */}
                <Text style={{ fontFamily: IranSansMobileBold, color: Colors.SECONDARY }}>رشته ورزشی</Text>
                <Picker
                    label='رشته ورزشی'
                    itemStyle={{ fontFamily: IranSansMobile }}
                    selectedValue={this.state.categoryId}
                    onValueChange={(itemValue, itemIndex) => {
                        console.log(itemValue, itemIndex)
                        this.setState({
                            categoryId: itemValue
                        })
                        this.onDataChanged()
                    }}
                    mode="dialog"
                >
                    {
                        this.state.categories.map(item => (
                            <Picker.Item value={item.id} key={item.id} label={item.name} />
                        ))
                    }
                </Picker>
                <Text style={{ fontFamily: IranSansMobileBold, color: Colors.SECONDARY }}>نوع رشته ورزشی</Text>
                <Picker
                    // label='رشته ورزشی'
                    itemStyle={{ fontFamily: IranSansMobile }}
                    selectedValue={this.state.sportType}
                    onValueChange={(itemValue, itemIndex) => {
                        console.log(itemValue, itemIndex)
                        this.setState({
                            sportType: itemValue
                        })
                        this.onDataChanged()
                    }}
                    mode="dialog"
                >
                    {
                        this.state.sportTypes.map(item => (
                            <Picker.Item value={item.id} key={item.id} label={item.title} />
                        ))
                    }
                </Picker>
                {/* </View> */}
                <Text style={{ fontFamily: IranSansMobileBold, color: Colors.SECONDARY }}>رده سنی</Text>
                {/* <Picker
                    // label='رشته ورزشی'
                    itemStyle={{ fontFamily: IranSansMobile }}
                    selectedValue={this.state.age}
                    onValueChange={(itemValue, itemIndex) => {
                        console.log(itemValue, itemIndex)
                        this.setState({
                            age: itemValue
                        })
                        this.onDataChanged()
                    }}
                    mode="dialog"
                >
                    {
                        this.state.ages.map(item => (
                            <Picker.Item value={item.id} key={item.id} label={item.title} />
                        ))
                    }
                </Picker> */}
                {/* <View style={{ flexDirection: 'row' }}> */}
                {
                    this.state.ages.map((item, index) => (
                        <Checkbox
                            key={index.toString()}
                            style={{ container: { flexDirection: 'row' }, label: { fontFamily: IranSansMobile } }}
                            onCheck={(isChecked) => {
                                if (isChecked) {
                                    this.state.selectedAges[index] = item.id
                                    this.state.isCheckedAges[index] = true
                                } else {
                                    this.state.selectedAges.splice(index, 1)
                                    this.state.isCheckedAges[index] = false
                                }
                                console.log('Selected ages ', this.state.selectedAges)
                                this.state.selectedAges = this.state.selectedAges.filter((value) => {
                                    return value != null && value != undefined
                                })
                                this.setState({
                                    selectedAges: this.state.selectedAges
                                })
                                this.onDataChanged()
                            }}
                            label={item.title}
                            value={item.id}
                            checked={this.state.isCheckedAges[index]}
                        />
                    ))
                }
                {/* </View> */}
                <Text style={{ fontFamily: IranSansMobileBold, color: Colors.SECONDARY }}>شرکت در مسابقات</Text>
                <Picker
                    // label='رشته ورزشی'
                    itemStyle={{ fontFamily: IranSansMobile }}
                    selectedValue={this.state.matchType}
                    onValueChange={(itemValue, itemIndex) => {
                        console.log(itemValue, itemIndex)
                        this.setState({
                            matchType: itemValue
                        })
                        this.onDataChanged()
                    }}
                    mode="dialog"
                >
                    {
                        this.state.matchTypes.map(item => (
                            <Picker.Item value={item.id} key={item.id} label={item.title} />
                        ))
                    }
                </Picker>
                <Text style={{ fontFamily: IranSansMobileBold, color: Colors.SECONDARY }}>سابقه فعالیت</Text>
                {
                    this.state.experiences.map((item, index) => (
                        <Checkbox
                            key={index.toString()}
                            style={{ container: { flexDirection: 'row' }, label: { fontFamily: IranSansMobile } }}
                            onCheck={(isChecked) => {
                                if (isChecked) {
                                    this.state.selectedExperiences[index] = item.id
                                    this.state.isExperiencesChecked[index] = true
                                } else {
                                    this.state.selectedExperiences.splice(index, 1)
                                    this.state.isExperiencesChecked[index] = false
                                }
                                this.state.selectedExperiences = this.state.selectedExperiences.filter((value) => {
                                    return value != null && value != undefined
                                })
                                this.setState({
                                    selectedExperiences: this.state.selectedExperiences
                                })
                                this.onDataChanged()
                            }}
                            label={item.title}
                            value={item.id}
                            checked={this.state.isExperiencesChecked[index]}
                        />
                    ))
                }
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                    <View style={{
                        borderWidth: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: wp('12%'), borderRadius: wp('1%'), width: '40%', borderColor: Color.SECONDARY, paddingHorizontal: wp('3%'), marginTop: wp('3%')
                    }}>
                        <AntDIcons name='totop' size={wp('6%')} color={Colors.SECONDARY} />
                        <TextInput
                            onChangeText={height => {
                                this.state.height = height
                                this.onDataChanged()
                            }}
                            defaultValue={this.state.height}
                            keyboardType='number-pad'
                            maxLength={3}
                            placeholder='قد (سانتی متر)'
                            style={{ flex: 1, fontFamily: IranSansMobile, fontSize: RF(2), textAlign: 'center' }} />
                    </View>
                    <View style={{
                        borderWidth: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: wp('12%'), borderRadius: wp('1%'), width: '40%', borderColor: Color.SECONDARY, paddingHorizontal: wp('3%'), marginTop: wp('3%')
                    }}>
                        <MCIcons name='weight' size={wp('6%')} color={Colors.SECONDARY} />
                        <TextInput

                            onChangeText={weight => {
                                this.state.weight = weight
                                this.onDataChanged()
                            }}
                            defaultValue={this.state.weight}
                            keyboardType='number-pad'
                            maxLength={3}
                            placeholder='وزن (کیلوگرم)'
                            style={{ flex: 1, fontFamily: IranSansMobile, fontSize: RF(2), textAlign: 'center' }} />
                    </View>
                </View>

                <View style={{
                    borderWidth: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: wp('12%'), borderRadius: wp('1%'), width: '100%', borderColor: Color.SECONDARY, paddingHorizontal: wp('3%'), marginTop: wp('3%')
                }}>
                    <MCIcons name='teach' size={wp('6%')} color={Colors.SECONDARY} />
                    <TextInput
                        defaultValue={this.state.coach}
                        onChangeText={coach => {
                            this.state.coach = coach
                            this.onDataChanged()
                        }}
                        placeholder='مربی'
                        style={{ flex: 1, fontFamily: IranSansMobile }} />
                </View>
                <View style={{
                    borderWidth: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: wp('12%'), borderRadius: wp('1%'), width: '100%', borderColor: Color.SECONDARY, paddingHorizontal: wp('3%'), marginTop: wp('3%')
                }}>
                    <MIcons name='chat' size={wp('6%')} color={Colors.SECONDARY} />
                    <TextInput
                        defaultValue={this.state.degreeOfEducation}
                        onChangeText={degreeOfEducation => {
                            this.state.degreeOfEducation = degreeOfEducation
                            this.onDataChanged()
                        }}
                        placeholder='مدرک تحصیلی'
                        style={{ flex: 1, fontFamily: IranSansMobile }} />
                </View>
                <View style={{
                    borderWidth: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: wp('12%'), borderRadius: wp('1%'), width: '100%', borderColor: Color.SECONDARY, paddingHorizontal: wp('3%'), marginTop: wp('3%')
                }}>
                    <MCIcons name='office-building' size={wp('6%')} color={Colors.SECONDARY} />
                    <TextInput
                        defaultValue={this.state.placeName}
                        onChangeText={placeName => {
                            this.state.placeName = placeName
                            this.onDataChanged()
                        }}
                        placeholder='نام محل تمرین فعلی'
                        style={{ flex: 1, fontFamily: IranSansMobile }} />
                </View>
            </View>
        )
    }

    renderRefreeFields() {
        return (
            <View>

                {/* <View style={{flexDirection: 'row', justifyContent: 'space-between'}}> */}
                <Text style={{ fontFamily: IranSansMobileBold, color: Colors.SECONDARY }}>رشته ورزشی</Text>
                <Picker
                    label='رشته ورزشی'
                    itemStyle={{ fontFamily: IranSansMobile }}
                    selectedValue={this.state.categoryId}
                    onValueChange={(itemValue, itemIndex) => {
                        console.log(itemValue, itemIndex)
                        this.setState({
                            categoryId: itemValue
                        })
                        this.onDataChanged()
                    }}
                    mode="dialog"
                >
                    {
                        this.state.categories.map(item => (
                            <Picker.Item value={item.id} key={item.id} label={item.name} />
                        ))
                    }
                </Picker>
                <Text style={{ fontFamily: IranSansMobileBold, color: Colors.SECONDARY }}>نوع رشته ورزشی</Text>
                <Picker
                    // label='رشته ورزشی'
                    itemStyle={{ fontFamily: IranSansMobile }}
                    selectedValue={this.state.sportType}
                    onValueChange={(itemValue, itemIndex) => {
                        console.log(itemValue, itemIndex)
                        this.setState({
                            sportType: itemValue
                        })
                        this.onDataChanged()
                    }}
                    mode="dialog"
                >
                    {
                        this.state.sportTypes.map(item => (
                            <Picker.Item value={item.id} key={item.id} label={item.title} />
                        ))
                    }
                </Picker>
                {/* </View> */}
                <Text style={{ fontFamily: IranSansMobileBold, color: Colors.SECONDARY }}>رده سنی</Text>
                {
                    this.state.ages.map((item, index) => (

                        <Checkbox
                            key={Math.random().toString()}
                            style={{ container: { flexDirection: 'row' }, label: { fontFamily: IranSansMobile } }}
                            onCheck={(isChecked) => {
                                if (isChecked) {
                                    this.state.selectedAges[index] = item.id
                                    this.state.isCheckedAges[index] = true
                                } else {
                                    this.state.selectedAges.splice(index, 1)
                                    this.state.isCheckedAges[index] = false
                                }
                                console.log('Selected ages ', this.state.selectedAges)
                                this.state.selectedAges = this.state.selectedAges.filter((value) => {
                                    return value != null && value != undefined
                                })
                                this.setState({
                                    selectedAges: this.state.selectedAges
                                })
                                this.onDataChanged()
                            }}
                            label={item.title}
                            value={item.id}
                            checked={this.state.isCheckedAges[index]}
                        />
                    ))
                }
                <Text style={{ fontFamily: IranSansMobileBold, color: Colors.SECONDARY }}>سطح داوری در مسابقات</Text>
                <Picker
                    // label='رشته ورزشی'
                    itemStyle={{ fontFamily: IranSansMobile }}
                    selectedValue={this.state.matchType}
                    onValueChange={(itemValue, itemIndex) => {
                        console.log(itemValue, itemIndex)
                        this.setState({
                            matchType: itemValue
                        })
                        this.onDataChanged()
                    }}
                    mode="dialog"
                >
                    {
                        this.state.matchTypes.map(item => (
                            <Picker.Item value={item.id} key={item.id} label={item.title} />
                        ))
                    }
                </Picker>
                <Text style={{ fontFamily: IranSansMobileBold, color: Colors.SECONDARY }}>سابقه فعالیت</Text>
                {
                    this.state.experiences.map((item, index) => (
                        <Checkbox
                            key={index.toString()}
                            style={{ container: { flexDirection: 'row' }, label: { fontFamily: IranSansMobile } }}
                            onCheck={(isChecked) => {
                                if (isChecked) {
                                    this.state.selectedExperiences[index] = item.id
                                    this.state.isExperiencesChecked[index] = true
                                } else {
                                    this.state.selectedExperiences.splice(index, 1)
                                    this.state.isExperiencesChecked[index] = false
                                }
                                this.state.selectedExperiences = this.state.selectedExperiences.filter((value) => {
                                    return value != null && value != undefined
                                })
                                this.setState({
                                    selectedExperiences: this.state.selectedExperiences
                                })
                                this.onDataChanged()
                            }}
                            label={item.title}
                            value={item.id}
                            checked={this.state.isExperiencesChecked[index]}
                        />
                    ))
                }
                {/* <Text style={{ fontFamily: IranSansMobileBold, color: Colors.SECONDARY }}>درجه داوری</Text>
                <Picker
                    itemStyle={{ fontFamily: IranSansMobile }}
                    selectedValue={this.state.coachDegree}
                    onValueChange={(itemValue, itemIndex) => {
                        console.log(itemValue, itemIndex)
                        this.setState({
                            coachDegree: itemValue
                        })
                        this.onDataChanged()
                    }}
                    mode="dialog"
                >
                    {
                        this.state.coachDegrees.map(item => (
                            <Picker.Item value={item.id} key={item.id} label={item.title} />
                        ))
                    }
                </Picker> */}

                <View style={{
                    borderWidth: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: wp('12%'), borderRadius: wp('1%'), width: '100%', borderColor: Color.SECONDARY, paddingHorizontal: wp('3%'), marginTop: wp('3%')
                }}>
                    <MIcons name='description' size={wp('6%')} color={Colors.SECONDARY} />
                    <TextInput
                        defaultValue={this.state.coachDegree}
                        onChangeText={coachDegree => {
                            this.state.coachDegree = coachDegree
                            this.onDataChanged()
                        }}
                        placeholder='درجه داوری'
                        style={{ flex: 1, fontFamily: IranSansMobile }} />
                </View>


                <View style={{
                    borderWidth: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: wp('12%'), borderRadius: wp('1%'), width: '100%', borderColor: Color.SECONDARY, paddingHorizontal: wp('3%'), marginTop: wp('3%')
                }}>
                    <MIcons name='assignment-turned-in' size={wp('6%')} color={Colors.SECONDARY} />
                    <TextInput
                        defaultValue={this.state.major}
                        onChangeText={major => {
                            this.state.major = major
                            this.onDataChanged()
                        }}
                        placeholder='رشته تحصیلی'
                        style={{ flex: 1, fontFamily: IranSansMobile }} />
                </View>
                <View style={{
                    borderWidth: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: wp('12%'), borderRadius: wp('1%'), width: '100%', borderColor: Color.SECONDARY, paddingHorizontal: wp('3%'), marginTop: wp('3%')
                }}>
                    <MIcons name='chat' size={wp('6%')} color={Colors.SECONDARY} />
                    <TextInput
                        defaultValue={this.state.degreeOfEducation}
                        onChangeText={degreeOfEducation => {
                            this.state.degreeOfEducation = degreeOfEducation
                            this.onDataChanged()
                        }}
                        placeholder='مدرک تحصیلی'
                        style={{ flex: 1, fontFamily: IranSansMobile }} />
                </View>
                <View style={{
                    borderWidth: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: wp('12%'), borderRadius: wp('1%'), width: '100%', borderColor: Color.SECONDARY, paddingHorizontal: wp('3%'), marginTop: wp('3%')
                }}>
                    <MCIcons name='office-building' size={wp('6%')} color={Colors.SECONDARY} />
                    <TextInput
                        defaultValue={this.state.placeName}
                        onChangeText={placeName => {
                            this.state.placeName = placeName
                            this.onDataChanged()
                        }}
                        placeholder='نام محل تمرین فعلی'
                        style={{ flex: 1, fontFamily: IranSansMobile }} />
                </View>

            </View>
        )
    }
    renderCoachFields() {
        return (
            <View>

                {/* <View style={{flexDirection: 'row', justifyContent: 'space-between'}}> */}
                <Text style={{ fontFamily: IranSansMobileBold, color: Colors.SECONDARY }}>رشته ورزشی</Text>
                <Picker
                    label='رشته ورزشی'
                    itemStyle={{ fontFamily: IranSansMobile }}
                    selectedValue={this.state.categoryId}
                    onValueChange={(itemValue, itemIndex) => {
                        console.log(itemValue, itemIndex)
                        this.setState({
                            categoryId: itemValue
                        })
                        this.onDataChanged()
                    }}
                    mode="dialog"
                >
                    {
                        this.state.categories.map(item => (
                            <Picker.Item value={item.id} key={item.id} label={item.name} />
                        ))
                    }
                </Picker>
                <Text style={{ fontFamily: IranSansMobileBold, color: Colors.SECONDARY }}>نوع رشته ورزشی</Text>
                <Picker
                    // label='رشته ورزشی'
                    itemStyle={{ fontFamily: IranSansMobile }}
                    selectedValue={this.state.sportType}
                    onValueChange={(itemValue, itemIndex) => {
                        console.log(itemValue, itemIndex)
                        this.setState({
                            sportType: itemValue
                        })
                        this.onDataChanged()
                    }}
                    mode="dialog"
                >
                    {
                        this.state.sportTypes.map(item => (
                            <Picker.Item value={item.id} key={item.id} label={item.title} />
                        ))
                    }
                </Picker>

                {/* </View> */}
                <Text style={{ fontFamily: IranSansMobileBold, color: Colors.SECONDARY }}>مربی گری رده سنی</Text>
                {
                    this.state.ages.map((item, index) => (
                        <Checkbox
                            key={index.toString()}
                            style={{ container: { flexDirection: 'row' }, label: { fontFamily: IranSansMobile } }}
                            onCheck={(isChecked) => {
                                if (isChecked) {
                                    this.state.selectedAges[index] = item.id
                                    this.state.isCheckedAges[index] = true
                                } else {
                                    this.state.selectedAges.splice(index, 1)
                                    this.state.isCheckedAges[index] = false
                                }
                                console.log('Selected ages ', this.state.selectedAges)
                                this.state.selectedAges = this.state.selectedAges.filter((value) => {
                                    return value != null && value != undefined
                                })
                                this.setState({
                                    selectedAges: this.state.selectedAges
                                })
                                this.onDataChanged()
                            }}
                            label={item.title}
                            value={item.id}
                            checked={this.state.isCheckedAges[index]}
                        />
                    ))
                }
                <Text style={{ fontFamily: IranSansMobileBold, color: Colors.SECONDARY }}>سطح مربی گری در مسابقات</Text>
                <Picker
                    // label='رشته ورزشی'
                    itemStyle={{ fontFamily: IranSansMobile }}
                    selectedValue={this.state.matchType}
                    onValueChange={(itemValue, itemIndex) => {
                        console.log(itemValue, itemIndex)
                        this.setState({
                            matchType: itemValue
                        })
                        this.onDataChanged()
                    }}
                    mode="dialog"
                >
                    {
                        this.state.matchTypes.map(item => (
                            <Picker.Item value={item.id} key={item.id} label={item.title} />
                        ))
                    }
                </Picker>
                <Text style={{ fontFamily: IranSansMobileBold, color: Colors.SECONDARY }}>سابقه فعالیت</Text>
                {
                    this.state.experiences.map((item, index) => (
                        <Checkbox
                            key={index.toString()}
                            style={{ container: { flexDirection: 'row' }, label: { fontFamily: IranSansMobile } }}
                            onCheck={(isChecked) => {
                                if (isChecked) {
                                    this.state.selectedExperiences[index] = item.id
                                    this.state.isExperiencesChecked[index] = true
                                } else {
                                    this.state.selectedExperiences.splice(index, 1)
                                    this.state.isExperiencesChecked[index] = false
                                }
                                this.state.selectedExperiences = this.state.selectedExperiences.filter((value) => {
                                    return value != null && value != undefined
                                })
                                this.setState({
                                    selectedExperiences: this.state.selectedExperiences
                                })
                                this.onDataChanged()
                            }}
                            label={item.title}
                            value={item.id}
                            checked={this.state.isExperiencesChecked[index]}
                        />
                    ))
                }

                <View style={{
                    borderWidth: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: wp('12%'), borderRadius: wp('1%'), width: '100%', borderColor: Color.SECONDARY, paddingHorizontal: wp('3%'), marginTop: wp('3%')
                }}>
                    <MIcons name='description' size={wp('6%')} color={Colors.SECONDARY} />
                    <TextInput
                        defaultValue={this.state.coachDegree}
                        onChangeText={coachDegree => {
                            this.state.coachDegree = coachDegree
                            this.onDataChanged()
                        }}
                        placeholder='درجه مربی گری'
                        style={{ flex: 1, fontFamily: IranSansMobile }} />
                </View>

                <View style={{
                    borderWidth: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: wp('12%'), borderRadius: wp('1%'), width: '100%', borderColor: Color.SECONDARY, paddingHorizontal: wp('3%'), marginTop: wp('3%')
                }}>
                    <MIcons name='assignment-turned-in' size={wp('6%')} color={Colors.SECONDARY} />
                    <TextInput
                        defaultValue={this.state.major}
                        onChangeText={major => {
                            this.state.major = major
                            this.onDataChanged()
                        }}
                        placeholder='رشته تحصیلی'
                        style={{ flex: 1, fontFamily: IranSansMobile }} />
                </View>
                <View style={{
                    borderWidth: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: wp('12%'), borderRadius: wp('1%'), width: '100%', borderColor: Color.SECONDARY, paddingHorizontal: wp('3%'), marginTop: wp('3%')
                }}>
                    <MIcons name='chat' size={wp('6%')} color={Colors.SECONDARY} />
                    <TextInput
                        defaultValue={this.state.degreeOfEducation}
                        onChangeText={degreeOfEducation => {
                            this.state.degreeOfEducation = degreeOfEducation
                            this.onDataChanged()
                        }}
                        placeholder='مدرک تحصیلی'
                        style={{ flex: 1, fontFamily: IranSansMobile }} />
                </View>
                <View style={{
                    borderWidth: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: wp('12%'), borderRadius: wp('1%'), width: '100%', borderColor: Color.SECONDARY, paddingHorizontal: wp('3%'), marginTop: wp('3%')
                }}>
                    <MCIcons name='office-building' size={wp('6%')} color={Colors.SECONDARY} />
                    <TextInput
                        defaultValue={this.state.placeName}
                        onChangeText={placeName => {
                            this.state.placeName = placeName
                            this.onDataChanged()
                        }}
                        placeholder='نام محل تمرین فعلی'
                        style={{ flex: 1, fontFamily: IranSansMobile }} />
                </View>
            </View>
        )
    }

    renderFields() {
        switch (this.props.type) {
            case 0:
                return this.renderPlayerFields()
                break;
            case 1:
                return this.renderRefreeFields()
                break;
            case 2:
                return this.renderCoachFields()
                break;
            default:
                return this.renderPlayerFields()
                break;
        }
    }

    render() {
        return (
            <InputScrollView showsVerticalScrollIndicator={false} style={{ flex: 1, width: '100%' }}>
                {this.renderFields()}
                <TouchableOpacity
                    onPress={() => setStore({ isStateModalVisible: true })}
                    style={{
                        borderWidth: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', height: wp('12%'), borderRadius: wp('1%'), width: '100%', borderColor: Color.SECONDARY, paddingHorizontal: wp('3%'), marginTop: wp('3%')
                    }}>
                    <MIcons name='location-city' size={wp('6%')} color={Colors.SECONDARY} />
                    {
                        !getStore('isStateSelected') ?
                            <Text style={{ fontFamily: IranSansMobile, marginStart: wp('3%') }}>انتخاب شهر و استان</Text>
                            :
                            <Text style={{ fontFamily: IranSansMobile, marginStart: wp('3%') }}>{getStore('selectedStateString')}</Text>
                    }
                </TouchableOpacity>
                <View style={{
                    padding: wp('1%'),
                }}>
                    <Text style={{ fontFamily: IranSansMobileBold, color: Colors.SECONDARY, marginTop: wp('3%') }}>درباره من</Text>
                    <TextInput
                        numberOfLines={5}
                        multiline
                        defaultValue={this.state.aboutMe}
                        style={{ fontFamily: IranSansMobile, fontSize: RF(2), height: hp('20%'), justifyContent: 'flex-start', borderBottomColor: Colors.SECONDARY, borderBottomWidth: 1, textAlignVertical: 'top', textAlign: 'right' }}
                        onChangeText={aboutMe => {
                            this.state.aboutMe = aboutMe
                            this.onDataChanged()
                        }}
                        placeholder='کمی درباره خودتان بنویسید ...' />
                </View>
            </InputScrollView>
        )
    }
}

const mstp = state => (
    {
        isStateSelected: state.isStateSelected,
        selectedStateId: state.selectedStateId,
        selectedStateString: state.selectedStateString,
        isStateModalVisible: state.isStateModalVisible
    }
)
export default connect(mstp)(SportsInfo)
