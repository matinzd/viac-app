import React, { Component } from 'react'
import { View, Text, I18nManager, ActivityIndicator, FlatList, TextInput } from 'react-native'
import Colors from '../../../constants/Colors';
import { Toolbar, RadioButton } from 'react-native-material-ui';
import NavigationController from '../../../controllers/NavigationController';
import { Statusbar } from '../../../components/viac';
import { IranYekanMobile, IranSansMobile } from '../../../constants/Fonts';
import RF from 'react-native-responsive-fontsize';
import Color from '../../../styles/Color';
import MIcons from 'react-native-vector-icons/MaterialIcons'
import Ripple from 'react-native-material-ripple';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Steps from 'react-native-steps';
import ViewPager from "@react-native-community/viewpager";
import FeIcons from 'react-native-vector-icons/Feather'
import ViacToast from '../../../controllers/ViacToast';



export default class Honors extends Component {


    constructor(props) {
        super(props)
        console.log('Initial data in honor ', this.props.initialData)
        this.state = {
            data: [],
            title: '',
            year: ''
        }
    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.initialData) {
            this.setState({
                data: nextProps.initialData
            })
        }
    }


    add(title, year) {
        if (title != '' && year > 1300 && year < 3000) {
            this.state.data.push({
                title: title,
                year: year,
            })
            this.setState({
                data: this.state.data
            })
            this.setState({
                year: '',
                title: ''
            })
            this.onDataChanged()
        } else {
            ViacToast.showToast('لطفا مقادیر خواسته شده را وارد کنید', 2000)
        }
    }

    remove(index) {
        this.state.data.splice(index, 1);
        this.setState({
            data: this.state.data
        })
        this.onDataChanged()
    }

    onDataChanged() {
        this.props.onHonorsChanged(this.state.data)

    }


    render() {
        return (
            <View style={{ flex: 1, width: '100%' }}>
                <View style={{ flexDirection: 'row', width: '100%', marginTop: wp('2%') }}>
                    <View style={{ flex: 1, flexGrow: 2 }}>
                        <Text style={{ fontFamily: IranSansMobile }}>عنوان مقام</Text>
                    </View>
                    <View style={{ flex: 1, marginStart: wp('2%') }}>
                        <Text style={{ fontFamily: IranSansMobile }}>سال افتخار</Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', height: wp('10%'), width: '100%', marginTop: wp('2%') }}>
                    <View style={{ flex: 1, flexGrow: 2 }}>
                        <View
                            style={{
                                borderWidth: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: wp('10%'), borderRadius: wp('1%'), width: '100%', borderColor: Color.SECONDARY, paddingHorizontal: wp('3%'), marginTop: wp('2%')
                            }}>
                            <TextInput
                                onChangeText={title => {
                                    this.setState({
                                        title
                                    })
                                    this.onDataChanged()
                                }}
                                value={this.state.title}
                                placeholder={'برای مثال مقام اول فوتبال استانی'}
                                style={{ flex: 1, fontFamily: IranSansMobile }} />
                        </View>
                    </View>
                    <View style={{ flex: 1, marginStart: wp('2%') }}>
                        <View
                            style={{
                                borderWidth: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: wp('10%'), borderRadius: wp('1%'), width: '100%', borderColor: Color.SECONDARY, paddingHorizontal: wp('3%'), marginTop: wp('2%')
                            }}>
                            <TextInput
                                placeholder='1398'
                                value={this.state.year}
                                onChangeText={year => {
                                    this.setState({
                                        year
                                    })
                                    this.onDataChanged()
                                }}
                                keyboardType='number-pad'
                                maxLength={4}
                                style={{ flex: 1, textAlign: 'center', alignContent: 'center', fontFamily: IranSansMobile }} />
                        </View>
                    </View>
                </View>
                <Ripple onPress={() => this.add(this.state.title, this.state.year)} style={{ width: '100%', backgroundColor: Colors.PRIMARY, marginTop: wp('5%') }}>
                    <Text style={{ fontFamily: IranSansMobile, color: Colors.WHITE, alignSelf: 'center', paddingVertical: wp('2%') }}>ثبت</Text>
                </Ripple>
                <View style={{ height: 1, width: '100%', backgroundColor: Colors.SECONDARY, marginTop: wp('2%') }} />
                <FlatList
                    data={this.state.data}
                    keyboardDismissMode='none'
                    keyboardShouldPersistTaps='handled'
                    showsVerticalScrollIndicator={false}
                    ListEmptyComponent={
                        <Text style={{ alignSelf: 'center', fontFamily: IranSansMobile, marginVertical: wp('5%'), color: Color.BUTTON_ACCEPT }}>
                            افتخاری اضافه نکرده اید
                        </Text>
                    }
                    renderItem={({ item, index }) => (
                        <View style={{ flex: 3, flexDirection: 'row', width: '100%', marginTop: wp('2%') }}>
                            <View style={{ flex: 1, flexGrow: 2 }}>
                                <View
                                    style={{
                                        borderBottomWidth: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: wp('10%'), borderRadius: wp('1%'), width: '100%', borderBottomColor: Color.BUTTON_ACCEPT, paddingHorizontal: wp('3%'), marginTop: wp('2%')
                                    }}>
                                    <Text
                                        numberOfLines={1}
                                        lineBreakMode='tail'
                                        style={{ flex: 1, fontFamily: IranSansMobile }}>
                                        {item.title}
                                    </Text>
                                </View>
                            </View>
                            <View style={{ flex: 1, marginStart: wp('2%') }}>
                                <View
                                    style={{
                                        borderBottomWidth: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: wp('10%'), borderRadius: wp('1%'), width: '100%', borderBottomColor: Color.BUTTON_ACCEPT, paddingHorizontal: wp('3%'), marginTop: wp('2%')
                                    }}>
                                    <Text
                                        style={{ flex: 1, textAlign: 'center', alignContent: 'center', fontFamily: IranSansMobile }}>
                                        {item.year}
                                    </Text>
                                </View>

                            </View>
                            <Ripple rippleContainerBorderRadius={wp('3%')} style={{justifyContent: 'center', marginTop: wp('8%'), marginStart: wp('2%')}} onPress={() => this.remove(index)}>
                                <MIcons name='remove-circle' color={Colors.RED} size={wp('6%')} />
                            </Ripple>
                        </View>
                    )}
                />
            </View>
        )
    }
}
