import React, { Component } from 'react'
import { View, Text, I18nManager, ActivityIndicator, FlatList, RefreshControl } from 'react-native'
import Colors from '../../../constants/Colors';
import { Toolbar } from 'react-native-material-ui';
import NavigationController from '../../../controllers/NavigationController';
import { Statusbar } from '../../../components/viac';
import { IranYekanMobile, IranSansMobile } from '../../../constants/Fonts';
import RF from 'react-native-responsive-fontsize';
import Color from '../../../styles/Color';
import MIcons from 'react-native-vector-icons/MaterialIcons'
import Ripple from 'react-native-material-ripple';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import ResumeAPI from '../../../webservices/ResumeAPI';
import PlayersList from './PlayersList';
import RefreesList from './RefreesList';
import CoachsList from './CoachsList';

export default class ManageResumes extends Component {


    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            data: []
        }
    }

    componentDidMount() {
        this.refreshData()
    }

    refreshData() {
        this.setState({ loading: true })
        ResumeAPI.getMine().then(response => {
            this.setState({
                data: response.data.data,
                loading: false
            })
        })
    }

    renderResumeList(item) {
        switch (item.type.id) {
            case 0: //Bazikon
                return (<PlayersList onDelete={() => this.refreshData()} data={item} />)
            case 1://Davar
                return (<RefreesList onDelete={() => this.refreshData()} data={item} />)
            case 2://Morabi
                return (<CoachsList onDelete={() => this.refreshData()} data={item} />)
            default:
                return (<PlayersList onDelete={() => this.refreshData()} data={item} />)
        }
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
                <Statusbar light={false} backgroundColor={Colors.WHITE} />
                <Toolbar
                    ref={ref => this.toolbar = ref}
                    leftElement={I18nManager.isRTL ? "arrow-forward" : "arrow-back"}
                    centerElement={
                        <Text style={{ alignSelf: 'flex-start', color: Colors.SECONDARY, fontFamily: IranYekanMobile, fontSize: RF(2.5) }}>مدیریت رزومه ها</Text>
                    }
                    onLeftElementPress={() => {
                        NavigationController.goBack()
                    }}
                    onRightElementPress={(label) => { NavigationController.openDrawer() }}
                />
                <FlatList
                    refreshControl={
                        <RefreshControl onRefresh={() => { this.refreshData() }} refreshing={this.state.loading} />
                    }
                    ListEmptyComponent={() => (
                        <View style={{ width: wp('100%'), marginTop: wp('4%'), }}>
                            <Text style={[{
                                fontFamily: IranYekanMobile,
                                color: Colors.PRIMARY
                            }, { textAlign: 'center' }]}>شما رزومه ای ثبت نکرده اید!</Text>
                        </View>
                    )}
                    data={this.state.data}
                    renderItem={({ item }) => (
                        <View style={{ width: '100%' }}>
                            {this.renderResumeList(item)}
                        </View>
                    )}
                />
                <Ripple disabled={this.state.loading} onPress={() => { NavigationController.navigate('AddResume') }} style={{ height: wp('10%'), width: wp('50%'), alignItems: 'center', justifyContent: 'center', backgroundColor: Color.BUTTON_ACCEPT, marginHorizontal: wp('10%'), alignSelf: 'center', borderRadius: wp('1%'), marginVertical: wp('10%'), flexDirection: 'row' }}>
                    {/* {
                        this.state.loading ?
                            <ActivityIndicator size={wp('7%')} color={Colors.WHITE} />
                            :
                            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}> */}
                    <MIcons name='add' color={Colors.WHITE} size={20} />
                    <Text style={{ fontFamily: IranSansMobile, color: Colors.WHITE, alignSelf: 'center' }}>ثبت رزومه جدید</Text>
                    {/* </View>
                    } */}
                </Ripple>
            </View>
        )
    }
}
