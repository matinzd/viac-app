import React, { Component } from 'react'
import { View, Text, FlatList, StyleSheet, Image, RefreshControl, I18nManager } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Colors from '../../../constants/Colors';
import { Toolbar } from 'react-native-material-ui'
import Ripple from 'react-native-material-ripple';
import { FloatingAction } from '../../../components/FAB/src';
import { IranYekanMobile, IranYekanMobileBold } from '../../../constants/Fonts';
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import NavigationController from '../../../controllers/NavigationController';
import PlayerAPI from '../../../webservices/PlayerAPI';
import { BASE_URL } from '../../../webservices/BaseAdapter';
import { Statusbar } from '../../../components/viac';
import RF from 'react-native-responsive-fontsize'
import ResumeAPI from '../../../webservices/ResumeAPI';
import ViacToast from '../../../controllers/ViacToast';
import PlayersList from './PlayersList'
import RefreesList from './RefreesList';
import CoachList from './CoachsList';
import CoachsList from './CoachsList';

class ResumeList extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: [],
            loading: false,
            searchText: ''
        }

    }

    componentDidMount() {
        this.getData()
    }

    getData(search) {
        this.setState({ loading: true })
        ResumeAPI.getAll(this.props.navigation.getParam('type'), search)
            .then(response => {
                this.setState({ loading: false })
                this.setState({
                    data: response.data.data.data
                })

            }).catch((err) => {
                this.setState({ loading: false })
                if (err.serverMessage.response.data.message) {
                    ViacToast.showToast(err.serverMessage.response.data.message, 2000)
                }
            })
    }

    onSearchClicked() {
        this.getData(this.state.searchText)
    }

    onSearchTextChanged(text) {
        this.setState({
            searchText: text
        })
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Statusbar light={false} backgroundColor={Colors.WHITE} />
                <Toolbar
                    ref={ref => this.toolbar = ref}
                    leftElement={I18nManager.isRTL ? "arrow-forward" : "arrow-back"}
                    searchable={{
                        autoFocus: true,
                        placeholder: 'جست و جو ...',
                        onSubmitEditing: () => this.onSearchClicked(),
                        onSearchPressed: () => this.onSearchClicked(),
                        onChangeText: (text) => this.onSearchTextChanged(text),
                    }}
                    centerElement={
                        <Text style={{ alignSelf: 'flex-start', color: Colors.SECONDARY, fontFamily: IranYekanMobile, fontSize: RF(2.5) }}>رزومه های ورزشی > {this.props.navigation.getParam('resumeType')}</Text>
                    }
                    currentRoute='Tabs'
                    onLeftElementPress={() => {
                        NavigationController.goBack()
                    }}
                    onRightElementPress={(label) => { NavigationController.openDrawer() }}
                />
                <FlatList
                    refreshControl={
                        <RefreshControl onRefresh={() => this.getData()} refreshing={this.state.loading} />
                    }
                    ListEmptyComponent={() => (
                        !this.state.loading ?
                            <View style={{ width: wp('100%'), marginTop: wp('4%'), }}>
                                <Text style={[styles.itemTextStyle, { textAlign: 'center' }]}>متاسفانه رزومه ای مطابق درخواست شما یافت نشد 😢</Text>
                            </View>
                            :
                            null
                    )}
                    data={this.state.data}
                    renderItem={({ item }) => (
                        <View style={{ width: '100%' }}>
                            {this.renderResumeList(item)}
                        </View>
                    )}
                />
            </View>

        )
    }

    renderResumeList(item) {
        switch (item.type.id) {
            case 0: //Bazikon
                return (<PlayersList data={item} />)
            case 1://Davar
                return (<RefreesList data={item} />)
            case 2://Morabi
                return (<CoachsList data={item} />)
            default:
                return (<PlayersList data={item} />)
        }
    }


}

const styles = StyleSheet.create({
    listItemContainer: {
        height: hp('20%'),
        width: wp('90%'),
        elevation: 5,
        backgroundColor: Colors.WHITE,
        marginTop: wp('3%'),
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        marginBottom: 10,
    },
    gridViewContainer: {
        height: wp('50%'),
        width: wp('45%'),
        elevation: 5,
        backgroundColor: Colors.WHITE,
        marginTop: wp('4%'),
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        marginBottom: 10,

    },
    itemTextStyle: {
        fontFamily: IranYekanMobile,
        color: Colors.PRIMARY
    }
})



export default ResumeList