import React, { Component } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { View, Text, StyleSheet } from 'react-native'
import { setStore } from 'trim-redux';
import { BottomDrawer } from '../../../components/viac';
import PlayersFilter from './PlayersFilter';
import Colors from '../../../constants/Colors';

export default class BottomSheetPlayerFilter extends Component {

    constructor(props) {
        super(props)
    }

    componentDidMount(){
        
    }

    onIndexChange(index) {
        this.setState({ index })
    }

    componentWillReceiveProps() {
        
    }

    render() {
        return (

            <BottomDrawer
                ref={ref => {
                    this.playerBottomSheet = ref
                    setStore({
                        playerBottomSheet: ref
                    })
                }}
                roundedEdges={true}
                shadow={true}
                downDisplay={hp('80%')}
                startUp={false}
                offset={wp('10%')}
                containerHeight={hp('80%')}
                >
                
                <PlayersFilter />
            </BottomDrawer>
        )
    }

}
