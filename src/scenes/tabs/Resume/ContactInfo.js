import React, { Component } from 'react'
import { View, Text, I18nManager, ActivityIndicator, ScrollView, KeyboardAvoidingView, TextInput } from 'react-native'
import Colors from '../../../constants/Colors';
import { Toolbar, RadioButton } from 'react-native-material-ui';
import NavigationController from '../../../controllers/NavigationController';
import { Statusbar } from '../../../components/viac';
import { IranYekanMobile, IranSansMobile } from '../../../constants/Fonts';
import RF from 'react-native-responsive-fontsize';
import Color from '../../../styles/Color';
import MIcons from 'react-native-vector-icons/MaterialIcons'
import Ripple from 'react-native-material-ripple';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Steps from 'react-native-steps';
import ViewPager from "@react-native-community/viewpager";
import FeIcons from 'react-native-vector-icons/Feather'
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Honors from './Honors';
import InputScrollView from 'react-native-input-scroll-view';

export default class ContactInfo extends Component {


    constructor(props) {
        super(props)
        this.state = {
            insta: '',
            mobile: '',
            telegram: '',
            email: ''
        }
    }

    onDataChanged() {
        this.props.onContactInfoChanged(this.state)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.initialData) {
            this.setState({
                insta: nextProps.initialData.insta,
                mobile: nextProps.initialData.mobile,
                telegram: nextProps.initialData.telegram,
                email: nextProps.initialData.email
            })
        }
    }

    render() {
        return (
            <InputScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
                <View style={{
                    borderWidth: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: wp('12%'), borderRadius: wp('1%'), width: '100%', borderColor: Color.SECONDARY, paddingHorizontal: wp('3%')
                }}>
                    <TextInput
                        onChangeText={insta => {
                            this.state.insta = insta
                            this.onDataChanged()
                        }}
                        defaultValue={this.state.insta}
                        placeholder='Instagram'
                        style={{ flex: 1 }} />
                    <FeIcons name='instagram' size={wp('6%')} color={Colors.SECONDARY} />
                </View>
                <View style={{
                    borderWidth: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: wp('12%'), borderRadius: wp('1%'), width: '100%', borderColor: Color.SECONDARY, paddingHorizontal: wp('3%'), marginTop: wp('3%')
                }}>
                    <TextInput
                        onChangeText={mobile => {
                            this.state.mobile = mobile
                            this.onDataChanged()
                        }}
                        defaultValue={this.state.mobile}
                        placeholder='Phone'
                        keyboardType='phone-pad'
                        style={{ flex: 1 }} />
                    <FeIcons name='smartphone' size={wp('6%')} color={Colors.SECONDARY} />
                </View>
                <View style={{
                    borderWidth: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: wp('12%'), borderRadius: wp('1%'), width: '100%', borderColor: Color.SECONDARY, paddingHorizontal: wp('3%'), marginTop: wp('3%')
                }}>
                    <TextInput
                        onChangeText={telegram => {
                            this.state.telegram = telegram
                            this.onDataChanged()
                        }}
                        defaultValue={this.state.telegram}
                        placeholder='Telegram'
                        style={{ flex: 1 }} />
                    <MCIcons name='telegram' size={wp('6%')} color={Colors.SECONDARY} />
                </View>
                <View style={{
                    borderWidth: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', height: wp('12%'), borderRadius: wp('1%'), width: '100%', borderColor: Color.SECONDARY, paddingHorizontal: wp('3%'), marginTop: wp('3%')
                }}>
                    <TextInput
                        onChangeText={email => {
                            this.state.email = email
                            this.onDataChanged()
                        }}
                        defaultValue={this.state.email}
                        placeholder='Email address'
                        keyboardType='email-address'
                        style={{ flex: 1 }} />
                    <MCIcons name='email' size={wp('6%')} color={Colors.SECONDARY} />
                </View>
            </InputScrollView>
        )
    }
}
