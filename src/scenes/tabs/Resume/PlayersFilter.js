import React, { Component } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { View, Text, StyleSheet, ScrollView, KeyboardAvoidingView } from 'react-native'
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';
// import { setStore } from 'trim-redux';
import { TextField } from 'react-native-material-textfield';
import { Checkbox } from 'react-native-material-ui';
import {
    Chip,
    Selectize
} from 'react-native-material-selectize';
import Ripple from 'react-native-material-ripple';
import { getStore } from 'trim-redux';
import { IranSansMobile } from '../../../constants/Fonts';
import Colors from '../../../constants/Colors';

export default class PlayersFilter extends Component {

    constructor(props) {
        super(props)
        // this.state = {
        // index: 0,
        // routes: [
        // { key: 'sort', title: 'مرتب سازی' },
        //     { key: 'filter', title: 'فیلتر کردن' },
        // ],
        // isTabBarVisible: true

        // };
        this.state = {
            names: [
                'اسختر', 'والیبال'
            ],
            text: '',
            hasImage: true
        }
    }

    componentDidMount() {

    }

    onIndexChange(index) {

    }

    componentWillReceiveProps() {

    }


    onSubmitPress = () => {
        let advertisementBottomSheet = getStore('advertisementBottomSheet')
        advertisementBottomSheet.closeBottomDrawer()
    }

    render() {
        return (
            <View>
                <ScrollView
                    keyboardShouldPersistTaps='handled'
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}>
                    <View style={{ flex: 1, padding: wp('8%'), marginBottom: 40, justifyContent: 'center' }}>
                        <KeyboardAvoidingView behavior="padding">
                            <Selectize
                                textInputProps={{ placeholder: 'به طور مثال استخر ...' }}
                                labelStyle={{ fontFamily: IranSansMobile }}
                                // containerStyle={{ fontFamily: IranSansMobile }}
                                label='انتخاب رشته ورزشی'
                                ref={c => (this._childEmailField = c)}
                                items={reshte}

                                renderRow={(id, onPress, item) => (
                                    <Ripple
                                        onPress={onPress}
                                        style={{
                                            paddingVertical: 8,
                                            paddingHorizontal: 10,
                                            backgroundColor: Colors.SECONDARY,
                                            height: hp('5%'),
                                            width: wp('90%'),
                                            justifyContent: 'center'
                                        }}
                                    >
                                        <Text style={{ fontFamily: IranSansMobile, color: Colors.WHITE }} >{id}</Text>
                                    </Ripple>
                                )}
                            />

                            <Selectize
                                textInputProps={{ placeholder: 'برای مثال تفریح ...' }}
                                labelStyle={{ fontFamily: IranSansMobile }}
                                // containerStyle={{ fontFamily: IranSansMobile }}
                                label='انتخاب تگ'
                                ref={c => (this._childEmailField = c)}
                                items={tags}
                                renderRow={(id, onPress, item) => (
                                    <Ripple
                                        onPress={onPress}
                                        style={{
                                            paddingVertical: 8,
                                            paddingHorizontal: 10,
                                            backgroundColor: Colors.SECONDARY,
                                            height: hp('5%'),
                                            width: wp('90%'),
                                            justifyContent: 'center'
                                        }}
                                    >
                                        <Text style={{ fontFamily: IranSansMobile, color: Colors.WHITE }} >{id}</Text>
                                    </Ripple>
                                )}
                            />

                            <Selectize
                                textInputProps={{ placeholder: 'برای مثال نردبان یک ...' }}
                                labelStyle={{ fontFamily: IranSansMobile }}
                                // containerStyle={{ fontFamily: IranSansMobile }}
                                label='انتخاب نردبان'
                                ref={c => (this._childEmailField = c)}
                                items={nardeban}
                                renderRow={(id, onPress, item) => (
                                    <Ripple
                                        onPress={onPress}
                                        style={{
                                            paddingVertical: 8,
                                            paddingHorizontal: 10,
                                            backgroundColor: Colors.SECONDARY,
                                            height: hp('5%'),
                                            width: wp('90%'),
                                            justifyContent: 'center'
                                        }}
                                    >
                                        <Text style={{ fontFamily: IranSansMobile, color: Colors.WHITE }} >{id}</Text>
                                    </Ripple>
                                )}
                            />
                        </KeyboardAvoidingView>
                        <View style={{ height: hp('5%') }}>
                            <Checkbox label="فقط نمایش عکس دار ها" value="hasImage" onCheck={() => { this.setState({ hasImage: !this.state.hasImage }) }} checked={this.state.hasImage} />
                        </View>
                        <Ripple style={[styles.verifyButtonStyle]} onPress={this.onSubmitPress.bind(this)} >
                            <Text style={styles.buttonTextStyle} >اعمال</Text>
                        </Ripple>
                    </View>

                </ScrollView>

            </View>

        )
    }

}

const reshte = [
    { id: 'استخر', email: 'john@gmail.com' },
    { id: 'فوتبال', email: 'doe@gmail.com' }
]
const tags = [
    { id: 'جذاب', email: 'john@gmail.com' },
    { id: 'بفریحی', email: 'doe@gmail.com' },
    { id: '1جذاب', email: 'john@gmail.com' },
    { id: '2بفریحی', email: 'doe@gmail.com' },
    { id: '3جذاب', email: 'john@gmail.com' },
    { id: '4بفریحی', email: 'doe@gmail.com' },
    { id: '5جذاب', email: 'john@gmail.com' },
    { id: '6بفریحی', email: 'doe@gmail.com' },
    { id: '7جذاب', email: 'john@gmail.com' },
    { id: '8بفریحی', email: 'doe@gmail.com' }
]
const nardeban = [
    { id: 'نردبان یک', email: 'john@gmail.com' },
    { id: 'نردبان دو', email: 'doe@gmail.com' }
]

const objectTags = [
    {
        key: 'id_01',
        value: 'cherry',
    },
    {
        key: 'id_02',
        value: 'mango',
    },
    {
        key: 'id_03',
        value: 'cashew',
    },
    {
        key: 'id_04',
        value: 'almond'
    },
    {
        key: 'id_05',
        value: 'guava'
    },
    {
        key: 'id_06',
        value: 'pineapple'
    },
    {
        key: 'id_07',
        value: 'orange'
    },
    {
        key: 'id_08',
        value: 'pear'
    },
    {
        key: 'id_09',
        value: 'date'
    }
]

const styles = StyleSheet.create({
    inputContainer: {
        marginHorizontal: wp('10%')
    },
    verifyButtonStyle: {
        backgroundColor: Colors.PRIMARY,
        justifyContent: 'center',
        width: '50%',
        height: hp('5%'),
        borderRadius: wp('2%'),
        marginHorizontal: wp('10%'),
        marginTop: hp('2%'),
        alignSelf: 'center'
    },
    inputLabelStyle: {
        fontFamily: IranSansMobile,
    },
    buttonTextStyle: {
        color: Colors.WHITE,
        alignSelf: 'center',
        fontFamily: IranSansMobile
    }
})

{/* <TabView
    navigationState={this.state}
    renderScene={SceneMap({
        // sort: Sort,
        filter: Filter,
    })}
    renderTabBar={props =>
        <TabBar
            {...props}
            onTabPress={() => {
                this.bottomSheet.toggleDrawerState()
            }}
            labelStyle={{ fontFamily: IranSansMobile, color: '#FFFFFF' }}
            // tabStyle={{ backgroundColor: '#FFFFFF' }}
            indicatorStyle={{ backgroundColor: Colors.SECONDARY, height: hp('1%') }}
            style={{ backgroundColor: Colors.SECONDARY, height: hp('7%') }}
            renderLabel={({ route, focused, color }) => {
                return (
                    <View style={{ flexDirection: 'row' }} >
                        <Icon name={route.key === 'sort' ? 'sort' : 'filter-outline'} size={20} color="#fff" />
                        <Text style={styles.bottomTextStyle} >{route.title}</Text>
                    </View>
                )
            }}
        />
    }

    onIndexChange={index => {
        this.onIndexChange(index)
    }}
    swipeEnabled={true}
    initialLayout={{ width: wp('100%') }}
/> */}