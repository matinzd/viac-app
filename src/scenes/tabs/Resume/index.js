import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, I18nManager } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Toolbar } from 'react-native-material-ui';
import { IranSansMobile, IranYekanMobile } from '../../../constants/Fonts';
import Colors from '../../../constants/Colors';
import NavigationController from '../../../controllers/NavigationController';
import { Statusbar } from '../../../components/viac';
import PlayersList from './PlayersList';
import RandomTagsList from './RandomTagsList';
import BottomSheetPlayerFilter from './BottomSheetPlayerFilter';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import Ripple from 'react-native-material-ripple';
import RF from 'react-native-responsive-fontsize';
import * as Animatable from 'react-native-animatable'
import Color from '../../../styles/Color';
import { getStore } from 'trim-redux';
import { FloatingAction } from '../../../components/FAB/src';


class Resume extends Component {

    // static navigationOptions =  ({ navigation }) => ({
    //     tabBarColor: Colors.PRIMARY,
    //     tabBarIcon: <Icon name="users" size={20} color="#fff" />,
    //     tabBarLabel: <Text style={{ fontFamily: IranSansMobile }} >بازیکنان</Text>,
    //     tabBarVisible: navigation.getParam('tabBarVisible') === false ? false : true
    // });


    componentDidMount() {
        console.log(this.props.navigation)
    }

    onSearchClicked = () => {
        console.log('On search clicked')
    }

    onSubmitEditing = () => {
        this.toolbar.onSearchCloseRequested
    }

    onSearchTextChanged = (text) => {
        console.log('Text ', text)
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
                <Statusbar light={false} backgroundColor={Colors.WHITE} />
                <Toolbar
                    ref={ref => this.toolbar = ref}
                    leftElement="menu"
                    rightElement={
                        // <Ripple
                        //     onPress={() => {
                        //         NavigationController.navigate('AddOrEditResume')
                        //     }}
                        // >
                        //     <Text style={{ alignSelf: 'flex-start', margin: wp('2%'), color: Colors.SECONDARY, fontFamily: IranYekanMobile, fontSize: RF(2.5) }}>رزومه های من</Text>
                        // </Ripple>
                        <Ripple onPress={() => {
                            getStore('userInfo').isUserLoggedIn ? NavigationController.navigate('ManageResumes') : NavigationController.navigate('Authentication')
                        }}
                            style={{ height: wp('10%'), width: null, justifyContent: 'center', backgroundColor: Color.BUTTON_ACCEPT, alignSelf: 'center', paddingHorizontal: wp('2%'), borderRadius: wp('1%'), marginVertical: wp('10%') }}>
                            <Text style={{ fontFamily: IranSansMobile, color: Colors.WHITE, alignSelf: 'center' }}>رزومه های من</Text>
                        </Ripple>
                    }
                    // searchable={{
                    //     autoFocus: true,
                    //     placeholder: 'جست و جو ...',
                    //     onSubmitEditing: this.onSubmitEditing.bind(this),
                    //     onSearchPressed: this.onSearchClicked.bind(this),
                    //     onChangeText: this.onSearchTextChanged.bind(this),
                    // }}
                    centerElement={
                        <Text style={{ alignSelf: 'flex-start', color: Colors.SECONDARY, fontFamily: IranYekanMobile, fontSize: RF(2.5) }}>رزومه های ورزشی</Text>
                    }
                    currentRoute='Tabs'
                    onLeftElementPress={() => {
                        NavigationController.openDrawer()
                    }}
                    onRightElementPress={(label) => { NavigationController.openDrawer() }}
                />
                {/* <RandomTagsList />
                <PlayersList />
                <BottomSheetPlayerFilter /> */}

                <Animatable.View animation='flipInY' useNativeDriver={true} style={{ flex: 3, flexDirection: 'column', justifyContent: 'space-between' }}>
                    {/* <Ripple rippleOpacity={0.1} rippleCentered={true} style={{ flex: 1, elevation: 5, margin: widthPercentageToDP('3%') }}>
                        <ImageBackground style={{width: '100%'}} resizeMode={'contain'} source={require('../../../assets/images/refree_whistle.jpg')} />
                    </Ripple> */}
                    <ImageBackground animation='zoomIn' blurRadius={0.8} style={styles.resumeItemImgBg} resizeMode={'cover'} source={require('../../../assets/images/refree_whistle.jpg')}>
                        <Ripple
                            onPress={() => NavigationController.navigate('ResumeList', { resumeType: 'داوران', type: [1] })}
                            rippleOpacity={0.1} rippleCentered={true} style={{ flex: 1, elevation: 5, justifyContent: 'center' }}>
                            <View style={styles.overlay} />
                            <Text style={{ alignSelf: 'flex-start', margin: wp('5%'), fontFamily: IranSansMobile, color: Colors.WHITE, fontSize: RF(4) }}>داوران</Text>
                        </Ripple>
                    </ImageBackground>
                    <ImageBackground blurRadius={0.8} style={styles.resumeItemImgBg} resizeMode={'cover'} source={require('../../../assets/images/coach.jpg')}>
                        <Ripple
                            onPress={() => NavigationController.navigate('ResumeList', { resumeType: 'مربیان', type: [2] })}
                            rippleOpacity={0.1} rippleCentered={true} style={{ flex: 1, elevation: 5, justifyContent: 'center' }}>
                            <View style={styles.overlay} />
                            <Text style={{ alignSelf: 'flex-start', margin: wp('5%'), fontFamily: IranSansMobile, color: Colors.WHITE, fontSize: RF(4) }}>مربیان</Text>
                        </Ripple>
                    </ImageBackground>
                    <ImageBackground blurRadius={0.8} style={styles.resumeItemImgBg} resizeMode={'cover'} source={require('../../../assets/images/players.jpg')}>
                        <Ripple
                            onPress={() => NavigationController.navigate('ResumeList', { resumeType: 'بازیکنان', type: [0] })}
                            rippleOpacity={0.1}
                            rippleCentered={true}
                            style={{ flex: 1, elevation: 5, justifyContent: 'center' }}>
                            <View style={styles.overlay} />
                            <Text style={{ alignSelf: 'flex-start', margin: wp('5%'), fontFamily: IranSansMobile, color: Colors.WHITE, fontSize: RF(4) }}>بازیکنان</Text>
                        </Ripple>
                    </ImageBackground>
                </Animatable.View>
                <FloatingAction
                    floatingIcon={
                        <MCIcons name="plus" size={wp('8%')} color={Colors.WHITE} />
                    }
                    // onPressItem={this.handleItemPress.bind(this)}
                    // actions={this.state.actions}
                    onPressMain={() => {
                        if (getStore('userInfo').isUserLoggedIn) {
                            NavigationController.navigate('AddResume')
                        } else {
                            NavigationController.navigate('Authentication')
                        }
                    }}
                    position="left"
                // style={{ backgroundColor: '#3ab', position:'absolute', bottom:10,elevation: 8  }}
                />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    resumeItemImgBg: { flex: 1, elevation: 5, margin: wp('3%') },
    overlay: { position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, backgroundColor: '#000', opacity: 0.5 }
})


export default Resume