import React, { Component } from 'react'
import { View, Text, I18nManager, ActivityIndicator, ScrollView, TextInput, KeyboardAvoidingView, Keyboard } from 'react-native'
import Colors from '../../../constants/Colors';
import { Toolbar, RadioButton } from 'react-native-material-ui';
import NavigationController from '../../../controllers/NavigationController';
import { Statusbar } from '../../../components/viac';
import { IranYekanMobile, IranSansMobile } from '../../../constants/Fonts';
import RF from 'react-native-responsive-fontsize';
import Color from '../../../styles/Color';
import MIcons from 'react-native-vector-icons/MaterialIcons'
import Ripple from 'react-native-material-ripple';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Steps from 'react-native-steps';
import ViewPager from "@react-native-community/viewpager";
import FeIcons from 'react-native-vector-icons/Feather'
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Honors from './Honors';
import ContactInfo from './ContactInfo';
import ResumeTypes from './ResumeTypes';
import ResumeAPI from '../../../webservices/ResumeAPI';
import SportsInfo from './SportsInfo';
import ViacToast from '../../../controllers/ViacToast';
import StateModal from '../../../modals/StateModal';
import { setStore, getStore } from 'trim-redux';
import * as Animatable from 'react-native-animatable'

const labels = ["نوع", "تکمیل اطلاعات", "افتخارات", "اطلاعات تماس"];


export default class EditResume extends Component {


    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            current: 0,
            page: 3,
            resumeType: this.props.navigation.getParam('type'),
            honor: [],
            contact: {},
            info: {
                id: this.props.navigation.getParam('id'),
                coachDegree: 0,
                matchType: 0,
                experience: 0,
                sportType: 0,
                age: 0,
                selectedAges: [],
                selectedExperiences: [],
                categoryId: 0,
                weight: '0',
                height: '0',
                coach: '',
                degreeOfEducation: '',
                placeName: '',
                major: '',
                aboutMe: ''
            },
            stepsShown: true
        }
    }



    componentDidMount() {
        this.getDefaultValues()
        Keyboard.addListener('keyboardDidShow', () => {
            if (this.stepRef !== null && this.buttonsRef !== null && this.state.stepsShown) {
                this.stepRef.fadeOutUp(500);
                this.buttonsRef.fadeOutDown(500);
                this.setState({
                    stepsShown: false
                })
            }
        })

        Keyboard.addListener('keyboardDidHide', () => {
            this.setState({
                stepsShown: true
            })
            // if (this.stepRef !== null && this.buttonsRef !== null && !this.state.stepsShown) {

            this.stepRef.fadeInDown(500);
            this.buttonsRef.fadeInUp(500);
            // }
        })
    }

    componentWillUnmount() {
        Keyboard.removeAllListeners('keyboardDidHide')
        Keyboard.removeAllListeners('keyboardDidShow')
    }

    getDefaultValues() {
        ResumeAPI.get(this.props.navigation.getParam('id')).then(response => {
            let result = response.data.data
            this.state.info = {
                id: this.props.navigation.getParam('id'),
                coachDegree: result.coachDegree,
                matchType: result.matchType.id,
                experience: result.experience,
                sportType: result.sportType.id,
                selectedAges: result.ages,
                selectedExperiences: result.experience,
                categoryId: result.category.id,
                weight: result.weight,
                height: result.height,
                coach: result.coach,
                degreeOfEducation: result.degreeOfEducation,
                placeName: result.placeName,
                major: result.major,
                aboutMe: result.aboutMe
            }
            this.state.contact = result.contact
            this.state.honor = result.honors
            console.log('info ', this.state.info)
            this.setState({
                info: this.state.info,
                honor: this.state.honor,
                contact: this.state.contact
            })
        })
    }

    insertResume() {
        this.setState({ loading: true })
        ResumeAPI.insertUpdate(this.state.resumeType, this.state.contact, this.state.honor, this.state.info.selectedAges, this.state.info.categoryId, this.state.info.coach, this.state.info.sportType, this.state.info.selectedExperiences, this.state.info.placeName, this.state.info.matchType, this.state.info.height, this.state.info.weight, this.state.info.coachDegree, this.state.info.degreeOfEducation, this.state.info.major, getStore('selectedStateId'), this.state.info.aboutMe, this.props.navigation.getParam('id'))
            .then((response) => {
                this.setState({ loading: false })
                if (response.data.message) {
                    ViacToast.showToast(response.data.message, 4000)
                }

                if (response.data.success) {
                    NavigationController.goBack()
                }
            })
            .catch(() => {
                this.setState({ loading: false })
            })
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
                <Statusbar light={false} backgroundColor={Colors.WHITE} />
                <Toolbar
                    ref={ref => this.toolbar = ref}
                    leftElement={I18nManager.isRTL ? "arrow-forward" : "arrow-back"}
                    centerElement={
                        <Text style={{ alignSelf: 'flex-start', color: Colors.SECONDARY, fontFamily: IranYekanMobile, fontSize: RF(2.5) }}>ویرایش رزومه</Text>
                    }
                    onLeftElementPress={() => {
                        NavigationController.goBack()
                    }}
                    onRightElementPress={(label) => { NavigationController.openDrawer() }}
                />

                <Animatable.View collapsable={true} useNativeDriver={true} ref={ref => this.stepRef = ref} style={{ margin: wp('3%') }}>
                    {
                        this.state.stepsShown ?
                            <Steps
                                current={this.state.current}
                                labels={labels}
                                count={4}
                                renderStepIndicator={({ position, stepStatus }) =>
                                    this.renderStepIcon(position, stepStatus)
                                }
                            />
                            :
                            null
                    }
                </Animatable.View>
                <ViewPager
                    ref={ref => this.vp = ref}
                    style={{ flex: 1, width: '100%' }}
                    onPageSelected={(event) => {
                        this.setState({
                            current: Math.abs(event.nativeEvent.position - 3)
                        })
                    }}
                    initialPage={this.state.page}
                >
                    <View key="3" style={{ width: 50, padding: wp('7%'), alignItems: 'center' }}>
                        <ContactInfo
                            initialData={this.state.contact}
                            onContactInfoChanged={(contact) => this.setState({ contact: contact })}
                        />
                    </View>
                    <View key="2" style={{ width: 50, padding: wp('7%'), alignItems: 'center' }}>
                        <Honors
                            initialData={this.state.honor}
                            onHonorsChanged={(honors) => this.setState({ honor: honors })}
                        />
                    </View>
                    <View key="1" style={{ width: 50, padding: wp('7%'), alignItems: 'center' }}>
                        <SportsInfo
                            initialData={this.state}
                            onInfoChanged={(newInfo) => this.setState({ info: newInfo })}
                            type={this.state.resumeType} />
                    </View>
                    <ScrollView key="0" style={{ width: 50, padding: wp('7%') }} contentContainerStyle={{ alignItems: 'center' }}>
                        <ResumeTypes
                            onTypeChanged={(newType) => {
                                this.setState({ resumeType: newType })
                            }}
                            initialResumeType={this.state.resumeType} />
                    </ScrollView >

                </ViewPager>
                {
                    this.state.loading ?
                        <ActivityIndicator size='large' color={Colors.SECONDARY} />
                        :
                        this.state.stepsShown ?
                            this.renderBottomButtons()
                            :
                            null
                }
                <StateModal />
            </View >
        )
    }

    renderBottomButtons = () => (
        <Animatable.View collapsable={true} useNativeDriver={true} ref={ref => this.buttonsRef = ref} enabled={false} >
            {
                this.state.stepsShown ?
                    <View style={{ flexDirection: 'row', marginBottom: wp('3%'), justifyContent: 'space-between', alignItems: 'center', marginHorizontal: wp('20%') }}>
                        {
                            this.state.current !== 0 ?
                                <Ripple onPress={() => {
                                    this.state.current--
                                    this.state.page++
                                    this.vp.setPage(this.state.page)
                                    this.setState({ current: this.state.current })
                                }} style={{ width: wp('20%'), borderRadius: wp('2%'), borderWidth: 1, borderColor: Color.BUTTON_ACCEPT, height: 50, backgroundColor: Color.WHITE, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: Color.BUTTON_ACCEPT, fontFamily: IranSansMobile, fontSize: RF(2) }}>قبلی</Text>
                                </Ripple>
                                :
                                <Ripple onPress={() => { }} style={{ width: wp('20%'), opacity: 0, borderRadius: wp('2%'), borderWidth: 1, borderColor: Colors.WHITE, height: 50, backgroundColor: Colors.WHITE, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: Colors.WHITE, fontFamily: IranSansMobile, fontSize: RF(2) }}>قبلی</Text>
                                </Ripple>
                        }
                        {
                            this.state.current !== 3 ?
                                <Ripple onPress={() => {
                                    this.state.current++
                                    this.state.page = 3 - this.state.current
                                    this.vp.setPage(this.state.page)
                                    this.setState({ current: this.state.current })
                                }}
                                    style={{ width: wp('20%'), borderRadius: wp('2%'), height: 50, backgroundColor: Color.BUTTON_ACCEPT, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: Colors.WHITE, fontFamily: IranSansMobile, fontSize: RF(2) }}>بعدی</Text>
                                </Ripple>
                                :
                                <Ripple onPress={() => { this.insertResume() }} style={{ width: wp('20%'), borderRadius: wp('2%'), height: 50, backgroundColor: Color.BUTTON_ACCEPT, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: Colors.WHITE, fontFamily: IranSansMobile, fontSize: RF(2) }}>ویرایش</Text>
                                </Ripple>
                        }
                    </View>
                    :
                    null
            }
        </Animatable.View>
    )

    renderStepIcon(position, stepStatus) {
        let icon;
        switch (position) {
            case 0:
                icon = 'assignment'
                break
            case 1:
                icon = 'edit'
                break
            case 2:
                icon = 'stars'
                break
            case 3:
                icon = 'smartphone'
                break
        }
        return (
            <MIcons name={icon} color={stepStatus === 'current' ? Colors.SECONDARY : Colors.WHITE} size={wp('5%')} />
        )
    }
}
