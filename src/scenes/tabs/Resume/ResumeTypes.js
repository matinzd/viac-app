import React, { Component } from 'react'
import { View, Text, I18nManager, ActivityIndicator, ScrollView, TextInput } from 'react-native'
import Colors from '../../../constants/Colors';
import { Toolbar, RadioButton } from 'react-native-material-ui';
import NavigationController from '../../../controllers/NavigationController';
import { Statusbar } from '../../../components/viac';
import { IranYekanMobile, IranSansMobile } from '../../../constants/Fonts';
import RF from 'react-native-responsive-fontsize';
import Color from '../../../styles/Color';
import MIcons from 'react-native-vector-icons/MaterialIcons'
import Ripple from 'react-native-material-ripple';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Steps from 'react-native-steps';
import ViewPager from "@react-native-community/viewpager";
import FeIcons from 'react-native-vector-icons/Feather'
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons'

export default class ResumeTypes extends Component {


    constructor(props) {
        super(props)
        this.state = {
            type: this.props.initialResumeType,
        }
    }

    componentDidMount() {

    }



    render() {
        return (
            <View style={{ flex: 1, width: '100%' }}>
                <RadioButton
                    label="من داور هستم"
                    checked={this.state.type === 1}
                    value={1}
                    onCheck={() => {
                        this.state.type = 1
                        this.props.onTypeChanged(1)
                        this.setState({ type: this.state.type })
                    }}
                    onSelect={() => {
                        this.state.type = 1
                        this.props.onTypeChanged(1)
                        this.setState({ type: this.state.type })
                    }}

                />
                <RadioButton
                    label="من مربی هستم"
                    checked={this.state.type === 2}
                    value={2}
                    onCheck={checked => {
                        this.state.type = 2
                        this.props.onTypeChanged(2)
                        this.setState({ type: this.state.type })
                    }}
                    onSelect={() => {
                        this.state.type = 2
                        this.props.onTypeChanged(2)
                        this.setState({ type: this.state.type })
                    }}
                />
                <RadioButton
                    label="من ورزش کار هستم"
                    checked={this.state.type === 0}
                    value={0}
                    onCheck={checked => {
                        this.state.type = 0
                        this.props.onTypeChanged(0)
                        this.setState({ type: this.state.type })
                    }}
                    onSelect={() => {
                        this.state.type = 0
                        this.props.onTypeChanged(0)
                        this.setState({ type: this.state.type })
                    }}
                />
                <View style={{ borderWidth: 1, borderColor: Colors.SECONDARY, padding: wp('3%') }}>
                    <Text style={{ fontFamily: IranSansMobile, lineHeight: wp('7%'), textAlign: 'center' }}>
                        برای ویرایش نام ، نام خانوادگی و عکس رزومه لطفا از قسمت ویرایش پروفایل اقدام فرمایید
                    </Text>

                    <Ripple  onPress={() => { NavigationController.navigate('Profile') }} style={{ height: wp('10%'), width: wp('50%'), alignItems: 'center', justifyContent: 'center', backgroundColor: Color.BUTTON_ACCEPT, marginHorizontal: wp('10%'), alignSelf: 'center', borderRadius: wp('1%'), marginVertical: wp('5%'), flexDirection: 'row' }}>
                        <Text style={{ fontFamily: IranSansMobile, color: Colors.WHITE, alignSelf: 'center' }}>ویرایش پروفایل</Text>
                    </Ripple>
                </View>
            </View>
        )
    }
}
