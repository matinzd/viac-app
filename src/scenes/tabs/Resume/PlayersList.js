import React, { Component } from 'react'
import { View, Text, FlatList, StyleSheet, Image, RefreshControl, TouchableOpacity, Alert } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Colors from '../../../constants/Colors';
import Ripple from 'react-native-material-ripple';
import { FloatingAction } from '../../../components/FAB/src';
import { IranYekanMobile, IranYekanMobileBold } from '../../../constants/Fonts';
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import NavigationController from '../../../controllers/NavigationController';
import PlayerAPI from '../../../webservices/PlayerAPI';
import { BASE_URL } from '../../../webservices/BaseAdapter';
import AntDIcon from 'react-native-vector-icons/AntDesign'
import ResumeAPI from '../../../webservices/ResumeAPI';
import ViacToast from '../../../controllers/ViacToast';

class PlayersList extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: this.props.data,
        }
        console.log(this.props.data)
        this.offset = 0
    }

    deleteResume(id) {
        ResumeAPI.delete(id).then(response => {
            ViacToast.showToast(response.data.message)
            this.props.onDelete()
        })
    }

    promptDelete(id) {
        Alert.alert('حذف رزومه', 'آیا از حدف این رزومه اطمینان دارید؟', [
            {
                text: 'بله',
                onPress: () => this.deleteResume(id)
            },
            {
                text: 'خیر',
                style: 'cancel'
            }
        ])
    }

    edit(id) {
        NavigationController.navigate('EditResume', { id , type: 0 })
    }


    render() {
        return (
            <View style={{ width: '100%', alignSelf: 'center' }}>
                <TouchableOpacity
                    style={[this.state.isGridView ? styles.gridViewContainer : styles.listItemContainer, { flexDirection: this.state.isGridView ? 'column' : 'row' }, this.state.isGridView ? { marginStart: index % 2 === 1 ? wp('5%') : 0 } : {}]}
                    onPress={() => { NavigationController.navigate('PlayerDetails', { id: this.state.data.id }) }}
                >
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={this.state.data.user.pix ? { uri: BASE_URL + '/' + this.state.data.user.pix } : require('../../../assets/images/default_img.png')} style={{ height: hp('20%'), width: this.state.isGridView ? wp('45%') : hp('20%') }} />
                    </View>
                    <View style={{ width: wp('55%'), height: this.state.isGridView ? hp('8%') : hp('20%'), padding: wp('2%'), justifyContent: 'center' }}>
                        <Text style={[styles.itemTextStyle, { fontFamily: IranYekanMobileBold, fontSize: wp('4%') }]} >{`${this.state.data.user.name} ${this.state.data.user.lastName}`}</Text>
                        <Text style={[styles.itemTextStyle, { color: Colors.PRIMARY, fontSize: wp('3%') }]}> <Text style={{ color: '#081931' }}>رشته ورزشی :</Text> {this.state.data.category.name}</Text>
                        <Text style={[styles.itemTextStyle, { color: Colors.PRIMARY, fontSize: wp('3%') }]}> <Text style={{ color: '#081931' }}>نوع رزومه : </Text>{this.state.data.type.title}</Text>
                        <Text style={[styles.itemTextStyle, { color: Colors.PRIMARY, fontSize: wp('3%') }]}> <Text style={{ color: '#081931' }}>قد :</Text> {this.state.data.height} سانتی متر</Text>
                        <Text style={[styles.itemTextStyle, { color: Colors.PRIMARY, fontSize: wp('3%') }]}> <Text style={{ color: '#081931' }}>وزن : </Text> {this.state.data.weight} کیلوگرم</Text>
                    </View>
                    {
                        NavigationController.getCurrentRouteName() === 'ManageResumes' ?
                            <View style={{ backgroundColor: '#081931', position: 'absolute', flexDirection: 'row', end: 0, top: 0, justifyContent: 'flex-end' }}>
                                <Text style={{ fontFamily: IranYekanMobile, color: Colors.WHITE, paddingHorizontal: wp('6%'), paddingVertical: wp('1%') }}>{this.state.data.status}</Text>
                            </View>
                            :
                            null
                    }
                    {
                        NavigationController.getCurrentRouteName() === 'ManageResumes' ?
                            <View style={{ position: 'absolute', flexDirection: 'row', zIndex: 10, end: 0, bottom: 0, justifyContent: 'flex-end' }}>
                                <TouchableOpacity onPress={() => this.edit(this.state.data.id)}>
                                    <AntDIcon name='edit' color={'#081931'} style={{ alignSelf: 'center', padding: wp('2%') }} size={wp('5%')} />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.promptDelete(this.state.data.id)}>
                                    <AntDIcon name='delete' color={'#081931'} style={{ alignSelf: 'center', padding: wp('2%') }} size={wp('5%')} />
                                </TouchableOpacity>
                            </View>
                            :
                            null
                    }
                </TouchableOpacity>
            </View>
        )
    }


}

const data = []

// {
//     title: 'لیونل مسی',
//     briefDescription: 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحای متنوع با هدف بهبود ابزارهای کارب',
//     time: '20 دقیقه پیش',
//     imagePath: 'https://i.ytimg.com/vi/Orh592OBoKY/maxresdefault.jpg'
// },
// {
//     title: 'سادیو مانه',
//     briefDescription: 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحای متنوع با هدف بهبود ابزارهای کارب',
//     time: '20 دقیقه پیش',
//     imagePath: 'http://images.sportspromedia.com/images/made/images/uploads/news/Sadio_Mane_LFC_630_354_80_s_c1.jpg'
// },
// {
//     title: 'لیونل مسی',
//     briefDescription: 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحای متنوع با هدف بهبود ابزارهای کارب',
//     time: '20 دقیقه پیش',
//     imagePath: 'https://i.ytimg.com/vi/Orh592OBoKY/maxresdefault.jpg'
// },
// {
//     title: 'سادیو مانه',
//     briefDescription: 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحای متنوع با هدف بهبود ابزارهای کارب',
//     time: '20 دقیقه پیش',
//     imagePath: 'http://images.sportspromedia.com/images/made/images/uploads/news/Sadio_Mane_LFC_630_354_80_s_c1.jpg'
// }

const styles = StyleSheet.create({
    listItemContainer: {
        height: hp('20%'),
        width: wp('100%'),
        elevation: 5,
        backgroundColor: Colors.WHITE,
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        marginBottom: 10,
    },
    gridViewContainer: {
        height: wp('50%'),
        width: wp('45%'),
        elevation: 5,
        backgroundColor: Colors.WHITE,
        marginTop: wp('4%'),
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        marginBottom: 10,

    },
    itemTextStyle: {
        fontFamily: IranYekanMobile,
        color: Colors.PRIMARY
    }
})



export default PlayersList