import React, { Component } from 'react'
import { View, Text, FlatList, StyleSheet, Image, RefreshControl, I18nManager } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Colors from '../../../constants/Colors';
import Ripple from 'react-native-material-ripple';
import { IranYekanMobile, IranYekanMobileBold } from '../../../constants/Fonts';
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { getStore } from 'trim-redux';
import TagAPI from '../../../webservices/TagAPI';
import { ActivityIndicator } from 'react-native-paper';

class RandomTagsList extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isGridView: false,
            randomTags: [],
            loading: false
        }
    }

    handleItemPress = (itemName) => {
        switch (itemName) {
            case "bt_accessibility":
                alert('add advertisement')
                break;
            case "grid_list":
                this.setState({
                    isGridView: !this.state.isGridView
                })
                break;
            default:
                break;
        }
    }

    componentWillMount() {
        this.setState({ loading: true })
        TagAPI.getRandom().then(response => {
            this.setState({
                randomTags: response.data.data,
                loading: false
            })
        }).catch(() => {
            this.setState({
                loading: false
            })
        })
    }

    onFilterPress = () => {
        let advertisementBottomSheet = getStore('playerBottomSheet');
        advertisementBottomSheet.toggleDrawerState()
    }

    render() {
        return (
            <View style={{ height: null, marginVertical: wp('2%') }}>

                {
                    this.state.loading ?
                        <ActivityIndicator color={Colors.PRIMARY} size={15} />
                        :
                        <FlatList
                            data={this.state.randomTags}
                            ListHeaderComponent={() => (
                                <Ripple style={{ paddingHorizontal: wp('4%'), paddingVertical: wp('1%'), borderWidth: 1, borderColor: Colors.PRIMARY, borderRadius: hp('1%'), flexDirection: 'row', marginStart: wp('3%') }} onPress={this.onFilterPress.bind(this)} >
                                    <MCIcons name="filter" size={wp('5%')} />
                                    <Text style={[styles.name, { color: Colors.PRIMARY, marginStart: wp('1%') }]}>فیلتر کردن</Text>
                                </Ripple>
                            )}
                            style={{ alignSelf: 'flex-start' }}
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item, index }) => (
                                <Ripple style={{ paddingHorizontal: wp('4%'), paddingVertical: wp('1%'), borderWidth: 1, borderColor: Colors.PRIMARY, borderRadius: hp('1%'), flexDirection: 'row', marginStart: wp('3%') }}  >
                                    {/* <MCIcons name="filter" size={wp('5%')} /> */}
                                    <Text style={[styles.name, { color: Colors.PRIMARY, marginStart: wp('1%') }]}>{item.title}</Text>
                                </Ripple>
                            )}
                        />
                }
            </View>
        )
    }
}

const data = [
    {
        name: 'فوتبال',
        id: 1
    },
    {
        name: 'استخر',
        id: 2
    },
    {
        name: 'سالن والیبال',
        id: 3
    },
    {
        name: 'فوتبال',
        id: 1
    },
    {
        name: 'استخر',
        id: 2
    },
    {
        name: 'سالن والیبال',
        id: 3
    },
    {
        name: 'فوتبال',
        id: 1
    },
    {
        name: 'استخر',
        id: 2
    },
    {
        name: 'سالن والیبال',
        id: 3
    }
]

const styles = StyleSheet.create({
    name: {
        fontFamily: IranYekanMobile,
        fontSize: 12,
        color: '#a2abb0'
    }
})


export default RandomTagsList