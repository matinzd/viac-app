import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Dimensions, I18nManager, Platform, Linking, TouchableOpacity } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Header } from 'react-navigation';
import { Toolbar } from 'react-native-material-ui';
import { IranSansMobile, IranYekanMobile, IranSansMobileBold } from '../../constants/Fonts';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Colors from '../../constants/Colors'
import HeaderImageScrollView, { TriggeringView } from 'react-native-image-header-scroll-view';
import Ripple from 'react-native-material-ripple';
import Carousel from 'react-native-snap-carousel';
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MIcons from 'react-native-vector-icons/MaterialIcons';
import FAIcons from 'react-native-vector-icons/FontAwesome5';
import NavigationController from '../../controllers/NavigationController';
import { Statusbar } from '../../components/viac';
import Color from '../../styles/Color';
import PlayerAPI from '../../webservices/PlayerAPI';
import { BASE_URL } from '../../webservices/BaseAdapter';
import ResumeAPI from '../../webservices/ResumeAPI';
import { FlatList } from 'react-native-gesture-handler';
import FeIcons from 'react-native-vector-icons/Feather'
import FaFiveIcons from 'react-native-vector-icons/FontAwesome5'
import RF from 'react-native-responsive-fontsize';
import SLIcon from 'react-native-vector-icons/SimpleLineIcons'

const MIN_HEIGHT = Header.HEIGHT;
const MAX_HEIGHT = 250;
const { width: screenWidth } = Dimensions.get('window')


class PlayerDetails extends Component {
    constructor() {
        super();
        this.state = {
            showNavTitle: false,
            playerImage: '',
            description: '',
            playerName: '',
            honors: [],
            role: '',
            experiences: [],
            matchTypeTitle: '',
            sportTypeTitle: '',
            ages: [],
            weight: '',
            height: '',
            placeName: '',
            contactDetails: {},
            state: '',
            city: '',
            showImage: false,
            category: {},
            coachDegree: {},
            degreeOfEducation: '',
            coach: '',
            city: '',
            aboutMe: ''
        };
    }

    componentDidMount() {
        ResumeAPI.get(this.props.navigation.state.params.id)
            .then(response => {
                console.log('player_detail', response.data.data);
                this.setState({
                    playerImage: BASE_URL + '/' + response.data.data.user.pix,
                    honors: response.data.data.honors,
                    ages: response.data.data.ages,
                    experiences: response.data.data.experience,
                    matchTypeTitle: response.data.data.matchType.title,
                    sportTypeTitle: response.data.data.sportType.title,
                    weight: response.data.data.weight,
                    height: response.data.data.height,
                    placeName: response.data.data.placeName,
                    contactDetails: response.data.data.contact,
                    playerName: response.data.data.user.name + ' ' + response.data.data.user.lastName,
                    category: response.data.data.category,
                    coachDegree: response.data.data.coachDegree,
                    degreeOfEducation: response.data.data.degreeOfEducation + ' ' + response.data.data.major,
                    coach: response.data.data.coach,
                    aboutMe: response.data.data.aboutMe,
                    city: response.data.data.city.state.name + ' / ' + response.data.data.city.name
                    // description: response.data.data.fullDescription,
                    // city: response.data.data[0].city ? response.data.data[0].city.name : '',
                    // state: response.data.data[0].city ? response.data.data[0].city.state.name : '',
                })
            })
            .catch(response => {

            })
        this.navTitleView.fadeInUp(300)
    }


    _renderItem({ item, index }, parallaxProps) {
        return (
            <View style={styles.item}>
                <Image
                    source={{ uri: item.thumbnail }}
                    style={styles.image}
                />
            </View>
        );
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#FFF' }}>
                <Statusbar light={true} backgroundColor={Colors.PRIMARY} />
                <HeaderImageScrollView
                    maxHeight={hp('10%')}
                    minHeight={hp('8%')}
                    fadeOutForeground
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    overlayColor={'#FFFFFF00'}
                    renderFixedForeground={() => (
                        <View style={{ backgroundColor: '#000' }}>

                        </View>
                    )}
                    renderTouchableFixedForeground={() => (
                        <Animatable.View
                            useNativeDriver={true}
                            style={styles.navTitleView}
                            ref={navTitleView => {
                                this.navTitleView = navTitleView;
                            }}
                        >
                            <Toolbar
                                leftElement={I18nManager.isRTL ? "arrow-forward" : "arrow-back"}
                                onLeftElementPress={() => {
                                    NavigationController.goBack()
                                }}
                                rightElement={
                                    <Animatable.View
                                        useNativeDriver={true}
                                        onLayout={() => {
                                            this.imageViewRef.fadeOutDown(100)
                                        }}
                                        ref={ref => {
                                            this.imageViewRef = ref;
                                        }}>
                                        <View style={{ height: wp('15%'), width: wp('15%'), borderRadius: wp('12%'), backgroundColor: Colors.PRIMARY, justifyContent: 'center', marginStart: wp('4%') }}>
                                            <Image
                                                source={{ uri: this.state.playerImage }}
                                                style={{ height: wp('10%'), width: wp('10%'), borderRadius: wp('7%'), alignSelf: 'center' }} />
                                        </View>
                                    </Animatable.View>
                                }
                                centerElement={
                                    <Animatable.Text
                                        useNativeDriver={true}
                                        onLayout={() => {
                                            this.titleView.fadeOutDown(100)
                                        }}
                                        style={{ fontFamily: IranSansMobile, color: Colors.WHITE }}
                                        ref={ref => {
                                            this.titleView = ref;
                                        }}>
                                        {this.state.playerName}
                                    </Animatable.Text>

                                }
                                // centerElement
                                // rightElement="menu"
                                style={{ container: { backgroundColor: Colors.PRIMARY } }}
                                onRightElementPress={(label) => { NavigationController.goBack() }}
                            />
                        </Animatable.View>
                    )}
                >

                    <View style={{ backgroundColor: '#f1f1f1' }}>
                        <View style={{ width: wp('100%'), paddingTop:24, backgroundColor: Colors.PRIMARY, justifyContent: 'flex-start', flexDirection: 'column', alignItems: 'center' }}>
                            <TriggeringView
                                onBeginDisplayed={() => {
                                    this.imageViewRef.zoomOut(200)
                                }}
                                onHide={() => {
                                    this.imageViewRef.zoomIn(200)
                                    this.setState({
                                        showImage: true
                                    })
                                }}
                            >
                                <View style={{ height: 128, width: 128, borderRadius: wp('25%'), backgroundColor: Colors.WHITE, justifyContent: 'center' }}>
                                    <View style={{ height: 124, width: 124, borderRadius: wp('25%'), backgroundColor: Colors.PRIMARY, justifyContent: 'center', alignSelf: 'center' }}>
                                        <Image
                                            source={{ uri: this.state.playerImage }}
                                            style={{ height: 120, width: 120, borderRadius: wp('25%'), alignSelf: 'center' }} />
                                    </View>
                                </View>
                            </TriggeringView>
                            <View style={{ paddingVertical:8,  }}>
                                <TriggeringView
                                    onDisplay={() => {
                                        this.titleView.fadeOutDown(200)
                                    }}
                                    onHide={() => {
                                        this.titleView.fadeInUp(200)
                                    }}
                                >
                                    <Text style={[styles.name, { color: Colors.WHITE, fontSize: wp('5%'), textAlign: 'center' }]} >{this.state.playerName}</Text>
                                    <Text style={[styles.name, { color: Colors.WHITE, fontSize: wp('3%'), textAlign: 'center' }]} >{this.state.city}</Text>
                                </TriggeringView>
                                <Text style={[styles.name, { color: Colors.WHITE, fontSize: wp('4%') }]} >{this.state.role}</Text>
                            </View>
                        </View>


                        {/* <View style={{ height: wp('0.5%'), width: '100%', marginTop: wp('1%'), backgroundColor: Colors.LIGHTBLUE }} />

                        <View style={{ padding: wp('5%') }}>
                            <Text style={[styles.name, { color: Colors.PRIMARY, lineHeight: wp('6%') }]}>{this.state.description}</Text>
                        </View> */}
                        {/* <View style={styles.section}>
                        <Text style={[styles.name, { color: Colors.PRIMARY }]}>اصفهان {'>'} اصفهان</Text>
                        <Text style={[styles.name, { color: Colors.PRIMARY, marginStart: wp('5%'), marginTop: hp('3%') }]} ellipsizeMode="middle" >اصفهان خیابان آبشار حد فاصل خیابان فیض و آپادانا پلاک 3 کوچه هفتاد</Text>
                        <Ripple style={{ padding: wp('2%'), width: wp('30%'), borderWidth: 1, borderColor: Colors.PRIMARY, borderRadius: hp('1%'), flexDirection: 'row', marginTop: hp('3%') }} >
                            <MDIcons name="map-marker-circle" size={wp('5%')} />
                            <Text style={[styles.name, { color: Colors.PRIMARY }]}>آدرس روی نقشه</Text>
                        </Ripple>
                    </View>
                    <Ripple style={[styles.section, { flexDirection: 'row' }]}>
                        <FAIcons name="exclamation-triangle" size={wp('5%')} />
                        <Text style={[styles.name, { color: Colors.PRIMARY, marginStart: wp('4%') }]}>گزارش مشکل در آگهی</Text>
                    </Ripple> */}
                        <View style={{flex:1, flexDirection:'row', backgroundColor:'#FFF'}}>
                            <View style={{flex:1, aspectRatio:1, justifyContent:'center'}}>
                                <Text style={[styles.faFont, {textAlign:'center', fontSize:10, color:'#999'}]}>قد</Text>
                                <Text style={[styles.faFont, {textAlign:'center', fontSize:12, color:'#333'}]}>{this.state.height} سانتی متر</Text>
                            </View>
                            <View style={{flex:1, aspectRatio:1, justifyContent:'center'}}>
                                <Text style={[styles.faFont, {textAlign:'center', fontSize:10, color:'#999'}]}>رشته ورزشی</Text>
                                <Text style={[styles.faFont, {textAlign:'center', fontSize:12, color:'#333'}]}>{this.state.category.name}</Text>
                            </View>
                            <View style={{flex:1, aspectRatio:1, justifyContent:'center'}}>
                                <Text style={[styles.faFont, {textAlign:'center', fontSize:10, color:'#999'}]}>وزن</Text>
                                <Text style={[styles.faFont, {textAlign:'center', fontSize:12, color:'#333'}]}>{this.state.weight} کیلوگرم</Text>
                            </View>
                        </View>
                        <View style={{borderBottomColor: Colors.PRIMARY, borderBottomWidth:.5}} />

                        <View style={{ paddingHorizontal: wp('10%'), paddingVertical: wp('5%') }}>
                            <View style={{flexDirection:'row'}}>
                                <Text style={[styles.name, {fontSize:10}]}>نوع فعالیت ورزشی : </Text>
                                <Text style={[styles.name, {fontSize:12, color:'#333'}]}>{this.state.sportTypeTitle}</Text>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <Text style={[styles.name, {fontSize:10}]}>نام محل تمرین : </Text>
                                <Text style={[styles.name, {fontSize:12, color:'#333'}]}>{this.state.placeName}</Text>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <Text style={[styles.name, {fontSize:10}]}>نام مربی : </Text>
                                <Text style={[styles.name, {fontSize:12, color:'#333'}]}>{this.state.coach}</Text>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <Text style={[styles.name, {fontSize:10}]}>رده(ها)ی سنی : </Text>
                                <Text style={[styles.name, {fontSize:12, color:'#333'}]}>
                                {
                                    this.state.ages.map((item, index) => {
                                        if (index !== this.state.ages.length - 1) {
                                            return (
                                                item.title + ' / '
                                            )
                                        } else {
                                            return (
                                                item.title
                                            )
                                        }
                                    })
                                }
                                </Text>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <Text style={[styles.name, {fontSize:10}]}>سابقه فعالیت : </Text>
                                <Text style={[styles.name, {fontSize:12, color:'#333'}]}>
                                {
                                    this.state.experiences.map((item, index) => {
                                        if (index !== this.state.experiences.length - 1) {
                                            return (
                                                item.title + ' , '
                                            )
                                        } else {
                                            return (
                                                item.title
                                            )
                                        }
                                    })
                                }
                                </Text>
                            </View>
                            {
                                this.state.degreeOfEducation.length > 1 ? (
                                    <View style={{flexDirection:'row'}}>
                                        <Text style={[styles.name, {fontSize:10}]}>مدرک تحصیلی : </Text>
                                        <Text style={[styles.name, {fontSize:12, color:'#333'}]}>{this.state.degreeOfEducation}</Text>
                                    </View>
                                ) : (null)
                            }
                            <Text style={[styles.name, { color: '#333', fontSize:12, paddingVertical:8 }]}>
                                {
                                    this.state.aboutMe !== null ? this.state.aboutMe : ''
                                }
                            </Text>
                        </View>
                    </View>

                    
                    <View style={{borderBottomColor: Colors.PRIMARY, borderBottomWidth:.5}} />
                    <View style={{flex:1, flexDirection:'row', justifyContent:'center'}}>
                        <Ripple style={{flex:1, aspectRatio:1, justifyContent:'center'}} onPress={() => Linking.openURL(`mailto:${this.state.contactDetails.email}`)}>
                            <MCIcons name='email' color={'#999'} size={24} style={{alignSelf:'center'}}  />
                            <Text style={[styles.faFont, {textAlign:'center', fontSize:10}]}>ایمیل</Text>
                        </Ripple>
                        <Ripple style={{flex:1, aspectRatio:1, justifyContent:'center'}}  onPress={() => Linking.openURL(`tel:${this.state.contactDetails.mobile}`)}>
                            <SLIcon name='screen-smartphone' color={'#999'} size={24} style={{alignSelf:'center'}} />
                            <Text style={[styles.faFont, {textAlign:'center', fontSize:10}]}>شماره تماس</Text>
                        </Ripple>
                        <Ripple style={{flex:1, aspectRatio:1, justifyContent:'center'}} onPress={() => Linking.openURL(`https://t.me/${this.state.contactDetails.telegram}`)}>
                            <MCIcons name='telegram' color={'#999'} size={24} style={{alignSelf:'center'}}  />
                            <Text style={[styles.faFont, {textAlign:'center', fontSize:10}]}>تلگرام</Text>
                        </Ripple>
                        <Ripple style={{flex:1, aspectRatio:1, justifyContent:'center'}} onPress={() => Linking.openURL(`https://instagram.com/${this.state.contactDetails.insta}`)}>
                            <MCIcons name='instagram' color={'#999'} size={24} style={{alignSelf:'center'}}  />
                            <Text style={[styles.faFont, {textAlign:'center', fontSize:10}]}>اینستاگرام</Text>
                        </Ripple>
                    </View>
                    <View style={{borderBottomColor: Colors.PRIMARY, borderBottomWidth:.5}} />

                   {
                       this.state.honors.length > 0 ? 
                        (
                            <View style={{ backgroundColor: Colors.WHITE, paddingVertical:8 }}>
                                <Text style={[styles.faFont, {fontSize:12, color:'#999', padding:4, textAlign:'center'}]}>ویترین افتخارات : </Text>
                                <FlatList
                                    data={this.state.honors}
                                    style={{ width: '100%', height: null }}
                                    contentContainerStyle={{ justifyContent: 'flex-start' }}
                                    renderItem={({ item, index }) => {
                                        return (
                                            // <Text style={[styles.name, { color: Colors.PRIMARY, lineHeight: wp('6%'), marginStart: wp('3%') }]}>{item.title} در سال {item.year}</Text>
                                            <View style={{flex:1, flexDirection:'row'}}>
                                                <Text style={[{flex:1, height:32, lineHeight:32, fontSize:12, color:'#333', paddingHorizontal:8}, styles.faFont]}>{item.title}</Text>
                                                <Text style={[{width:56, height:32, lineHeight:32, fontSize:12, color:'#333', textAlign:'center'}, styles.faFont]}>{item.year}</Text>
                                            </View>
                                        )
                                    }}
                                />
                            </View>
                        ) 
                       :
                        (null)
                   }

                </HeaderImageScrollView>
                {/* <View style={styles.contactContainer}>
                    <Ripple style={{ backgroundColor: Color.BUTTON_ACCEPT, height: '100%', justifyContent: 'center', borderRadius: 2 }} >
                        <Text style={{ alignSelf: 'center', fontFamily: IranYekanMobile, color: Colors.WHITE }} >اطلاعات تماس</Text>
                    </Ripple>
                </View> */}
            </View >
        );
    }

    renderSocialIcons() {

    }
}

const styles = StyleSheet.create({
    title: {
        fontSize: 15,
        fontFamily: IranSansMobileBold,
    },
    name: {
        fontFamily: IranYekanMobile,
        fontSize: 10,
        color: '#999',
    },
    section: {
        padding: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#cccccc',
        backgroundColor: 'white',
    },
    sectionTitle: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    sectionContent: {
        fontSize: 16,
        textAlign: 'justify',
    },
    keywords: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
    },
    keywordContainer: {
        backgroundColor: '#999999',
        borderRadius: 10,
        margin: 10,
        padding: 10,
    },
    keyword: {
        fontSize: 16,
        color: 'white',
    },
    titleContainer: {
        flex: 1,
        alignSelf: 'stretch',
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageTitle: {
        color: 'white',
        backgroundColor: 'transparent',
        fontSize: 24,
    },
    navTitleView: {
        height: MIN_HEIGHT,
        justifyContent: 'center',
        alignItems: 'center',
        opacity: 0,
    },
    navTitle: {
        color: 'white',
        fontSize: 16,
        backgroundColor: 'transparent',
        fontFamily: IranSansMobile,
        marginBottom: hp('3%'),
    },
    sectionLarge: {
        height: 600,
    },
    contactContainer: {
        height: hp('7%'),
        width: wp('100%'),
        backgroundColor: '#f1f1f1',
        borderTopWidth: 1,
        borderTopColor: Colors.PRIMARY,
        paddingHorizontal: 10,
        paddingVertical: 3
    },
    item: {
        width: screenWidth - 60,
        height: screenWidth - 90,
        marginTop: -20
    },
    imageContainer: {
        flex: 1,
        marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
        backgroundColor: 'white',
        borderRadius: 8,
    },
    image: {
        resizeMode: 'cover',
        width: wp('100%'),
        height: hp('50%'),
    },
    faFont:
    {
        fontFamily: IranSansMobile,
    }
});


export default PlayerDetails;

// renderTouchableFixedForeground={() => (
                //     <Animatable.View
                //         useNativeDriver={true}
                //         style={styles.navTitleView}
                //         ref={navTitleView => {
                //             this.navTitleView = navTitleView;
                //         }}
                //     >
                //         <Toolbar
                //             leftElement={I18nManager.isRTL ? "arrow-forward" : "arrow-back"}
                //             onLeftElementPress={() => {
                //                 NavigationController.goBack()
                //             }}
                //             centerElement={this.state.title}
                //             // centerElement
                //             // rightElement="menu"
                //             onRightElementPress={(label) => { NavigationController.goBack() }}
                //         />
                //     </Animatable.View>
                // )}
                // renderForeground={() => (
                //     <View>
                //         <Image source={require('../../assets/images/logo.png')} />
                //     </View>
                // )}