import React, { Component } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { View, Text, Image, I18nManager, StyleSheet, ScrollView } from 'react-native'
import _ from 'lodash'
import RF from "react-native-responsive-fontsize"
import { Toolbar } from 'react-native-material-ui'
import NavigationController from '../../controllers/NavigationController';
import Colors from '../../constants/Colors';
import { IranSansMobile, IranSansMobileBold, IranYekanMobile, IranYekanMobileBold } from '../../constants/Fonts';
import { Statusbar } from '../../components/viac';

export default class About extends Component {

    constructor(props) {
        super(props)

        this.state = {

        }
    }

    componentDidMount() {

    }



    render() {
        return (
            <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
                <Statusbar light={false} backgroundColor={Colors.WHITE} />
                <Toolbar
                    ref={ref => this.toolbar = ref}
                    leftElement={I18nManager.isRTL ? "arrow-forward" : "arrow-back"}
                    centerElement="درباره ما"
                    onLeftElementPress={() => {
                        NavigationController.goBack()
                    }}
                />
                {/* <Ripple onPress={() => NavigationController.navigate('AdvertisementDetails')} >
                    <Text style={{ fontFamily: IranSansMobileBold }} >جزییات آگهی</Text>
                </Ripple> */}
                <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1, width: wp('100%'), alignSelf: 'center' }}>
                    <Text style={{ color: Colors.PRIMARY, fontFamily: IranSansMobileBold, fontSize: RF(3.5), marginHorizontal: wp('5%') }}>
                        داستان ما ...
                    </Text>
                    <Text style={{ color: Colors.PRIMARY, fontFamily: IranYekanMobile, fontSize: RF(2.3), lineHeight: wp('5%'),marginHorizontal: wp('7%'), textAlign:'justify'  }}>
                        ویاک با هدف قرار دادن جامعه ورزشی قصد دارد یک فضای مناسب برای آگهی ها و آگـهی های ورزشی ایجاد کند. اینجا شما خیلی راحت تر از هر راه دیگـری میتوانید با کسانی که در جامعه ورزشی هستند در تماس باشید. علاوه بر این شما میتوانید اینجا یک رزومه ورزشی هم داشته باشید و بین صد ها ورزشکار و مربی دیده شوید.
                    </Text>
                    <Image source={require('../../assets/images/viac_about_running.png')} style={{ width: '100%', height: hp('55%'), margin: wp('5%'), alignSelf: 'center' }} />
                    <Text style={{ color: Colors.PRIMARY, fontFamily: IranSansMobileBold, fontSize: RF(3.5), marginHorizontal: wp('5%')  }}>
                        مناسب برای شما ...
                    </Text>
                    <Text style={{ color: Colors.PRIMARY, fontFamily: IranYekanMobile, fontSize: RF(2.3), lineHeight: wp('5%'), marginHorizontal: wp('7%'), textAlign:'justify'  }}>
                        یکی از قابلیت های مهم ما جامعیت و گستردگی عرصه فعالیتمان هست از این رو اینجا هر کس و هر چیزی که به دنیای ورزش مربوط باشد میتوانید یک سهم داشته باشد این شامل باشگاه ها ، سالن ها ، مربی ها ، فروشگاه ها و ... می شود. ما امید داریم که خیلی زود دنیای ورزشـی هیجان انگـیزی داریم.
                    </Text>
                    <Image source={require('../../assets/images/image_bg.png')} style={{ width: '100%', height: hp('55%'), margin: wp('5%'), alignSelf: 'center' }} />
                    <Text style={{ color: Colors.PRIMARY, fontSize: RF(2), lineHeight: wp('5%'), alignSelf: 'center' }}>
                        Viac v1.5.0 By RAPICO.IR
                    </Text>
                </ScrollView>
            </View>
        )
    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    textContainer: {
        padding: wp('5%')
    },
    bottomContainer: {
        height: hp('8%'),
        bottom: 0,
        position: 'absolute',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: wp('3%'),
    },
    textContentStyle: {
        fontFamily: IranSansMobile,
        color: Colors.PRIMARY,
        fontSize: RF(2)
    },
    textTitleStyle: {
        fontFamily: IranSansMobileBold,
        color: Colors.PRIMARY,
        fontSize: RF(2.5)
    },
    listItemContainer: {
        height: hp('20%'),
        width: wp('90%'),
        elevation: 5,
        backgroundColor: Colors.WHITE,
        marginTop: wp('3%'),
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        marginBottom: 10,
    },
    itemTextStyle: {
        fontFamily: IranYekanMobile,
        color: Colors.PRIMARY,
        fontSize: RF(2.5)
    }
})
