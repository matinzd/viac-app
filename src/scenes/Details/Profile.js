import React, { Component } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { View, Text, Image, I18nManager, StyleSheet, Clipboard, TextInput, ActivityIndicator, PermissionsAndroid, ToastAndroid, TouchableOpacity } from 'react-native'
import Ripple from 'react-native-material-ripple';
import MapView, { Marker, MapViewAnimated } from 'react-native-maps'
import _ from 'lodash'
import RF from "react-native-responsive-fontsize"
import { setStore, getStore } from 'trim-redux';
import Modal from 'react-native-modal';
import { Toolbar, RadioButton } from 'react-native-material-ui'
import { Statusbar } from '../../components/viac';
import Colors from '../../constants/Colors';
import { IranSansMobile, IranYekanMobile } from '../../constants/Fonts';
import Color from '../../styles/Color';
import ImagePicker from 'react-native-image-picker';
import UserAPI from '../../webservices/UserAPI';
import NavigationController from '../../controllers/NavigationController';
import { BASE_URL } from '../../webservices/BaseAdapter';
import { TextField } from 'react-native-material-textfield';
import FaIcon from 'react-native-vector-icons/FontAwesome'
import ViacToast from '../../controllers/ViacToast';

export default class Profile extends Component {

    constructor(props) {
        super(props)

        this.state = {
            loading: false,
            image: {
                uri: null,
                type: '',
                name: ''
            },
            userInfo: {
                name: '',
                lastName: '',
                email: '',
                pix: '',
                sex: 1,
                reagentCode: '',
                fromReagentCode: ''
            },
            removePic: false
        }
    }

    componentWillMount() {
        let userInfo = getStore('userInfo');
        console.log('User info sex', userInfo)
        this.setState({
            userInfo: {
                name: userInfo.name,
                lastName: userInfo.lastName,
                email: userInfo.email ? userInfo.email : '',
                pix: userInfo.pix,
                sex: parseInt(userInfo.sex),
                reagentCode: userInfo.reagentCode,
                fromReagentCode: userInfo.fromReagentCode,
            },
            image: {
                uri: BASE_URL + '/' + userInfo.pix
            }
        })
    }

    submit() {
        this.formData = new FormData()
        if (! this.state.removePic && ! this.state.image.uri.includes('http')) {
            this.formData.append('pic', this.state.image)
        }
        this.formData.append('removePic', this.state.removePic ? 1 : 0)
        this.formData.append('name', this.state.userInfo.name)
        this.formData.append('lastName', this.state.userInfo.lastName)
        this.formData.append('sex', this.state.userInfo.sex)
        this.formData.append('fromReagentCode', this.state.userInfo.fromReagentCode)
        this.setState({ loading: true })
        UserAPI.updateMe(this.formData)
            .then(response => {
                this.setState({ loading: false })
                ViacToast.showToast(response.data.message, 3000)
                if (response.data.success) {
                    NavigationController.goBack()
                }
            })
            .catch(response => {
                console.log(JSON.parse(JSON.stringify(response)))
                this.setState({ loading: false })
                ViacToast.showToast('حطا در ثبت تغییرات', 2000)
            })
    }


    options = {
        title: 'انتخاب عکس',
        storageOptions: {
            skipBackup: true,
            path: 'images',
        },
        chooseFromLibraryButtonTitle: 'انتخاب از گالری',
        takePhotoButtonTitle: 'گرفتن عکس جدید',
        cancelButtonTitle: 'انصراف',
        customButtons: [
            {
                name: 'remove_profile_pic',
                title: 'حدف عکس پروفایل'
            }
        ]
    };

    pickImage() {

        this.grantPermission().then((isGranted) => {
            let isCameraGranted = isGranted["android.permission.CAMERA"]
            let isWriteStorageGranted = isGranted["android.permission.WRITE_EXTERNAL_STORAGE"]
            let isReadStorageGranted = isGranted["android.permission.READ_EXTERNAL_STORAGE"]
            if (isWriteStorageGranted || isCameraGranted) {
                ImagePicker.showImagePicker(this.options, (response) => {
                    if (response.customButton === 'remove_profile_pic') {
                        this.setState({
                            image: {
                                uri: null,
                                type: '',
                                name: ''
                            },
                            removePic: true
                        })
                        return
                    }
                    this.setState({
                        image: {
                            uri: null,
                            type: '',
                            name: ''
                        },
                        removePic: false
                    })
                    console.log('Response = ', response);

                    if (response.didCancel) {
                        console.log('User cancelled image picker');
                    } else if (response.error) {
                        console.log('ImagePicker Error: ', response.error);
                    } else if (response.customButton) {
                        console.log('User tapped custom button: ', response.customButton);
                    } else {
                        const source = { uri: response.uri };

                        // You can also display the image using data:
                        // const source = { uri: 'data:image/jpeg;base64,' + response.data };\
                        this.setState({
                            image: {
                                uri: response.uri,
                                type: response.type,
                                name: response.fileName
                            }
                        })

                    }
                });
            } else {
                ViacToast.showToast('دسترسی به حاظه داده نشد', 2000)
            }
        })
    }

    async grantPermission() {
        return await PermissionsAndroid.requestMultiple(
            [
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.CAMERA
            ]
        )
    }

    setClipboard() {
        Clipboard.setString(this.state.userInfo.reagentCode)
        ViacToast.showToast('کد معرف کپی شد ...', 2000)
    }

    render() {
        return (
            <View style={styles.container}>
                <Statusbar light={false} backgroundColor={Colors.WHITE} />
                <Toolbar
                    ref={ref => this.toolbar = ref}
                    leftElement={I18nManager.isRTL ? "arrow-forward" : "arrow-back"}
                    centerElement="ویرایش پروفایل"
                    onLeftElementPress={() => {
                        NavigationController.goBack()
                    }}
                    onRightElementPress={(label) => { NavigationController.openDrawer() }}
                />
                <View style={styles.infoContainer}>
                    <Ripple onPress={() => this.pickImage()} rippleContainerBorderRadius={wp('20%')} style={styles.imgContainer} r>
                        <Image source={this.state.image.uri ? { uri: this.state.image.uri } : require('../../assets/images/profile_default.png')} style={{
                            width: wp('39%'),
                            height: wp('39%'),
                            borderRadius: wp('20%'),
                            alignSelf: 'center'
                        }} />
                        <View style={styles.overlay} />
                    </Ripple>
                    <View style={styles.inputContainer}>
                        <TextField
                            labelTextStyle={{ fontFamily: IranSansMobile }}
                            label='نام'
                            value={this.state.userInfo.name}
                            defaultValue={this.state.userInfo.name}
                            style={{ width: '100%', fontFamily: IranSansMobile }}
                            onChangeText={email => {
                                this.state.userInfo.name = email
                            }} />
                        <TextField
                            labelTextStyle={{ fontFamily: IranSansMobile }}
                            label='نام خانوادگی'
                            value={this.state.userInfo.lastName}
                            defaultValue={this.state.userInfo.lastName}
                            style={{ width: '100%', fontFamily: IranSansMobile }}
                            onChangeText={lastName => {
                                this.state.userInfo.lastName = lastName
                            }} />
                    </View>
                </View>
                <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                    <TouchableOpacity onPress={() => this.setClipboard()} style={{ padding: wp('3%') }}>
                        <Text style={{ alignSelf: 'center', fontFamily: IranYekanMobile, color: Colors.PRIMARY }}>کد معرف شما برای دیگران</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: wp('3%') }}>
                            <FaIcon name='clipboard' color={Colors.SECONDARY} size={wp('5%')} />
                            <Text style={{ alignSelf: 'center', marginStart: wp('2%') }}>{this.state.userInfo.reagentCode}</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={{ padding: wp('3%') }}>
                        <Text style={{ alignSelf: 'center', fontFamily: IranYekanMobile, color: Colors.PRIMARY }}>
                            کد معرف
                            {
                                this.state.userInfo.fromReagentCode.length !== 0 ?
                                    <Text style={{ color: Colors.Green, marginStart: wp('3%') }}>
                                        {'  (ثبت شده)'}
                                    </Text>
                                    :
                                    null
                            }
                        </Text>
                        <TextInput
                            maxLength={6}
                            underlineColorAndroid={Color.BUTTON_ACCEPT}
                            autoCapitalize='characters'
                            // underlineColorAndroid=
                            editable={this.state.userInfo.fromReagentCode.length === 0}
                            disabled={this.state.userInfo.fromReagentCode.length !== 0}
                            // value={this.state.userInfo.fromReagentCode}
                            defaultValue={this.state.userInfo.fromReagentCode}
                            style={{ width: wp('20%'), fontFamily: IranSansMobile }}
                            onChangeText={fromReagentCode => {
                                this.state.userInfo.fromReagentCode = fromReagentCode
                            }} />
                    </View>
                </View>
                <View style={{ flexDirection: 'row', alignSelf: 'center', margin: wp('2%') }}>
                    <RadioButton
                        label="من مرد هستم"
                        checked={this.state.userInfo.sex === 1}
                        value={this.state.userInfo.sex}
                        onCheck={() => {
                            this.state.userInfo.sex = 1
                            this.setState({ userInfo: this.state.userInfo })
                        }}
                        onSelect={() => {
                            this.state.userInfo.sex = 1
                            this.setState({ userInfo: this.state.userInfo })
                        }}

                    />
                    <RadioButton

                        label="من زن هستم"
                        checked={this.state.userInfo.sex === 0}
                        value={this.state.userInfo.sex}
                        onCheck={checked => {
                            this.state.userInfo.sex = 0
                            this.setState({ userInfo: this.state.userInfo })
                        }}
                        onSelect={() => {
                            this.state.userInfo.sex = 1
                            this.setState({ userInfo: this.state.userInfo })
                        }}
                    />
                </View>
                <Ripple disabled={this.state.loading} onPress={() => this.submit()} style={{ height: wp('10%'), width: wp('80%'), justifyContent: 'center', backgroundColor: Color.BUTTON_ACCEPT, marginHorizontal: wp('10%'), alignSelf: 'center', borderRadius: wp('1%') }}>
                    {
                        this.state.loading ?
                            <ActivityIndicator size={wp('7%')} color={Colors.WHITE} />
                            :
                            <Text style={{ fontFamily: IranSansMobile, color: Colors.WHITE, alignSelf: 'center' }}>ثبت تغییرات</Text>
                    }
                </Ripple>
            </View>
        )
    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    textContainer: {
        padding: wp('5%')
    },
    infoContainer: {
        flexDirection: 'row',
    },
    imgContainer: {
        width: wp('40%'),
        height: wp('40%'),
        borderRadius: wp('20%'),
        marginVertical: wp('10%'),
        marginHorizontal: wp('3%'),
        justifyContent: 'center',
        backgroundColor: Colors.SECONDARY
    },
    inputContainer: {
        justifyContent: 'center',
        width: '100%',
        margin: 4,
        flex: 1
    },
    bottomContainer: {
        height: hp('8%'),
        bottom: 0,
        position: 'absolute',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: wp('3%'),
    },
    textContentStyle: {
        fontSize: RF(2)
    },
    textTitleStyle: {
        fontSize: RF(2.5)
    },
    overlay: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        backgroundColor: '#000',
        opacity: 0.2,
        borderRadius: wp('20%')
    }
})
