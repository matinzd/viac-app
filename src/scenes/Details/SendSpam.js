import React, { Component } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { View, Text, Image, I18nManager, StyleSheet, Modal, TextInput, ToastAndroid } from 'react-native'
import Ripple from 'react-native-material-ripple';
import MapView, { Marker, MapViewAnimated } from 'react-native-maps'
import _ from 'lodash'
import RF from "react-native-responsive-fontsize"
import { setStore, getStore, connect } from 'trim-redux';
// import Modal from 'react-native-modal';
import { Toolbar } from 'react-native-material-ui'
import { Statusbar } from '../../components/viac';
import Colors from '../../constants/Colors';
import Color from '../../styles/Color';
import NavigationController from '../../controllers/NavigationController';
import { IranSansMobile, IranSansMobileBold } from '../../constants/Fonts';
import AdvertisingAPI from '../../webservices/AdvertisingAPI';
import ViacToast from '../../controllers/ViacToast';


class SendSpam extends Component {

    constructor(props) {
        super(props)

        this.state = {
            userText: ''
        }
    }

    componentDidMount() {

    }

    sendSpamMessage() {
        AdvertisingAPI.sendWarning(this.props.navigation.state.params.id, this.state.userText)
            .then(response => {
                ViacToast.showToast(response.data.message, 2000)
                if( response.data.success ) {
                    NavigationController.goBack()
                }
            })
            .catch(response => {
                ViacToast.showToast('خطا در ارسال گزارش', 2000)
            })
    }

    render() {
        return (
            <View style={styles.container}>
                <Statusbar light={false} backgroundColor={Colors.WHITE} />
                <Toolbar
                    ref={ref => this.toolbar = ref}
                    leftElement={I18nManager.isRTL ? "arrow-forward" : "arrow-back"}
                    centerElement="گزارش اشکال در آگهی"
                    onLeftElementPress={() => {
                        NavigationController.goBack()
                    }}
                />
                <View style={styles.textContainer}>
                    <TextInput multiline style={{ fontFamily: IranSansMobile, marginTop: -wp('5%'), fontSize: RF(2) ,height: hp('30%') , borderBottomColor: Colors.SECONDARY, borderBottomWidth: 1}} onChangeText={description => this.setState({ userText: description })} placeholder='لطفا گزارش خود را شرح دهید ...' />
                </View>
                <View style={styles.bottomContainer}>
                    <Ripple onPress={() => NavigationController.goBack()} style={{width: wp('30%') ,backgroundColor: Colors.WHITE, justifyContent: 'center', borderColor: Color.BUTTON_ACCEPT, borderWidth: 1, borderRadius: wp('1%'), padding: 10 }}>
                        <Text style={[styles.textContentStyle, { color: Color.BUTTON_ACCEPT, textAlign: 'center' }]}>لغو</Text>
                    </Ripple>
                    <Ripple onPress={() => { this.sendSpamMessage() }} style={{ width: wp('30%'), backgroundColor: Color.BUTTON_ACCEPT, borderRadius: wp('1%'), justifyContent: 'center', paddingHorizontal: wp('5%'), marginStart: wp('5%') }}>
                        <Text style={[styles.textContentStyle, { color: Colors.WHITE, textAlign: 'center' }]}>ارسال گزارش</Text>
                    </Ripple>
                </View>
            </View>
        )
    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    textContainer: {
        padding: wp('5%')
    },
    bottomContainer: {
        height: hp('8%'),
        bottom: 0,
        position: 'absolute',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: wp('3%'),
    },
    textContentStyle: {
        fontFamily: IranSansMobile,
        color: Colors.PRIMARY,
        fontSize: RF(2)
    },
    textTitleStyle: {
        fontFamily: IranSansMobileBold,
        color: Colors.PRIMARY,
        fontSize: RF(2.5)
    }
})

export default SendSpam