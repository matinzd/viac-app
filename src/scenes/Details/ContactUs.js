import React, { Component } from 'react';
import { View, Text, StyleSheet, ActivityIndicator, I18nManager, ScrollView, TextInput } from 'react-native'
import { Toolbar } from 'react-native-material-ui';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Ripple from 'react-native-material-ripple';
import RF from 'react-native-responsive-fontsize';
import * as Animatable from 'react-native-animatable'
import NavigationController from '../../controllers/NavigationController';
import { Statusbar } from '../../components/viac';
import Colors from '../../constants/Colors';
import { IranYekanMobile, IranSansMobile } from '../../constants/Fonts';
import { TextField } from 'react-native-material-textfield';
import Color from '../../styles/Color';
import ContactAPI from '../../webservices/ContactAPI';
import ViacToast from '../../controllers/ViacToast';

class ContactUs extends Component {

    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            fullName: '',
            subject: '',
            email: '',
            phoneNumber: '',
            content: ''
        }
    }

    componentDidMount() {

    }

    submit() {
        this.setState({ loading: true })
        ContactAPI.send(this.state.fullName, this.state.subject, this.state.email, this.state.phoneNumber, this.state.content)
            .then(response => {
                this.setState({ loading: false })
                ViacToast.showToast(response.data.message, 1000)
                if (response.data.success) {
                    NavigationController.goBack()
                }
            })
            .catch(response => {
                this.setState({ loading: false })
                ViacToast.showToast('خطا در ارسال پیام', 1000)
            })
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
                <Statusbar light={false} backgroundColor={Colors.WHITE} />
                <Toolbar
                    ref={ref => this.toolbar = ref}
                    leftElement={I18nManager.isRTL ? "arrow-forward" : "arrow-back"}
                    centerElement={
                        <Text style={{ alignSelf: 'flex-start', color: Colors.SECONDARY, fontFamily: IranYekanMobile, fontSize: RF(2.5) }}>تماس با ما</Text>
                    }
                    onLeftElementPress={() => {
                        NavigationController.goBack()
                    }}
                    onRightElementPress={(label) => { NavigationController.openDrawer() }}
                />
                <ScrollView showsHorizontalScrollIndicator={false} style={{ padding: wp('10%') }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{ width: wp('40%') }}>
                            <TextField
                                labelTextStyle={{ fontFamily: IranSansMobile }}
                                label='نام'
                                style={{ width: '50%', fontFamily: IranSansMobile }}
                                onChangeText={fullName => {
                                    this.state.fullName = fullName
                                }} />
                        </View>
                        <View style={{ width: wp('40%'), marginStart: wp('5%') }}>
                            <TextField
                                labelTextStyle={{ fontFamily: IranSansMobile }}
                                label='شماره تماس'
                                keyboardType='phone-pad'
                                style={{ width: '50%', fontFamily: IranSansMobile }}
                                onChangeText={phoneNumber => {
                                    this.state.phoneNumber = phoneNumber
                                }} />
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{ width: wp('40%') }}>
                            <TextField
                                labelTextStyle={{ fontFamily: IranSansMobile }}
                                label='ایمیل'
                                keyboardType='email-address'
                                style={{ width: '50%', fontFamily: IranSansMobile }}
                                onChangeText={email => {
                                    this.state.email = email
                                }} />
                        </View>
                        <View style={{ width: wp('40%'), marginStart: wp('5%') }}>
                            <TextField
                                labelTextStyle={{ fontFamily: IranSansMobile }}
                                label='موضوع پیام'
                                style={{ width: '50%', fontFamily: IranSansMobile }}
                                onChangeText={subject => {
                                    this.state.subject = subject
                                }} />
                        </View>


                    </View>
                    <View style={{ width: '100%', justifyContent: 'center', height: hp('30%'), marginStart: wp('5%') }}>
                        <TextField
                            labelTextStyle={{ fontFamily: IranSansMobile }}
                            label='شرح موضوع شما'
                            multiline={true}
                            numberOfLines={5}
                            maxLength={256}
                            inputContainerStyle={{ height: hp('30%') }}
                            containerStyle={{ height: hp('30%') }}
                            style={{ width: '100%', fontFamily: IranSansMobile }}
                            onChangeText={content => {
                                this.state.content = content
                            }} />
                    </View>
                    <Ripple disabled={this.state.loading} onPress={() => { this.submit() }} style={{ height: wp('10%'), width: wp('80%'), justifyContent: 'center', backgroundColor: Color.BUTTON_ACCEPT, marginHorizontal: wp('10%'), alignSelf: 'center', borderRadius: wp('1%'), marginVertical: wp('10%') }}>
                        {
                            this.state.loading ?
                                <ActivityIndicator size={wp('7%')} color={Colors.WHITE} />
                                :
                                <Text style={{ fontFamily: IranSansMobile, color: Colors.WHITE, alignSelf: 'center' }}>ارسال</Text>
                        }
                    </Ripple>
                </ScrollView>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    resumeItemImgBg: { flex: 1, elevation: 5, margin: wp('3%') },
    overlay: { position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, backgroundColor: '#000', opacity: 0.5 }
})


export default ContactUs