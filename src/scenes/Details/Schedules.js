import React, { Component } from 'react';
import { View, StyleSheet, Text, TextInput, FlatList, I18nManager } from 'react-native'
import _ from 'lodash'
import states from '../../constants/data/states.json'
import { Toolbar } from 'react-native-material-ui'
import { Statusbar } from '../../components/viac';
import NavigationController from '../../controllers/NavigationController.js';
import Colors from '../../constants/Colors.js';
import Icon from 'react-native-vector-icons/Foundation'
import Ripple from 'react-native-material-ripple'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { IranYekanMobile } from '../../constants/Fonts.js';
import { Table, TableWrapper, Row, Rows, Col } from 'react-native-table-component';

export default class Schedules extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: _.filter(states, { parentId: null }),
            temp: _.filter(states, { parentId: null }),
            text: 'در قسمت شرح آگهی توضیحات در مورد سانس ها عنوان شده است، برای اطمینان از صحت سانس ها با مدیریت آگهی تماس حاصل فرمایید.',
            tableHead: ['روز', 'ساعت شروع', 'ساعت پایان'],
            tableData: [
                ['شنبه', '10:30', '15:30'],
                ['یکشنبه', '10:30', '15:30'],
                ['دوشنبه', '10:30', '15:30'],
                ['سه شنبه', '10:30', '15:30']
            ]
        }
    }



    render() {
        return (
            <View style={styles.container} >
                <Statusbar light={true} backgroundColor={Colors.PRIMARY} />
                <Toolbar
                    ref={ref => this.toolbar = ref}
                    leftElement={I18nManager.isRTL ? "arrow-forward" : "arrow-back"}
                    centerElement="سانس ها"
                    // currentRoute='Tabs'
                    onLeftElementPress={() => {
                        NavigationController.goBack()
                    }}
                    onRightElementPress={(label) => { NavigationController.openDrawer() }}
                />
                <View style={{ flexDirection: 'row', padding: 20 }}>
                    <Icon name="info" size={20} color="#18a1e5" />
                    <Text style={styles.textStyle}>
                        {this.state.text}
                    </Text>
                </View>
                <View style={styles.tableContainer}>
                    <Table>
                        <Row data={this.state.tableHead} flexArr={[2, 1, 1]} style={styles.head} textStyle={[styles.text, { color: Colors.WHITE }]} />
                        <TableWrapper style={styles.wrapper} >
                            <Rows data={this.state.tableData} flexArr={[2, 1, 1]} style={styles.row} textStyle={styles.text} />
                        </TableWrapper>
                    </Table>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '',
    },
    itemContainer: {
        paddingVertical: wp('3%'),
        paddingHorizontal: wp('5%'),
        marginVertical: wp('1%'),
        marginHorizontal: wp('3%'),
        elevation: 3,
        backgroundColor: Colors.WHITE
    },
    textStyle: {
        fontFamily: IranYekanMobile,
        color: '#18a1e5',
        marginStart: wp('3%')
    },
    head: { height: 40, backgroundColor: Colors.PRIMARY },
    wrapper: { flexDirection: 'row' },
    title: { flex: 1, backgroundColor: '#f6f8fa' },
    row: { height: 28, backgroundColor: '#fff' },
    text: { textAlign: 'left', fontFamily: IranYekanMobile, marginStart: wp('2%') },
    tableContainer: {
        padding: 10
    }
})