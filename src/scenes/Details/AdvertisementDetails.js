import React, { Component } from 'react';
import { StyleSheet, FlatList, Text, View, Image, Dimensions, I18nManager, Platform, Linking, ToastAndroid, Share, StatusBar, TouchableOpacity, ProgressBarAndroid } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Header } from 'react-navigation';
import { Toolbar } from 'react-native-material-ui';
import { IranSansMobile, IranYekanMobile, IranSansMobileBold } from '../../constants/Fonts';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Colors from '../../constants/Colors'
import HeaderImageScrollView, { TriggeringView } from 'react-native-image-header-scroll-view';
import Ripple from 'react-native-material-ripple';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import MDIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MIcons from 'react-native-vector-icons/MaterialIcons';
import FAIcons from 'react-native-vector-icons/FontAwesome5';
import NavigationController from '../../controllers/NavigationController';
import { Statusbar, BottomDrawer } from '../../components/viac';
import AdvertisingAPI from '../../webservices/AdvertisingAPI';
import { BASE_URL } from '../../webservices/BaseAdapter';
import { setStore, getStore } from 'trim-redux';
import BottomSortAndFilter from '../Tabs/Advertisements/BottomSortAndFilter';
import Modal from 'react-native-modal';
import Terms from '../../modals/Terms';
import RF from 'react-native-responsive-fontsize';
import AntDIcon from 'react-native-vector-icons/AntDesign';
import Color from '../../styles/Color';
import ViacToast from '../../controllers/ViacToast';
import MapView, { Marker } from 'react-native-maps';
import MidCarousel from '../../components/MidCarousel'
import Video from 'react-native-video';


const MIN_HEIGHT = Header.HEIGHT;
const MAX_HEIGHT = 250;
const { width: screenWidth } = Dimensions.get('window')


class AdvertisementDetails extends Component {
    constructor() {
        super();
        this.state = {
            showNavTitle: false,
            entries: [],
            id: null,
            categoryId: null,
            activeSlide: 0,
            description: '',
            briefDescription: '',
            title: '',
            time: '',
            city: { name: '', state: { name: '' } },
            address: '',
            sans: [],
            tags: [],
            pix: [],
            resume: [],
            lat: 1,
            lng: 1,
            isBookmarked: false,
            advertisementId: null,
            isContactInfoVisible: false,
            mobileNumber: '',
            isLoading: true,
            ladder: {},
            video: null
        };
    }

    pagination() {
        const { entries, activeSlide } = this.state;
        return (
            <Pagination
                dotsLength={entries.length}
                activeDotIndex={activeSlide}
                containerStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.75)' }}
                dotStyle={{
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    marginHorizontal: 8,
                    backgroundColor: Color.BUTTON_ACCEPT
                }}
                inactiveDotStyle={{
                    // Define styles for inactive dots here
                }}
                inactiveDotOpacity={0.4}
                inactiveDotScale={0.6}
            />
        );
    }


    // {
    //     thumbnail: 'https://picsum.photos/200/300?grayscale'
    // },
    // {
    //     thumbnail: 'https://res.cloudinary.com/demo/image/upload/h_210/f_auto,q_auto/paint.jpg'
    // },
    // {
    //     thumbnail: 'https://picsum.photos/200/300?grayscale'
    // },
    // {
    //     thumbnail: 'https://res.cloudinary.com/demo/image/upload/h_210/f_auto,q_auto/paint.jpg'
    // },


    componentDidMount() {
        AdvertisingAPI.get(this.props.navigation.state.params.id).then(response => {

            let data = response.data.data
            data.pix.map(item => {
                this.state.entries.push({
                    thumbnail: BASE_URL + '/' + item.url
                })
            })
            if(data.video !== undefined)
                this.state.entries.push({thumbnail: BASE_URL + '/' + data.video})
            this.setState({
                title: data.title,
                id: data.id,
                ladder: data.ladder,
                video: data.video,
                resume: data.resume,
                categoryId: data.category.id,
                description: data.fullDescription,
                briefDescription: data.briefDescription,
                pix: data.pics,
                time: data.timeAgo,
                city: data.city,
                address: data.address,
                entries: this.state.entries,
                sans: data.sans,
                tags: data.tags,
                lat: data.latitude,
                lng: data.longitude,
                isBookmarked: data.isLike,
                advertisementId: data.id,
                mobileNumber: data.user.mobileNumber,
                isLoading: false,
            })
        })
    }

    onLocationPress() {
        Linking.openURL(`https://maps.google.com/?q=${this.state.lat},${this.state.lng}`)
    }

    _renderItem({ item, index }, parallaxProps) {
        return (
            <View style={styles.item}>
                <Image
                    source={{ uri: item.thumbnail }}
                    style={styles.image}
                />
            </View>
        );
    }

    onSharePressed() {
        let url = `https://www.viac-sport.com/b/detail/${this.state.advertisementId}/${this.state.title}`;
        let text = 'این آگهی را در ویاک ببینید !' + '\n\n' + url
        setTimeout(() => {
            Share.share({ message: text })
        }, 1000)
        // Linking.openURL()
    }

    onBookmarkPressed() {
        AdvertisingAPI.toggleLike(this.state.advertisementId).then(response => {
            ViacToast.showToast(response.data.message, 2000)
            if (response.data.success) {
                this.setState({
                    isBookmarked: !this.state.isBookmarked
                })
            }
        })
    }

    requestTermsView = () => {
        if (!getStore('areTermsAccepted')) {
            setStore({ isTermsVisible: true })
        } else {
            this.setState({ isContactInfoVisible: true })
        }
    }

    onAcceptTerms() {
        setStore({ isTermsVisible: false, areTermsAccepted: true })
        this.setState({ isContactInfoVisible: true })
    }

    callContact() {
        Linking.openURL(`tel:${this.state.mobileNumber}`)
    }

    messageContact() {
        Linking.openURL(`sms:${this.state.mobileNumber}${this.getSMSDivider()}body=`)
    }

    getSMSDivider() {
        return Platform.OS === "ios" ? "&" : "?";
    }

    reportSpam() {
        NavigationController.navigate('SendSpam', { id: this.state.advertisementId });
    }

    ondEditPress() {
        let editingObject = {
            type: 'edit',
            advertisementDetail: {
                id: this.state.id,
                title: this.state.title,
                categoryId: this.state.categoryId,
                briefDescription: this.state.briefDescription,
                fullDescription: this.state.description,
                address: this.state.address,
                pix: this.state.pix,
                latitude: this.state.lat,
                longitude: this.state.lng
            }
        }
        NavigationController.navigate('AddOrEditAdvertisement', editingObject)
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Statusbar light={false} backgroundColor={Colors.WHITE} />

                {
                    this.state.isLoading ?
                        (
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <Text style={[styles.faFont, { textAlign: 'center', color: Colors.PRIMARY }]}>در حال دریافت اطلاعات آگهی</Text>
                                <Text style={[styles.faFont, { textAlign: 'center', color: Colors.PRIMARY, fontSize: 10 }]}>لطفا کمی صبر کنید</Text>
                                <ProgressBarAndroid style={{ width: '50%', alignSelf: 'center', marginVertical: 8 }} color={Colors.PRIMARY} styleAttr="Horizontal" />
                            </View>
                        )
                        :
                        (
                            <View style={{ flex: 1 }}>
                                <HeaderImageScrollView
                                    maxHeight={hp('10%')}
                                    minHeight={hp('8%')}
                                    fadeOutForeground
                                    showsHorizontalScrollIndicator={false}
                                    showsVerticalScrollIndicator={false}
                                    overlayColor={'#FFFFFF00'}
                                    renderFixedForeground={() => (
                                        <View style={{ backgroundColor: '#000' }}>

                                        </View>
                                    )}
                                    renderTouchableFixedForeground={() => (
                                        <Toolbar
                                            leftElement={I18nManager.isRTL ? "arrow-forward" : "arrow-back"}
                                            onLeftElementPress={() => {
                                                NavigationController.goBack()
                                            }}
                                            centerElement={
                                                <Animatable.Text
                                                    onLayout={() => {
                                                        this.navTitleView.fadeOutDown(200)
                                                    }}
                                                    useNativeDriver={true}
                                                    style={{ fontFamily: IranSansMobile, color: Colors.SECONDARY }}
                                                    ref={navTitleView => {
                                                        this.navTitleView = navTitleView;
                                                    }}>
                                                    {this.state.title}
                                                </Animatable.Text>
                                            }

                                            onRightElementPress={(label) => { NavigationController.goBack() }}
                                        />
                                    )}
                                >

                                    {
                                        this.state.entries.length > 0 ? 
                                        (
                                            <MidCarousel
                                        entries={this.state.entries}
                                        sliderWidth={100}
                                        hasPagination={true}
                                        autoPlay={false}
                                        loop={true}
                                        renderItem={items => {
                                            const item = items.item
                                            console.log('renderItem', item.thumbnail.endsWith('mp4') + " : "+ item.thumbnail)
                                            return (
                                                <View style={{}}>
                                                    {
                                                        item.thumbnail.endsWith('mp4') && this.state.ladder && this.state.video && this.state.ladder.pri === 5 ? 
                                                        (
                                                            <Video
                                                                source={{ uri: item.thumbnail }}   // Can be a URL or a local file.
                                                                resizeMode='contain'
                                                                style={{width:'100%', height:200 }}          // Callback when video cannot be loaded
                                                            />
                                                        )
                                                        :
                                                        (
                                                            <View>
                                                                <Image source={{ uri: item.thumbnail }} style={{ height: 200, backgroundColor: '#fff', width: '100%', resizeMode: 'cover' }} />
                                                            </View>
                                                        )
                                                    }
                                                </View>
                                            )
                                        }}
                                    />
                                        ) 
                                        :
                                        (null)
                                    }

                                    {/* <Carousel
                                    sliderWidth={wp('100%')}
                                    sliderHeight={wp('40%')}
                                    itemWidth={screenWidth}
                                    data={this.state.entries}
                                    pagingEnabled={true}
                                    onSnapToItem={(index) => this.setState({ activeSlide: index })}
                                    renderItem={this._renderItem}
                                // hasParallaxImages={true}
                                />
                                {this.pagination()} */}

                                    <View style={{ backgroundColor: '#fff', padding: 10, elevation: 0 }}>
                                        <View style={styles.section}>
                                            <TriggeringView
                                                onDisplay={() => {
                                                    this.navTitleView.fadeOutDown(200)
                                                }}
                                                onBeginHidden={() => {
                                                    this.navTitleView.fadeInUp(200)
                                                }}
                                            >
                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                    <Text style={styles.title}>
                                                        {this.state.title}
                                                    </Text>
                                                    <Ripple
                                                        onPress={() => this.ondEditPress()}
                                                        style={{ padding: wp('2%'), opacity: getStore('userInfo').mobileNumber === this.state.mobileNumber ? 1 : 0, borderWidth: 1, borderColor: Colors.PRIMARY, borderRadius: hp('1%'), flexDirection: 'row' }} >
                                                        <MIcons name="edit" size={wp('5%')} />
                                                        <Text style={[styles.name, { color: Colors.PRIMARY, marginStart: wp('1%') }]}>ویرایش آگهی</Text>
                                                    </Ripple>
                                                </View>
                                            </TriggeringView>
                                            <Text style={styles.name}>{this.state.time}</Text>
                                            {
                                                this.state.tags.length > 0 ? (
                                                    <Text style={[styles.name, { color: '#5A6268', fontFamily: IranSansMobileBold }]}>تگ ها :
                                                    {
                                                            this.state.tags.map(item => (
                                                                <Text key={Math.random().toString()}> <Text style={{ color: '#F08F8D' }}>#</Text> {item.title} </Text>
                                                            ))
                                                        }
                                                    </Text>
                                                ) : null
                                            }

                                        </View>


                                        <View style={[styles.section, { flexDirection: 'row', justifyContent: 'space-around' }]}>
                                            <Ripple onPress={() => this.onBookmarkPressed()} style={{ padding: wp('2%'), borderWidth: 1, borderColor: Colors.PRIMARY, borderRadius: hp('1%'), flexDirection: 'row' }} >
                                                <MDIcons name={this.state.isBookmarked ? "bookmark" : "bookmark-outline"} size={wp('5%')} style={{ color: Colors.PRIMARY }} />
                                                <Text style={[styles.name, { color: Colors.PRIMARY, marginStart: wp('1%'), fontSize: RF(1.7) }]}>
                                                    {
                                                        this.state.isBookmarked ?
                                                            'نشان شده است'
                                                            :
                                                            'نشان کردن'
                                                    }
                                                </Text>
                                            </Ripple>
                                            <Ripple onPress={() => this.onSharePressed()} style={{ padding: wp('2%'), borderWidth: 1, borderColor: Colors.PRIMARY, borderRadius: hp('1%'), flexDirection: 'row' }} >
                                                <MDIcons name="share-variant" size={wp('5%')} style={{ color: Colors.PRIMARY }} />
                                                <Text style={[styles.name, { color: Colors.PRIMARY, marginStart: wp('1%'), fontSize: RF(1.7) }]}>اشتراک گذاری</Text>
                                            </Ripple>
                                            <Ripple style={{ padding: wp('2%'), opacity: this.state.sans.length <= 0 ? 0 : 1, borderWidth: 1, borderColor: Colors.PRIMARY, borderRadius: hp('1%'), flexDirection: 'row' }} >
                                                <MIcons name="schedule" size={wp('5%')} />
                                                <Text style={[styles.name, { color: Colors.PRIMARY, marginStart: wp('1%'), fontSize: RF(1.7) }]}>مشاهده سانس ها</Text>
                                            </Ripple>
                                        </View>

                                        <View style={styles.section}>
                                            {this.renderHighlightText(this.state.description)}
                                        </View>
                                        {/* {
                                            this.state.ladder && this.state.video && this.state.ladder.pri === 5 ?
                                                <Video
                                                    source={{ uri: BASE_URL + '/' + this.state.video }}   // Can be a URL or a local file.
                                                    resizeMode='contain'
                                                    style={{ width: '100%', height: 200, borderBottomColor: Colors.PRIMARY, borderWidth: 2 }}          // Callback when video cannot be loaded
                                                />
                                                :
                                                null
                                        } */}

                                        {
                                            this.state.ladder && this.state.ladder.pri === 5 && this.state.resume.length > 0 ?
                                                <View style={styles.section}>
                                                    <Text style={{ fontFamily: IranSansMobile, fontSize: RF(2.3), color: Colors.SECONDARY }}>رزومه های مرتبط با این آگهی</Text>
                                                    <FlatList
                                                        style={{marginVertical:8}}
                                                        numColumns={3}
                                                        data={this.state.resume}
                                                        renderItem={({ item, index }) => (
                                                            // <TouchableOpacity
                                                            //     style={{ marginVertical: wp('4%') }}
                                                            //     onPress={() => {
                                                            //         NavigationController.navigate('PlayerDetails', { id: item.id })
                                                            //     }}>
                                                            //     <Text>
                                                            //         {item.user.name} {item.user.lastName}
                                                            //     </Text>
                                                            // </TouchableOpacity>
                                                            
                                                            <TouchableOpacity
                                                                style={{flex:1, overflow:'hidden', border:.5, borderWidth:.5}}
                                                                onPress={() => {
                                                                    NavigationController.navigate('PlayerDetails', { id: item.id })
                                                                }}>
                                                                <Image source={ {uri: BASE_URL + '/' + item.user.pix}} style={{flex:1, aspectRatio:1}} />
                                                                <Text style={[styles.faFont, {textAlign:'center', paddingHorizontal:4, height:32, lineHeight:32, fontSize:10, backgroundColor: Colors.PRIMARY, color:'#FFF'}]} numberOfLines={1} ellipsizeMode={'clip'} >{item.user.name} {item.user.lastName}</Text>
                                                            </TouchableOpacity>
                                                        )}
                                                    />

                                                </View>
                                                :
                                                null
                                        }

                                        <View style={styles.section}>
                                            <Text style={{ fontFamily: IranSansMobile, fontSize: RF(2.3), color: Colors.SECONDARY }}>موقعیت آگهی</Text>
                                            <View style={{ flexDirection: 'column' }}>
                                                <Text style={[styles.name, { color: Colors.PRIMARY }]}>{this.state.city.state.name} {'/'} {this.state.city.name}</Text>
                                                <Text style={[styles.name, { color: Colors.PRIMARY }]} ellipsizeMode="middle" >آدرس :  {this.state.address ? this.state.address : 'فاقد آدرس'}</Text>
                                            </View>

                                            <MapView pitchEnabled={false} loadingEnabled={true} region={{ latitude: this.state.lat, longitude: this.state.lng, latitudeDelta: 0.01, longitudeDelta: 0.01 }} style={{ width: '100%', height: hp('30%'), marginTop: wp('2%') }}>
                                                <Marker coordinate={{ latitude: this.state.lat, longitude: this.state.lng, latitudeDelta: 0.02, longitudeDelta: 0.02 }} />
                                            </MapView>
                                            <Ripple onPress={() => this.onLocationPress()} style={{ padding: wp('2%'), borderWidth: 1, borderColor: Colors.PRIMARY, borderRadius: hp('1%'), flexDirection: 'row', marginTop: hp('3%'), justifyContent: 'center' }} >
                                                <MDIcons name="navigation" color={Colors.SECONDARY} size={wp('5%')} />
                                                <Text style={[styles.name, { color: Colors.SECONDARY, marginStart: wp('2%') }]}>مسیریابی به محل مورد نظر</Text>
                                            </Ripple>
                                        </View>
                                        <Ripple onPress={() => this.reportSpam()} style={[styles.section, { flexDirection: 'row' }]}>
                                            <FAIcons name="exclamation-triangle" size={wp('5%')} style={{ color: '#ffab00' }} />
                                            <Text style={[styles.name, { color: Colors.PRIMARY, marginStart: 8 }]}>گزارش مشکل در آگهی</Text>
                                        </Ripple>
                                    </View>

                                </HeaderImageScrollView>


                                <View style={styles.contactContainer}>
                                    <Ripple onPress={this.requestTermsView.bind(this)} style={{ backgroundColor: '#007BFF', height: '100%', justifyContent: 'center' }} >
                                        <Text style={{ alignSelf: 'center', fontFamily: IranYekanMobile, color: Colors.WHITE }} >اطلاعات تماس</Text>
                                    </Ripple>
                                </View>
                                <Terms onAcceptTerms={() => { this.onAcceptTerms() }} />
                                <Modal
                                    useNativeDriver={true}
                                    swipeDirection={'down'}
                                    onSwipeComplete={() => this.setState({ isContactInfoVisible: false })}
                                    onBackButtonPress={() => { this.setState({ isContactInfoVisible: false }) }}
                                    onBackdropPress={() => { this.setState({ isContactInfoVisible: false }) }}
                                    isVisible={this.state.isContactInfoVisible}
                                    style={{ margin: 0, marginTop: hp('70%'), borderTopStartRadius: wp('2%'), borderTopEndRadius: wp('2%'), backgroundColor: Colors.WHITE }}>

                                    <View style={{ padding: wp('5%') }}>
                                        <StatusBar backgroundColor="rgba(0,0,0,0.7)" barStyle="light-content" />
                                        <Text style={[styles.textStyle, { marginBottom: wp('3%') }]}>هشدار : لطفا پیش از انجام هرگونه معامله از صحت کالا یا خدمات ارایه شده به صورت حضوری اطمینان حاصل نمایید</Text>
                                        <TouchableOpacity onPress={() => this.callContact()} style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <AntDIcon name="phone" size={wp('5%')} color={Color.BUTTON_ACCEPT} />
                                            <Text style={styles.textStyle}>تماس با شماره {this.state.mobileNumber}</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.messageContact()} style={{ flexDirection: 'row', alignItems: 'center', marginTop: wp('2%') }}>
                                            <AntDIcon name="message1" size={wp('5%')} color={Color.BUTTON_ACCEPT} />
                                            <Text style={styles.textStyle}>ارسال پیامک به شماره {this.state.mobileNumber}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </Modal>
                            </View>
                        )
                }

            </View>
        );
    }


    renderHighlightText(text) {
        return (
            <Text style={[styles.name, { color: Colors.PRIMARY, lineHeight: wp('6%') }]}>
                {text}
            </Text>
        )
        var words = text.split(' ');
        var hashtagGlobalRegex = new RegExp(/#[^\s!@#$%^&*()=+./,\[{\]};:'"?><]+/g);
        var resultObject = new Array()
        for (var i = 0; i < words.length; i++) {
            if (hashtagGlobalRegex.test(words[i])) {
                resultObject.push(
                    <Text style={{ color: Colors.RED }} > {words[i]}</Text>
                )
            }
            else {
                resultObject.push(
                    <Text> {words[i]}</Text>
                )
            }
        }
        return (
            <Text style={[styles.name, { color: Colors.PRIMARY, lineHeight: wp('6%') }]}>                {
                resultObject.map(item => {
                    return (item)
                })
            }
            </Text>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        fontSize: 15,
        fontFamily: IranSansMobileBold,
        color: Colors.PRIMARY
    },
    name: {
        fontFamily: IranSansMobile,
        fontSize: RF(2),
        color: '#a2abb0'
    },
    section: {
        padding: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#cccccc',
        backgroundColor: '#FFF',
        elevation: 0
    },
    sectionTitle: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    sectionContent: {
        fontSize: 16,
        textAlign: 'justify',
    },
    keywords: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
    },
    keywordContainer: {
        backgroundColor: '#999999',
        borderRadius: 10,
        margin: 10,
        padding: 10,
    },
    keyword: {
        fontSize: 16,
        color: 'white',
    },
    titleContainer: {
        flex: 1,
        alignSelf: 'stretch',
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageTitle: {
        color: 'white',
        backgroundColor: 'transparent',
        fontSize: 24,
    },
    navTitleView: {
        height: MIN_HEIGHT,
        justifyContent: 'center',
        alignItems: 'center',
        opacity: 0,
    },
    navTitle: {
        color: 'white',
        fontSize: 16,
        backgroundColor: 'transparent',
        fontFamily: IranSansMobile,
        marginBottom: hp('3%'),
    },
    sectionLarge: {
        height: 600,
    },
    contactContainer: {
        height: 48,
        width: wp('100%'),
        backgroundColor: '#fff'
    },
    item: {
        width: screenWidth - 60,
        height: screenWidth - 90,
        marginTop: -20
    },
    imageContainer: {
        flex: 1,
        marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
        backgroundColor: 'white',
        borderRadius: 8,
    },
    image: {
        resizeMode: 'cover',
        width: wp('100%'),
        height: hp('50%'),
    },
    textStyle: {
        fontFamily: IranSansMobile,
        fontSize: RF(2),
        marginStart: wp('2%'),
        color: Colors.PRIMARY
    },
    faFont:
    {
        fontFamily: IranSansMobile,
    }
});


export default AdvertisementDetails;