import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Linking, ProgressBarAndroid } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { IranYekanMobile } from '../../constants/Fonts';
import NavigationController from '../../controllers/NavigationController';
import BaseAdapter from '../../webservices/BaseAdapter';
import { setStore } from 'trim-redux';
import UserAPI from '../../webservices/UserAPI';
import { Statusbar } from '../../components/viac';
import Colors from '../../constants/Colors';
import ConstInt from '../../constants/ConstInt';
import VersionAPI from '../../webservices/VersionAPI';
import VersionNumber from 'react-native-version-number'

class SplashScreen extends Component {

    componentDidMount() {
        this.initialize()
    }

    checkAppUpdate() {
        setTimeout(()=>{
            let installedVersionCode = VersionNumber.buildVersion
            VersionAPI.get(installedVersionCode)
                .then(response => {
                    let result = response.data.data
                    if (result) {
                        NavigationController.reset('UpdateScreen', { isForce: result.isForce, url: result.url, logs: result.logs })
                    } else {
                        NavigationController.reset('Tabs')
                    }
                }).catch(error=>{
                    console.log('error', error)
                })
        },2000)
    }

    initialize() {
        Linking.getInitialURL().then((url) => {
            // if (url) {
            console.log('Initial url is: ' + url);
            // }
        }).catch(err => console.error('An error occurred', err));
        Linking.addEventListener('url', (data) => {
            console.log("LINKING DATA ", data)
        })

        BaseAdapter.hasAuthToken().then(hasToken => {
            if (hasToken) {
                BaseAdapter.getAuthToken().then(token => {
                    setStore({
                        userInfo: {
                            isUserLoggedIn: hasToken,
                            token: token
                        }
                    })
                    UserAPI.getProfile()
                    // // NavigationController.reset('Tabs')
                    // setTimeout(this.changeAction, ConstInt.SPLASH_DELAY);
                    this.checkAppUpdate()
                })
            } else {
                // NavigationController.reset('Tabs')
                // setTimeout(this.changeAction, ConstInt.SPLASH_DELAY);
                this.checkAppUpdate()
            }
        })
    }

    changeAction() {
        NavigationController.reset('Tabs')
    }

    render() {
        return (
            <View style={styles.mainParent}>
                <Statusbar light={true} backgroundColor={Colors.PRIMARY} />
                <View style={styles.logoParent}>
                    <Image source={require('../../assets/images/logo.png')} style={styles.image} />
                    <Text style={styles.bottomText} >آگهی های ریز و درشت ورزشی</Text>
                </View>
                <View style={styles.prgBarParent}>
                    <ProgressBarAndroid styleAttr={'Horizontal'} style={styles.prgBar}  />
                </View>
            </View>
            // <View style={styles.container}>
            //     <Statusbar light={true} backgroundColor={Colors.PRIMARY} />
            //     <Image source={require('../../assets/images/logo.png')} style={styles.image} />
            //     <Text style={styles.bottomText} >آگهی های ریز و درشت ورزشی</Text>
            //     <View style={styles.prgBarParent}>
            //         <ProgressBarAndroid style={styles.prgBar}/>
            //     </View>

            // </View>
        )
    }
}

const styles = StyleSheet.create({
    mainParent: {
        flex: 1,
        backgroundColor: Colors.PRIMARY,
    },
    logoParent: {
        flex: .9,
        justifyContent: 'center'
    },
    prgBarParent: {
        flex: .1,
        justifyContent: 'center'
    },
    prgBar: {
        color: Colors.WHITE,
        width:200,
        alignSelf:'center'
    },
    image: {
        width: wp('55%'),
        height: wp('55%'),
        alignSelf: 'center'
    },
    bottomText: {
        color: '#fff',
        fontFamily: IranYekanMobile,
        alignSelf: 'center'
    }
})

export default SplashScreen