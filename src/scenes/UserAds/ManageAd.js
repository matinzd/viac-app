import React, { Component } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { View, Text, Image, I18nManager, StyleSheet, FlatList, RefreshControl, TouchableOpacity, Alert, DeviceEventEmitter } from 'react-native'
import Ripple from 'react-native-material-ripple';
import MapView, { Marker, MapViewAnimated } from 'react-native-maps'
import _ from 'lodash'
import RF from "react-native-responsive-fontsize"
import { setStore, getStore } from 'trim-redux';
import Modal from 'react-native-modal';
import { Toolbar } from 'react-native-material-ui'
import NavigationController from '../../controllers/NavigationController';
import Colors from '../../constants/Colors';
import { IranSansMobile, IranSansMobileBold, IranYekanMobile, IranYekanMobileBold } from '../../constants/Fonts';
import { Statusbar } from '../../components/viac';
import AdvertisingAPI from '../../webservices/AdvertisingAPI';
import { BASE_URL } from '../../webservices/BaseAdapter';
import * as Animatable from 'react-native-animatable'
import AntDIcon from 'react-native-vector-icons/AntDesign'
import MCIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import IIcons from 'react-native-vector-icons/Ionicons'
import MIcon from 'react-native-vector-icons/MaterialIcons'
import ViacToast from '../../controllers/ViacToast';

export default class ManageAd extends Component {

    constructor(props) {
        super(props)

        this.state = {
            id: this.props.navigation.getParam('id'),
            title: '',
            entries: [],
            categoryId: '',
            briefDescription: '',
            fullDescription: '',
            address: '',
            pix: '',
            latitude: '',
            longitude: '',
            status: '',
            ladder: {}
        }
    }

    componentDidMount() {
        AdvertisingAPI.get(this.props.navigation.state.params.id).then(response => {
            let data = response.data.data
            data.pix.map(item => {
                this.state.entries.push({
                    thumbnail: BASE_URL + '/' + item.url
                })
            })
            this.setState({
                title: data.title,
                id: data.id,
                categoryId: data.category.id,
                status: data.status,
                fullDescription: data.fullDescription,
                briefDescription: data.briefDescription,
                pix: data.pix,
                time: data.timeAgo,
                city: data.city,
                address: data.address,
                entries: this.state.entries,
                sans: data.sans,
                tags: data.tags,
                latitude: data.latitude,
                longitude: data.longitude,
                isBookmarked: data.isLike,
                advertisementId: data.id,
                mobileNumber: data.user.mobileNumber,
                ladder: data.ladder,
            })
        })
    }

    getData() {

    }

    onAdvertisementPress(id) {

    }

    onEditPress() {
        let editingObject = {
            type: 'edit',
            advertisementDetail: {
                id: this.props.navigation.getParam('id'),
                title: this.state.title,
                categoryId: this.state.categoryId,
                briefDescription: this.state.briefDescription,
                fullDescription: this.state.fullDescription,
                address: this.state.address,
                pix: this.state.pix,
                latitude: this.state.latitude,
                longitude: this.state.longitude,
                ladder: this.state.ladder,
            }
        }
        console.log('Editing obj ', editingObject)
        NavigationController.navigate('AddOrEditAdvertisement', editingObject)
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
                <Statusbar light={false} backgroundColor={Colors.WHITE} />
                <Toolbar
                    ref={ref => this.toolbar = ref}
                    leftElement={I18nManager.isRTL ? "arrow-forward" : "arrow-back"}
                    centerElement="مدیریت آگهی"
                    onLeftElementPress={() => {
                        NavigationController.goBack()
                    }}
                    onRightElementPress={(label) => { NavigationController.openDrawer() }}
                />
                {/* <Ripple onPress={() => NavigationController.navigate('AdvertisementDetails')} >
                    <Text style={{ fontFamily: IranSansMobileBold }} >جزییات آگهی</Text>
                </Ripple> */}
                <View style={{ flexDirection: 'row', padding: wp('5%') }}>
                    <Text style={{ fontFamily: IranSansMobile, fontSize: RF(2), color: Colors.PRIMARY, marginStart: wp('2%') }}>وضعیت آگهی : </Text>
                    <Text style={{ fontFamily: IranSansMobile, fontSize: RF(2), color: Colors.SECONDARY, marginStart: wp('2%') }}>{this.state.status}</Text>
                </View>
                <View style={{ flexDirection: 'row', padding: wp('5%') }}>
                    <Text style={{ fontFamily: IranYekanMobile, lineHeight: wp('6%'), fontSize: RF(2), color: Colors.Grays, marginStart: wp('2%') }}>
                        تایید آگهی ها حداکثر دو الی سه ساعت طول خواهند کشید و نتیجه ثبت، ارتقا یا حذف آگهی شما از طریق پیامک به شما اعلام خواهد شد.
                    </Text>
                </View>
                <Ripple
                    onPress={() => { NavigationController.navigate('Ladders', { id: this.state.id }) }}
                    style={{ width: '95%', alignSelf: 'center', height: 60, paddingHorizontal: wp('5%'), justifyContent: 'space-between', borderBottomColor: Colors.LIGHTGREY, borderBottomWidth: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <MCIcon name='chart-line' size={wp('5%')} />
                        <Text style={{ fontFamily: IranSansMobile, fontSize: RF(2), color: Colors.PRIMARY, marginStart: wp('2%') }}>ثبت پلکان</Text>
                    </View>
                    <View>
                        <IIcons name='ios-arrow-back' size={wp('5%')} />
                    </View>
                </Ripple>
                <Ripple
                    onPress={() => { NavigationController.navigate('AdvertisementDetails', { id: this.state.id }) }}
                    style={{ width: '95%', alignSelf: 'center', height: 60, paddingHorizontal: wp('5%'), justifyContent: 'space-between', borderBottomColor: Colors.LIGHTGREY, borderBottomWidth: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <MCIcon name='eye' size={wp('5%')} />
                        <Text style={{ fontFamily: IranSansMobile, fontSize: RF(2), color: Colors.PRIMARY, marginStart: wp('2%') }}>پیش نمایش آگهی</Text>
                    </View>
                    <View>
                        <IIcons name='ios-arrow-back' size={wp('5%')} />
                    </View>
                </Ripple>
                <Ripple
                    onPress={() => { this.onEditPress() }}
                    style={{ width: '95%', alignSelf: 'center', height: 60, paddingHorizontal: wp('5%'), justifyContent: 'space-between', borderBottomColor: Colors.LIGHTGREY, borderBottomWidth: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <MIcon name='edit' size={wp('5%')} />
                        <Text style={{ fontFamily: IranSansMobile, fontSize: RF(2), color: Colors.PRIMARY, marginStart: wp('2%') }}>ویرایش</Text>
                    </View>
                    <View>
                        <IIcons name='ios-arrow-back' size={wp('5%')} />
                    </View>
                </Ripple>
                <Ripple
                    onPress={() => { this.promptDelete(this.state.id) }}
                    style={{ width: '95%', alignSelf: 'center', height: 60, paddingHorizontal: wp('5%'), justifyContent: 'space-between', borderBottomColor: Colors.LIGHTGREY, borderBottomWidth: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <MCIcon name='delete' size={wp('5%')} />
                        <Text style={{ fontFamily: IranSansMobile, fontSize: RF(2), color: Colors.PRIMARY, marginStart: wp('2%') }}>حذف</Text>
                    </View>
                    <View>
                        <IIcons name='ios-arrow-back' size={wp('5%')} />
                    </View>
                </Ripple>
                <Ripple style={{ width: '95%', alignSelf: 'center', height: 60, paddingHorizontal: wp('5%'), justifyContent: 'space-between', borderBottomColor: Colors.LIGHTGREY, borderBottomWidth: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <MCIcon name='history' size={wp('5%')} />
                        <Text style={{ fontFamily: IranSansMobile, fontSize: RF(2), color: Colors.PRIMARY, marginStart: wp('2%') }}>تاریخچه پرداخت ها</Text>
                    </View>
                    <View>
                        <IIcons name='ios-arrow-back' size={wp('5%')} />
                    </View>
                </Ripple>
            </View>
        )
    }

    deleteAd(id) {
        AdvertisingAPI.delete(id).then(response => {
            ViacToast.showToast(response.data.message)
            NavigationController.goBack()
            DeviceEventEmitter.emit('onDeleteAd', {})
        })
    }

    promptDelete(id) {
        Alert.alert('حذف آگهی', 'آیا از حدف این آگهی اطمینان دارید؟', [
            {
                text: 'بله',
                onPress: () => this.deleteAd(id)
            },
            {
                text: 'خیر',
                style: 'cancel'
            }
        ])
    }


}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    textContainer: {
        padding: wp('5%')
    },
    bottomContainer: {
        height: hp('8%'),
        bottom: 0,
        position: 'absolute',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: wp('3%'),
    },
    textContentStyle: {
        fontFamily: IranSansMobile,
        color: Colors.PRIMARY,
        fontSize: RF(2)
    },
    textTitleStyle: {
        fontFamily: IranSansMobileBold,
        color: Colors.PRIMARY,
        fontSize: RF(2.5)
    },
    listItemContainer: {
        height: hp('20%'),
        width: wp('90%'),
        elevation: 5,
        backgroundColor: Colors.WHITE,
        marginTop: wp('3%'),
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        marginBottom: 10,
    },
    itemTextStyle: {
        fontFamily: IranYekanMobile,
        color: Colors.PRIMARY,
        fontSize: RF(2.5)
    }
})
