import React, { Component } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { View, Text, Image, I18nManager, StyleSheet, FlatList, RefreshControl, TouchableOpacity, Alert, DeviceEventEmitter } from 'react-native'
import Ripple from 'react-native-material-ripple';
import MapView, { Marker, MapViewAnimated } from 'react-native-maps'
import _ from 'lodash'
import RF from "react-native-responsive-fontsize"
import { setStore, getStore } from 'trim-redux';
import Modal from 'react-native-modal';
import { Toolbar } from 'react-native-material-ui'
import NavigationController from '../../controllers/NavigationController';
import Colors from '../../constants/Colors';
import { IranSansMobile, IranSansMobileBold, IranYekanMobile, IranYekanMobileBold } from '../../constants/Fonts';
import { Statusbar } from '../../components/viac';
import AdvertisingAPI from '../../webservices/AdvertisingAPI';
import { BASE_URL } from '../../webservices/BaseAdapter';
import * as Animatable from 'react-native-animatable'
import AntDIcon from 'react-native-vector-icons/AntDesign'
import ViacToast from '../../controllers/ViacToast';
import {ListItem} from '../Items/AdvertismentItemL'
import { TouchableRipple } from 'react-native-paper';
export default class MyAds extends Component {

    constructor(props) {
        super(props)

        this.state = {
            data: [],
            refreshing: false,
            active: 0,
            inActive: 0,
            deleted: 0
        }
    }

    componentDidMount() {
        this.onDeleteListener = DeviceEventEmitter.addListener('onDeleteAd', () => this.getData())
        this.getData()
    }

    getData() {
        this.setState({ refreshing: true })
        AdvertisingAPI.getStatics()
            .then(response => {
                let statics = response.data.data
                this.setState({
                    active: statics.activedAdsCount,
                    inActive: statics.inActiveAdsCount,
                    deleted: statics.removedAdsCount,
                })
            })
        AdvertisingAPI.getUserAds()
            .then(response => {
                this.setState({
                    data: response.data.data,
                    refreshing: false
                })
            })
    }

    onAdvertisementPress(id) {
        NavigationController.navigate('ManageAd', { id })
    }

    componentWillUnmount() {
        this.onDeleteListener.remove()
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
                <Statusbar light={false} backgroundColor={Colors.WHITE} />
                <Toolbar
                    ref={ref => this.toolbar = ref}
                    leftElement={I18nManager.isRTL ? "arrow-forward" : "arrow-back"}
                    centerElement="آگهی های من"
                    onLeftElementPress={() => {
                        NavigationController.goBack()
                    }}
                    onRightElementPress={(label) => { NavigationController.openDrawer() }}
                />
                {/* <Ripple onPress={() => NavigationController.navigate('AdvertisementDetails')} >
                    <Text style={{ fontFamily: IranSansMobileBold }} >جزییات آگهی</Text>
                </Ripple> */}

                <View style={{ flex: 1, width: wp('100%'), alignSelf: 'center' }}>
                    <FlatList
                        data={this.state.data}
                        refreshControl={
                            <RefreshControl onRefresh={() => this.getData()} refreshing={this.state.refreshing} />
                        }
                        onEndReached={() => {
                            console.log('On end reached')
                        }}
                        ListEmptyComponent={() => (
                            !this.state.refreshing ?
                                <View style={{ width: wp('100%'), marginTop: wp('4%') }}>
                                    <Text style={[styles.itemTextStyle, { textAlign: 'center' }]}>شما هیچ آگهی ثبت نکرده اید ! 😢</Text>
                                </View>
                                :
                                null
                        )}
                        ListHeaderComponent={this.renderHeader()}
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                        renderItem={({ item, index }) => (
                            <TouchableRipple onPress={() => this.onAdvertisementPress(item.id)}>
                                <ListItem data={{ title: item.title, ladder: item.ladder, baseUrl: BASE_URL, pix: item.pix, briefDescription: item.briefDescription, timeAgo: item.timeAgo, status: item.status }} />
                            </TouchableRipple>
                            
                            // <TouchableOpacity
                            //     style={[styles.listItemContainer, { flexDirection: 'row' }]}
                            //     onPress={() => this.onAdvertisementPress(item.id)}
                            // >
                            //     <View>
                            //         <Image source={item.pix ? { uri: BASE_URL + '/' + item.pix.url } : require('../../assets/images/default_img.png')} style={{ height: hp('20%'), width: hp('20%') }} />
                            //         <View style={{ flexDirection: 'row', position: 'absolute', backgroundColor: 'transparent', width: wp('22%'), height: wp('6%'), justifyContent: 'flex-end', alignSelf: 'flex-end', padding: wp('1%') }}>
                            //             {
                            //                 item.ladder.label !== null ?
                            //                     <View>
                            //                         <View style={{ width: wp('10%'), height: wp('6%'), backgroundColor: Colors.ORANGETAG }}>
                            //                             <Text style={{ color: Colors.WHITE, alignSelf: 'center', fontFamily: IranYekanMobile, justifyContent: 'center' }}>{item.ladder.label}</Text>
                            //                         </View>
                                                    
                            //                     </View>
                            //                     :
                            //                     null
                            //             }
                            //         </View>
                            //     </View>
                            //     <View style={{ width: wp('55%'), height: hp('20%'), padding: wp('2%'), justifyContent: 'center' }}>
                            //         <Text style={[styles.itemTextStyle, { fontFamily: IranYekanMobileBold, fontSize: wp('4%'), textAlign: 'auto' }]} >{item.title}</Text>
                            //         <Text numberOfLines={3} allowFontScaling={false} ellipsizeMode="tail" style={[styles.itemTextStyle, { fontSize: wp('3%') }]} >{item.briefDescription}</Text>
                            //         <Text style={[styles.itemTextStyle, { color: '#d5d5d5', fontSize: wp('3%'), textAlign: 'auto' }]}>{item.timeAgo}</Text>

                            //     </View>
                            //     <View style={{ backgroundColor: '#081931', position: 'absolute', flexDirection: 'row', end: 0, top: 0, justifyContent: 'flex-end' }}>
                            //         <Text style={{ fontFamily: IranYekanMobile, color: Colors.WHITE, paddingHorizontal: wp('6%'), paddingVertical: wp('1%') }}>{item.status}</Text>
                            //     </View>
                            //     {/* <TouchableOpacity onPress={() => this.promptDelete(item.id)} style={{ position: 'absolute', flexDirection: 'row', zIndex: 10, end: 0, bottom: 0, justifyContent: 'flex-end' }}>
                            //         <AntDIcon name='delete' color={'#081931'} style={{ alignSelf: 'center', padding: wp('2%') }} size={wp('5%')} />
                            //     </TouchableOpacity> */}
                            // </TouchableOpacity>
                        )}
                    />
                </View>
            </View>
        )
    }

    deleteAd(id) {
        AdvertisingAPI.delete(id).then(response => {
            ViacToast.showToast(response.data.message)
            this.getData()
        })
    }

    promptDelete(id) {
        Alert.alert('حذف آگهی', 'آیا از حدف این آگهی اطمینان دارید؟', [
            {
                text: 'بله',
                onPress: () => this.deleteAd(id)
            },
            {
                text: 'خیر',
                style: 'cancel'
            }
        ])
    }

    renderHeader() {
        return (
            <View>
                <View style={{ flexDirection: 'row', height: hp('20%') }}>
                    <Animatable.View animation='slideInRight' duration={1000} useNativeDriver={true} style={{ flex: 1, backgroundColor: '#081931', margin: wp('1%'), justifyContent: 'center', width: '100%' }}>
                        <Text style={{ fontFamily: IranSansMobile, color: Colors.WHITE, alignSelf: 'center' }}>آگهی های فعال</Text>
                        <Text style={{ fontFamily: IranSansMobile, color: '#FFB822', alignSelf: 'center', fontSize: RF(4) }}>{this.state.active}</Text>
                        <AntDIcon name='checkcircleo' color={Colors.WHITE} style={{ alignSelf: 'center' }} size={wp('5%')} />
                    </Animatable.View>
                    <Animatable.View animation='slideInLeft' duration={1000} useNativeDriver={true} style={{ flex: 1, backgroundColor: '#081931', margin: wp('1%'), justifyContent: 'center', width: '100%' }}>
                        <Text style={{ fontFamily: IranSansMobile, color: Colors.WHITE, alignSelf: 'center' }}>آگهی های منتظر تائید</Text>
                        <Text style={{ fontFamily: IranSansMobile, color: '#FFB822', alignSelf: 'center', fontSize: RF(4) }}>{this.state.inActive}</Text>
                        <AntDIcon name='warning' color={Colors.WHITE} style={{ alignSelf: 'center' }} size={wp('5%')} />
                    </Animatable.View>
                </View>
                <View style={{ flexDirection: 'row', height: hp('20%') }}>
                    <Animatable.View animation='slideInRight' duration={1000} useNativeDriver={true} style={{ flex: 1, backgroundColor: '#081931', margin: wp('1%'), justifyContent: 'center', width: '100%' }}>
                        <Text style={{ fontFamily: IranSansMobile, color: Colors.WHITE, alignSelf: 'center' }}>آگهی های حذف شده</Text>
                        <Text style={{ fontFamily: IranSansMobile, color: '#FFB822', alignSelf: 'center', fontSize: RF(4) }}>{this.state.deleted}</Text>
                        <AntDIcon name='delete' color={Colors.WHITE} style={{ alignSelf: 'center' }} size={wp('5%')} />
                    </Animatable.View>
                    <Animatable.View animation='slideInLeft' duration={1000} useNativeDriver={true} style={{ flex: 1, backgroundColor: '#081931', margin: wp('1%'), justifyContent: 'center', width: '100%' }}>
                        <Text style={{ fontFamily: IranSansMobile, color: Colors.WHITE, alignSelf: 'center' }}>همه آگهی های شما</Text>
                        <Text style={{ fontFamily: IranSansMobile, color: '#FFB822', alignSelf: 'center', fontSize: RF(4) }}>{this.state.data.length}</Text>
                        <AntDIcon name='bars' color={Colors.WHITE} style={{ alignSelf: 'center' }} size={wp('5%')} />
                    </Animatable.View>
                </View>
            </View>
        )
    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    textContainer: {
        padding: wp('5%')
    },
    bottomContainer: {
        height: hp('8%'),
        bottom: 0,
        position: 'absolute',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: wp('3%'),
    },
    textContentStyle: {
        fontFamily: IranSansMobile,
        color: Colors.PRIMARY,
        fontSize: RF(2)
    },
    textTitleStyle: {
        fontFamily: IranSansMobileBold,
        color: Colors.PRIMARY,
        fontSize: RF(2.5)
    },
    listItemContainer: {
        height: hp('20%'),
        width: wp('90%'),
        elevation: 5,
        backgroundColor: Colors.WHITE,
        marginTop: wp('3%'),
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        marginBottom: 10,
    },
    itemTextStyle: {
        fontFamily: IranYekanMobile,
        color: Colors.PRIMARY,
        fontSize: RF(2.5)
    }
})
