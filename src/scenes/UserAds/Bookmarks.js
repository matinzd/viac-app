import React, { Component } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { View, Text, Image, I18nManager, StyleSheet, FlatList, RefreshControl } from 'react-native'
import Ripple from 'react-native-material-ripple';
import MapView, { Marker, MapViewAnimated } from 'react-native-maps'
import _ from 'lodash'
import RF from "react-native-responsive-fontsize"
import { setStore, getStore } from 'trim-redux';
import Modal from 'react-native-modal';
import { Toolbar } from 'react-native-material-ui'
import NavigationController from '../../controllers/NavigationController';
import Colors from '../../constants/Colors';
import { IranSansMobile, IranSansMobileBold, IranYekanMobile, IranYekanMobileBold } from '../../constants/Fonts';
import { Statusbar } from '../../components/viac';
import AdvertisingAPI from '../../webservices/AdvertisingAPI';
import { BASE_URL } from '../../webservices/BaseAdapter';
import {ListItem} from '../Items/AdvertismentItemL'
import { TouchableRipple } from 'react-native-paper';

export default class Bookmarks extends Component {

    constructor(props) {
        super(props)

        this.state = {
            data: [],
            refreshing: false
        }
    }

    componentDidMount() {
        this.setState({ refreshing: true })
        this.getData()
    }

    getData() {
        AdvertisingAPI.getUserBookmarks()
            .then(response => {
                this.setState({
                    data: response.data.data,
                    refreshing: false
                })
            })
    }

    onAdvertisementPress(id) {
        NavigationController.navigate('AdvertisementDetails', { id })
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
                <Statusbar light={false} backgroundColor={Colors.WHITE} />
                <Toolbar
                    ref={ref => this.toolbar = ref}
                    leftElement={I18nManager.isRTL ? "arrow-forward" : "arrow-back"}
                    centerElement="نشان شده ها"
                    onLeftElementPress={() => {
                        NavigationController.goBack()
                    }}
                />
                {/* <Ripple onPress={() => NavigationController.navigate('AdvertisementDetails')} >
                    <Text style={{ fontFamily: IranSansMobileBold }} >جزییات آگهی</Text>
                </Ripple> */}
                <View style={{ flex: 1, width: wp('100%'), alignSelf: 'center' }}>
                    <FlatList
                        data={this.state.data}
                        refreshControl={
                            <RefreshControl onRefresh={() => this.getData()} refreshing={this.state.refreshing} />
                        }
                        onEndReached={() => {
                            console.log('On end reached')
                        }}
                        ListEmptyComponent={() => (
                            !this.state.refreshing ?
                                <View style={{ width: wp('100%'), marginTop: wp('4%') }}>
                                    <Text style={[styles.itemTextStyle, { textAlign: 'center' }]}>شما هیچ آگهی را نشان نکرده اید ! 😢</Text>
                                </View>
                                :
                                null
                        )}
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                        renderItem={({ item, index }) => (
                            <TouchableRipple onPress={() => this.onAdvertisementPress(item.id)}>
                                <ListItem data={{ title: item.title, ladder: item.ladder, baseUrl: BASE_URL, pix: item.pix, briefDescription: item.briefDescription, timeAgo: item.timeAgo, status: item.status }} />
                            </TouchableRipple>
                            
                            // <Ripple
                            //     style={[styles.listItemContainer, { flexDirection: 'row' }]}
                            //     onPress={() => this.onAdvertisementPress(item.id)}
                            // >
                            //     <View>
                            //         <Image source={item.pix ? { uri: BASE_URL + '/' + item.pix.url } : require('../../assets/images/default_img.png')} style={{ height: hp('20%'), width: hp('20%') }} />
                            //         <View style={{ flexDirection: 'row', position: 'absolute', backgroundColor: 'transparent', width: wp('22%'), height: wp('6%'), justifyContent: 'flex-end', alignSelf: 'flex-end', padding: wp('1%') }}>
                            //             {
                            //                 item.ladder && item.ladder.label ?
                            //                     <View>
                            //                         <View style={{ width: wp('10%'), height: wp('6%'), backgroundColor: Colors.ORANGETAG }}>
                            //                             <Text style={{ color: Colors.WHITE, alignSelf: 'center', fontFamily: IranYekanMobile, justifyContent: 'center' }}>فوری</Text>
                            //                         </View>
                            //                         <View style={{ width: wp('10%'), marginStart: wp('1%'), height: wp('6%'), backgroundColor: Colors.ORANGETAG }}>
                            //                             <Text style={{ color: Colors.WHITE, alignSelf: 'center', fontFamily: IranYekanMobile, justifyContent: 'center' }}>ویژه</Text>
                            //                         </View>
                            //                     </View>
                            //                     :
                            //                     null
                            //             }
                            //         </View>
                            //     </View>
                            //     <View style={{ width: wp('55%'), height: hp('20%'), padding: wp('2%'), justifyContent: 'center' }}>
                            //         <Text style={[styles.itemTextStyle, { fontFamily: IranYekanMobileBold, fontSize: wp('4%'), textAlign: 'auto' }]} >{item.title}</Text>
                            //         <Text numberOfLines={3} allowFontScaling={false} ellipsizeMode="tail" style={[styles.itemTextStyle, { fontSize: wp('3%') }]} >{item.briefDescription}</Text>
                            //         <Text style={[styles.itemTextStyle, { color: '#d5d5d5', fontSize: wp('3%'), textAlign: 'auto' }]}>{item.timeAgo}</Text>
                            //     </View>
                            // </Ripple>
                        )}
                    />
                </View>
            </View>
        )
    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    textContainer: {
        padding: wp('5%')
    },
    bottomContainer: {
        height: hp('8%'),
        bottom: 0,
        position: 'absolute',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: wp('3%'),
    },
    textContentStyle: {
        fontFamily: IranSansMobile,
        color: Colors.PRIMARY,
        fontSize: RF(2)
    },
    textTitleStyle: {
        fontFamily: IranSansMobileBold,
        color: Colors.PRIMARY,
        fontSize: RF(2.5)
    },
    listItemContainer: {
        height: hp('20%'),
        width: wp('90%'),
        elevation: 5,
        backgroundColor: Colors.WHITE,
        marginTop: wp('3%'),
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        marginBottom: 10,
    },
    itemTextStyle: {
        fontFamily: IranYekanMobile,
        color: Colors.PRIMARY,
        fontSize: RF(2.5)
    }
})
