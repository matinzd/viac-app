import React, { Component } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { View, Text, Image, I18nManager, StyleSheet, Linking, ActivityIndicator, FlatList, ScrollView, RefreshControl, TouchableOpacity, Alert } from 'react-native'
import Ripple from 'react-native-material-ripple';
import MapView, { Marker, MapViewAnimated } from 'react-native-maps'
import _ from 'lodash'
import RF from "react-native-responsive-fontsize"
import { setStore, getStore } from 'trim-redux';
import Modal from 'react-native-modal';
import { Toolbar, RadioButton, Checkbox } from 'react-native-material-ui'
import NavigationController from '../../controllers/NavigationController';
import Colors from '../../constants/Colors';
import { IranSansMobile, IranSansMobileBold, IranYekanMobile, IranYekanMobileBold } from '../../constants/Fonts';
import { Statusbar } from '../../components/viac';
import AdvertisingAPI from '../../webservices/AdvertisingAPI';
import { BASE_URL } from '../../webservices/BaseAdapter';
import * as Animatable from 'react-native-animatable'
import AntDIcon from 'react-native-vector-icons/AntDesign'
import MCIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import IIcons from 'react-native-vector-icons/Ionicons'
import MIcon from 'react-native-vector-icons/MaterialIcons'
import ViacToast from '../../controllers/ViacToast';
import LadderAPI from '../../webservices/LadderAPI';
import Color from '../../styles/Color';
import IABHandler from '../../controllers/IABHandler';
import PaymentAPI from '../../webservices/PaymentAPI';

export default class Ladders extends Component {

    constructor(props) {
        super(props)

        this.state = {
            id: this.props.navigation.getParam('id'),
            ladders: [],
            selectedLadder: null,
            selectedLadderId: null,
            payByCafeBazaar: false
        }
    }

    componentDidMount() {
        this.setState({ refreshing: true })
        this.getData()

    }

    getData() {
        LadderAPI.getAll().then((response) => {
            this.setState({
                ladders: response.data.data
            })
        })
    }

    onAdvertisementPress(id) {

    }

    pay() {
        if (!this.state.selectedLadder) {
            ViacToast.showToast('لطفا یک پلکان انتخاب کنید', 3000)
            return
        }
        if (this.state.payByCafeBazaar) {
            if (this.state.selectedLadder.sku != '') {
                IABHandler.payByCafeBazaar(this.state.selectedLadder.sku, 1001)
                    .then(result => {
                        console.log('IAB ', result)
                        PaymentAPI.submitLadder(
                            this.state.id,
                            this.state.selectedLadderId,
                            result,
                            false
                        ).then(response => {
                            ViacToast.showToast(response.data.message, 3000)
                            if (response.data.success)
                                IABHandler.consumeProduct(this.state.selectedLadder.sku)
                                    .then(() => {
                                        NavigationController.goBack()
                                    })
                                    .catch(error => {
                                        ViacToast.showToast(error.message)
                                    })
                        })
                    })
                    .catch(err => {
                        ViacToast.showToast(err.message, 3000)
                    })
            } else {
                PaymentAPI.submitLadder(this.state.id, this.state.selectedLadderId, null, true)
                    .then(response => {
                        if (response.data.data.payUrl) {
                            Linking.openURL(BASE_URL + '/' + response.data.data.payUrl)
                        } else {
                            if (response.data.success) {
                                ViacToast.showToast(response.data.message)
                                NavigationController.goBack()
                            }
                        }
                    })
            }
        } else {
            ///PAY BY ZARINPAL
            PaymentAPI.submitLadder(this.state.id, this.state.selectedLadderId, null, true)
                .then(response => {
                    Linking.openURL(BASE_URL + '/' + response.data.data.payUrl)
                })
        }
    }

    componentWillUnmount() {
        console.log('Remove listener')
        IABHandler.close()
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
                <Statusbar light={false} backgroundColor={Colors.WHITE} />
                <Toolbar
                    ref={ref => this.toolbar = ref}
                    leftElement={I18nManager.isRTL ? "arrow-forward" : "arrow-back"}
                    centerElement="مدیریت پلکان ها"
                    onLeftElementPress={() => {
                        NavigationController.goBack()
                    }}
                    onRightElementPress={(label) => { NavigationController.openDrawer() }}
                />
                {/* <Ripple onPress={() => NavigationController.navigate('AdvertisementDetails')} >
                    <Text style={{ fontFamily: IranSansMobileBold }} >جزییات آگهی</Text>
                </Ripple> */}
                <ScrollView style={{ flex: 1, paddingHorizontal:0 }}>
                    <Text style={[styles.textStyle, { marginTop:8, marginStart:16 }]}>انتخاب پلکان</Text>
                    <Text style={[styles.textStyle, { marginVertical:4, marginStart:16, fontSize: RF(1.8), color: Colors.Grays }]}>
                        با انتخاب پلکان مناسب آگهی خود را در معرض دید کاربران قرار دهید
                    </Text>

                    <FlatList
                        contentContainerStyle={{ flex: 1, width: '100%', paddingHorizontal:16, }}
                        data={this.state.ladders}
                        ListEmptyComponent={
                            <ActivityIndicator size='small' color={Color.BUTTON_ACCEPT} />
                        }
                        renderItem={({ item, index }) => (
                            item.price !== 0 ? (
                                <View style={{ width: '100%', alignSelf: 'center', height: null, elevation: 5, backgroundColor: Colors.WHITE, margin: wp('2%') }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Checkbox
                                        key={item.id.toString()}
                                        label={item.name}
                                        checked={this.state.selectedLadderId === item.id}
                                        value={item.id}
                                        onCheck={() => {
                                            this.setState({
                                                selectedLadder: item,
                                                selectedLadderId: item.id
                                            })
                                        }}
                                        onSelect={(value) => {
                                            this.setState({
                                                selectedLadder: item,
                                                selectedLadderId: item.id
                                            })
                                        }}
                                    />
                                    <Text style={[styles.textStyle, { marginVertical: wp('2%'), padding: wp('2%'), color: Colors.RED }]}>
                                        {`${item.price} تومان`}
                                    </Text>
                                </View>
                                <View style={{ width: '95%', backgroundColor: Colors.LIGHTGREY, height: 0.5, alignSelf: 'center' }} />
                                <Text style={[styles.textStyle, { marginVertical: wp('2%'), padding: wp('2%'), fontSize: RF(1.8), color: Colors.SECONDARY }]}>
                                    {item.description}
                                </Text>
                            </View>
                            ) : null
                        )}
                    />

                </ScrollView>
                <View style={{
                    height: 48,
                    width: wp('100%'),
                    backgroundColor: '#fff',
                }}>
                    <Ripple onPress={() => { this.pay() }} style={{ backgroundColor: '#007BFF', height: '100%', justifyContent: 'center' }} >
                        <Text style={{ alignSelf: 'center', fontFamily: IranYekanMobile, color: Colors.WHITE }} >
                            {
                                this.state.selectedLadder && this.state.selectedLadder.sku != '' ?
                                    'پرداخت'
                                    :
                                    'اعمال پلکان'
                            }
                        </Text>
                    </Ripple>
                </View>
            </View>
        )
    }

    deleteAd(id) {
        AdvertisingAPI.delete(id).then(response => {
            ViacToast.showToast(response.data.message)
            this.getData()
        })
    }

    promptDelete(id) {
        Alert.alert('حذف آگهی', 'آیا از حدف این آگهی اطمینان دارید؟', [
            {
                text: 'بله',
                onPress: () => this.deleteAd(id)
            },
            {
                text: 'خیر',
                style: 'cancel'
            }
        ])
    }


}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    textContainer: {
        padding: wp('5%')
    },
    bottomContainer: {
        height: hp('8%'),
        bottom: 0,
        position: 'absolute',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: wp('3%'),
    },
    textContentStyle: {
        fontFamily: IranSansMobile,
        color: Colors.PRIMARY,
        fontSize: RF(2)
    },
    textTitleStyle: {
        fontFamily: IranSansMobileBold,
        color: Colors.PRIMARY,
        fontSize: RF(2.5)
    },
    listItemContainer: {
        height: hp('20%'),
        width: wp('90%'),
        elevation: 5,
        backgroundColor: Colors.WHITE,
        marginTop: wp('3%'),
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        marginBottom: 10,
    },
    itemTextStyle: {
        fontFamily: IranYekanMobile,
        color: Colors.PRIMARY,
        fontSize: RF(2.5)
    },
    textStyle: {
        fontFamily: IranSansMobile,
        color: Colors.PRIMARY
    },
})
