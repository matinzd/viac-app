import { createStore } from 'trim-redux'

const states = {
    userInfo: {
        isUserLoggedIn: false,
        token: '',
    },
    advertisementBottomSheet: null,
    isStateModalVisible: false,
    playerBottomSheet: null,
    isStateSelected: false,
    selectedStateId: '',
    location: {
        latitude: 0,
        longitude: 0
    },
    isLocationModalVisible: false,
    selectedStateString: '',
    advertisements: [],
    selectedTags: [],
    selectedCities: [],
    selectedStates: [],
    selectedCategories: [],
    enums: {},
    textSearch: '',
    adsIsGridView: false,
    isTermsVisible: false,
    areTermsAccepted: false
}

export default store = createStore(states)