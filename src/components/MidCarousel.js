import React, {Component} from 'react'
import {View, StyleSheet} from 'react-native'
import Carousel, {Pagination} from 'react-native-snap-carousel'
import {wp, log} from '../helper/HelperMethods'


export default class MidCarousel extends Component{

 
    constructor(props)
    {
        super(props)
        this.state = {
            // enum(default, stack, tinder)
            layout: props.layout !== undefined ? props.layout : 'default',
            entries : props.entries !== undefined ? props.entries : [],
            activeSlide : this.props.activeSlide !== undefined ? this.props.activeSlide : 0,
            sliderWidth: this.props.sliderWidth !== undefined ? wp(this.props.sliderWidth) : wp(100),
            itemWidth:  this.props.itemWidth !== undefined ? wp(this.props.itemWidth) : wp(100),
            hasPagination: this.props.hasPagination !== undefined ? this.props.hasPagination : false,
            outerPagination: this.props.outerPagination !== undefined ? this.props.outerPagination : false,
            dotContainerStyle: this.props.dotContainerStyle !== undefined? [styles.dotContainerStyle, this.props.dotContainerStyle] : styles.dotContainerStyle,
            dotStyle: this.props.dotStyle !== undefined? [styles.dotStyle, this.props.dotStyle] : styles.dotStyle,
            inactiveDotStyle: this.props.inactiveDotStyle !== undefined? [styles.inactiveDotStyle, this.props.inactiveDotStyle] : styles.inactiveDotStyle,
            inactiveDotOpacity: this.props.inactiveDotOpacity !== undefined? this.props.inactiveDotOpacity : .5,
            inactiveDotScale: this.props.inactiveDotScale !== undefined? this.props.inactiveDotScale : .8,
            rootStyle: this.props.rootStyle !== undefined ? [styles.root ,this.props.rootStyle] : styles.root,
            autoPlay: this.props.autoPlay !== undefined ? this.props.autoPlay : false,
            autoPlayInterval: this.props.autoPlayInterval !== undefined ? this.props.autoPlayInterval : 5000,
            loop: this.props.loop !== undefined ? this.props.loop : false
        }
    }

    static getDerivedStateFromProps(props, state) {
        if (props.entries !== state.entries) {
          return {
            entries: props.entries,
          };
        }
        return null;
      }

    get pagination () {
        const { entries, activeSlide } = this.state;
        return (
            <Pagination
              dotsLength={entries.length}
              activeDotIndex={activeSlide}
              containerStyle={this.state.outerPagination ? styles.outerPagination : styles.innerPagination}
              dotContainerStyle={this.state.dotContainerStyle}
              dotStyle={this.state.dotStyle}
              inactiveDotStyle={this.state.inactiveDotStyle}
              inactiveDotOpacity={this.state.inactiveDotOpacity}
              inactiveDotScale={this.state.inactiveDotScale}
            />
        );
    }

    
    render()
    {
        return(
            <View style={this.state.rootStyle}>
                <Carousel
                    layout={this.state.layout}
                    ref={(c) => { this._carousel = c; }}
                    data={this.state.entries}
                    renderItem={this.props.renderItem}
                    sliderWidth={this.state.sliderWidth}
                    itemWidth={this.state.itemWidth}
                    firstItem={this.state.activeSlide}
                    loop={this.state.loop}
                    autoplay={this.state.autoPlay}
                    autoplayInterval={this.state.autoPlayInterval}
                    onBeforeSnapToItem={(index) => this.setState({ activeSlide: index }) }
                    />
                    { this.state.hasPagination ? this.pagination : null }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    root: {backgroundColor:'#FFF'},
    innerPagination : {padding:0, paddingVertical:4, position: 'absolute', bottom:0, width:'100%' },
    outerPagination : {padding:0, paddingVertical:4, width:'100%'},
    dotContainerStyle: {width:10, height:10, marginHorizontal:4},
    dotStyle: { width: 10, height: 10, borderRadius: 6, backgroundColor: '#3ab', borderWidth:1, borderColor:'#fff' },
    inactiveDotStyle: {}
})