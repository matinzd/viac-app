import React, { Component } from 'react';
import { StatusBar } from 'react-native'

class Statusbar extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <StatusBar
                backgroundColor={this.props.backgroundColor}
                barStyle={this.props.light ? 'light-content' : 'dark-content'}
            />
        )
    }
}


export default Statusbar