import { Component } from "react";

export class BottomDrawer extends Component<ViacBottomDrawerProps> { }

export interface ViacBottomDrawerProps {
    onCollapsed?: () => {};
    roundedEdges?: boolean;
    shadow?: boolean;
    startUp?: boolean;
    downDisplay?: string;
    containerHeight?: string;
    offset?: string;
}

