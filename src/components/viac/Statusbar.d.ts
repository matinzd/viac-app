import { Component } from "react";

export class Statusbar extends Component<ViacStatusbarProps> { }

export interface ViacStatusbarProps {
    light?: boolean;
    backgroundColor?: string;
}