import React from 'react';
import { View, Text, Linking } from 'react-native'
import TabBar from "@mindinventory/react-native-tab-bar-interaction";
import ViacRouter from './ViacRouter';
import { Provider } from 'trim-redux'
import { Drawer } from 'react-native-material-ui';
import { COLOR, ThemeContext, getTheme } from 'react-native-material-ui';
import Colors from '../constants/Colors';
import store from '../redux/store';
import { IranSansMobile, IranYekanMobile } from '../constants/Fonts';
import Toast from 'react-native-custom-toast';
import ViacToast from '../controllers/ViacToast';
import ResumeAPI from '../webservices/ResumeAPI';


const ViacThemeKit = {
    toolbar: {
        container: {
            backgroundColor: Colors.WHITE,
            color: Colors.PRIMARY,
        },
        rightElement: {
            color: Colors.SECONDARY
        },
        leftElement: {
            color: Colors.SECONDARY
        },
        centerElement: {
            color: Colors.SECONDARY
        }
    },
    checkbox: {
        label: {
            fontFamily: IranYekanMobile
        },
        icon: {
            color: Colors.SECONDARY
        }
    },
}

class Main extends React.Component {

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        ResumeAPI.getEnum()
    }
    

    render() {
        return (
            <Provider store={store} >
                <ThemeContext.Provider value={getTheme(ViacThemeKit)} >
                    <ViacRouter />
                </ThemeContext.Provider>
                <Toast backgroundColor = {Colors.SECONDARY} position = "bottom" ref={ref => ViacToast.setToastRef(ref)} />
            </Provider>
        )
    }
}

export default Main