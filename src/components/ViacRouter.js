import React from 'react';
import { View, Text } from 'react-native'
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import { createAppContainer, createStackNavigator, createDrawerNavigator, createBottomTabNavigator } from 'react-navigation'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NavigationController from "../controllers/NavigationController";
import { flipY, fromLeft } from 'react-navigation-transitions';


import Advertisements from "../scenes/Tabs/Advertisements";
import Resume from '../scenes/Tabs/Resume';
import SplashScreen from '../scenes/SplashScreen';
import Authentication from '../scenes/Auth';
import Sidebar from './Sidebar';
import AdvertisementDetails from '../scenes/Details/AdvertisementDetails';
import Test from '../scenes/Test';
import States from '../scenes/States';
import PlayerDetails from '../scenes/Details/Playerdetails';
import Schedules from '../scenes/Details/Schedules';
import AddOrEditAdvertisement from '../scenes/Tabs/Advertisements/AddOrEditAdvertisement';
import SendSpam from '../scenes/Details/SendSpam';
import Profile from '../scenes/Details/Profile';
import MyAds from '../scenes/UserAds/MyAds';
import Bookmarks from '../scenes/UserAds/Bookmarks';
import About from '../scenes/Details/About';
import ContactUs from '../scenes/Details/ContactUs';
import ManageResumes from '../scenes/Tabs/Resume/ManageResumes';


import AntDIcon from 'react-native-vector-icons/AntDesign';
import FAIcon from 'react-native-vector-icons/FontAwesome5';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import IoIcons from 'react-native-vector-icons/Ionicons';
import Colors from '../constants/Colors';
import { IranSansMobile } from '../constants/Fonts';
import AddResume from '../scenes/Tabs/Resume/AddResume';
import ResumeList from '../scenes/Tabs/Resume/ResumeList';
import CoachDetails from '../scenes/Details/CoachDetails';
import RefreeDetails from '../scenes/Details/RefreeDetails';
import UpdateScreen from '../scenes/UpdateScreen/UpdateScreen';
import EditResume from '../scenes/Tabs/Resume/EditResume';
import ManageAd from '../scenes/UserAds/ManageAd';
import Ladders from '../scenes/UserAds/Ladders';

const tabNavigator = createBottomTabNavigator({
    Advertisements: { screen: Advertisements },
    Resume: { screen: Resume },
    // Profile: { screen: Profile },
}, {
        initialRouteName: 'Advertisements',
        labeled: true,
        lazy: false,
        tabBarOptions: {
            showLabel: false,
            keyboardHidesTabBar: true,
            style: {
                height: hp('8%'),
                backgroundColor: Colors.WHITE
            },
            activeTintColor: Colors.SECONDARY
        },
        swipeEnabled: true,
        animationEnabled: true,
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused }) => {
                const { routeName } = navigation.state;
                let icon;
                switch (routeName) {
                    case 'Advertisements':
                        icon = 'Ionicons'
                        text = 'آگهی‌ها'
                        break
                    case 'Resume':
                        icon = 'users'
                        text = 'رزومه ها'
                        break
                }
                return (
                    <View style={{ height: hp('8%'), alignItems: 'center', justifyContent: 'center' }}>
                        {
                            routeName === 'Advertisements' ?
                                <IoIcons name={focused ? 'md-notifications' : 'ios-notifications-outline'} size={25} color={focused ? Colors.SECONDARY : Colors.PRIMARY} />
                                :
                                <MIcon name="description" color={focused ? Colors.SECONDARY : Colors.PRIMARY} size={20} />
                            }
                        <Text style={{ alignSelf: 'center', fontFamily: IranSansMobile, color: focused ? Colors.SECONDARY : Colors.PRIMARY }}>{text}</Text>
                    </View>
                )
            },

        })
    })

const MainNavigator = createStackNavigator({
    SplashScreen: SplashScreen,
    UpdateScreen: UpdateScreen,
    Authentication: Authentication,
    Tabs: tabNavigator,
    Resumes: Resume,
    ManageResumes: ManageResumes,
    AddResume: AddResume,
    EditResume: EditResume,
    ResumeList: ResumeList,
    AdvertisementDetails: AdvertisementDetails,
    SendSpam: SendSpam,
    PlayerDetails: PlayerDetails,
    CoachDetails: CoachDetails,
    RefreeDetails: RefreeDetails,
    Profile: Profile,
    AddOrEditAdvertisement: AddOrEditAdvertisement,
    Schedules: Schedules,
    MyAds: MyAds,
    ManageAd: ManageAd,
    Ladders: Ladders,
    Bookmarks: Bookmarks,
    About: About,
    ContactUs: ContactUs,
    Test: Test,
    States: States
}, {
        headerMode: 'none',
        initialRouteName: 'SplashScreen',
        transitionConfig: () => fromLeft()
})

const DrawerNavigatorRight = createDrawerNavigator({
    MainNavigator: MainNavigator,
}, {
        contentComponent: props => <Sidebar {...props} />,
        drawerPosition: "right",
        drawerType: "slide",
        drawerWidth: wp('65%'),

    })

const AppContainer = createAppContainer(DrawerNavigatorRight)

export default class ViacRouter extends React.Component {
    render() {
        return <AppContainer ref={navigatorRef => NavigationController.setTopLevelNavigator(navigatorRef)} />;
    }

}