import React, { Component } from 'react';
import { View, Text, StyleSheet, ToastAndroid, Alert, Image, ImageBackground } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Colors from '../constants/Colors';
import { IranSansMobile } from '../constants/Fonts';
import Icon from 'react-native-vector-icons/FontAwesome';
import AntDIcon from 'react-native-vector-icons/AntDesign';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import Ripple from 'react-native-material-ripple';
import NavigationController from '../controllers/NavigationController';
import { getStore, connect, setStore } from 'trim-redux';
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncResource from '@react-native-community/async-storage';
import Color from '../styles/Color';
import { BASE_URL } from '../webservices/BaseAdapter';
import ViacToast from '../controllers/ViacToast';

class Sidebar extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userInfo: {
                isUserLoggedIn: false,
                name: '',
                lastName: '',
                pix: ''
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            userInfo: nextProps.userInfo
        })
    }

    onLoginLogoutPressed() {

        if (this.state.userInfo.isUserLoggedIn) {
            Alert.alert('خروج از حساب کاربری', 'آیا میخواهید از حساب کاربری خود در ویاک خارج شوید؟', [
                {
                    text: 'بله',
                    onPress: () => this.clearData()
                },
                {
                    text: 'خیر',
                    style: 'cancel'
                }
            ])

        } else {
            NavigationController.navigate('Authentication')
        }
    }

    clearData() {
        setStore({
            userInfo: {
                isUserLoggedIn: false,
                token: '',
            }
        })
        AsyncResource.clear().then(() => {
            ViacToast.showToast('شما با موفقیت از حساب کاربری خود خارج شدید ! ', 2000)
            NavigationController.navigate('SplashScreen')
        })
    }

    render() {
        return (
            <View style={styles.container}>
                {/* <ImageBackground source={require('../assets/images/sidebar_bg.png')} style={styles.userInfoContainer}> */}
                <View style={styles.userInfoContainer}>
                    {
                        this.state.userInfo.isUserLoggedIn ?
                            <Image source={this.state.userInfo.pix ? { uri: BASE_URL + '/' + this.state.userInfo.pix } : require('../assets/images/profile_default.png')} style={{ width: wp('20%'), justifyContent: 'flex-start', alignItems: 'center', marginStart: wp('5%'), backgroundColor: Color.BUTTON_ACCEPT, height: wp('20%'), borderRadius: wp('10%') }} />
                            :
                            null
                    }
                    <Ripple onPress={() => {
                        !this.state.userInfo.isUserLoggedIn ? this.onLoginLogoutPressed() : null
                    }} >

                        <View style={styles.itemContainer}>
                            {
                                this.state.userInfo.isUserLoggedIn ?
                                    null
                                    :
                                    <Icon name="sign-in" color={Colors.WHITE} size={20} />
                            }
                            <Text style={[styles.menuTextStyle, { color: Colors.WHITE, alignSelf: 'center', marginStart: wp('3%') }]} >
                                {
                                    this.state.userInfo.isUserLoggedIn ?
                                        `${this.state.userInfo.name} ${this.state.userInfo.lastName}`
                                        :
                                        'ثبت نام / ورود'
                                }
                            </Text>
                        </View>
                    </Ripple>
                    {/* </ImageBackground> */}
                </View>
                <View style={styles.listMenuContainer}>
                    {/** Items */}

                    {/** Divider TODO Flat list */}
                    {
                        this.state.userInfo.isUserLoggedIn ?
                            <View>
                                <Ripple onPress={() => NavigationController.navigate('Profile')}>
                                    <View style={styles.itemContainer}>
                                        <MIcon name="account-circle" color={Colors.SECONDARY} size={20} />
                                        <Text style={[styles.menuTextStyle, { color: Colors.PRIMARY, alignSelf: 'center', marginStart: wp('3%') }]} >پروفایل</Text>
                                    </View>
                                </Ripple>
                                <View style={{ width: wp('65%'), borderWidth: .5, borderColor: '#f1f1f1' }} />
                                <Ripple onPress={() => NavigationController.navigate('MyAds')}>
                                    <View style={styles.itemContainer}>
                                        <MIcon name="notifications" color={Colors.SECONDARY} size={20} />
                                        <Text style={[styles.menuTextStyle, { color: Colors.PRIMARY, alignSelf: 'center', marginStart: wp('3%') }]} >آگهی های من</Text>
                                    </View>
                                </Ripple>
                                <View style={{ width: wp('65%'), borderWidth: .5, borderColor: '#f1f1f1' }} />
                                <Ripple onPress={() => NavigationController.navigate('Bookmarks')}>
                                    <View style={styles.itemContainer}>
                                        <MIcon name="bookmark" color={Colors.SECONDARY} size={20} />
                                        <Text style={[styles.menuTextStyle, { color: Colors.PRIMARY, alignSelf: 'center', marginStart: wp('3%') }]} >نشان شده ها</Text>
                                    </View>
                                </Ripple>
                                <View style={{ width: wp('65%'), borderWidth: .5, borderColor: '#f1f1f1' }} />
                                <Ripple onPress={() => this.onLoginLogoutPressed()}>
                                    <View style={styles.itemContainer}>
                                        {
                                            this.state.userInfo.isUserLoggedIn ?
                                                <Icon name="sign-out" color={Colors.SECONDARY} size={20} />
                                                :
                                                <Icon name="sign-in" color={Colors.SECONDARY} size={20} />
                                        }
                                        <Text style={[styles.menuTextStyle, { color: Colors.PRIMARY, alignSelf: 'center', marginStart: wp('3%') }]} >خروج از حساب</Text>
                                    </View>
                                </Ripple>
                            </View>
                            :
                            null
                    }
                    <View style={{ width: wp('65%'), borderWidth: .5, borderColor: '#f1f1f1' }} />
                    {/* <Ripple>
                        <View style={styles.itemContainer}>
                            <AntDIcon name="questioncircleo" color={Colors.PRIMARY} size={20} />
                            <Text style={[styles.menuTextStyle, { color: Colors.PRIMARY, alignSelf: 'center', marginStart: wp('3%') }]} >سوالات متداول</Text>
                        </View>
                    </Ripple> */}
                    <View style={{ width: wp('65%'), borderWidth: .5, borderColor: '#f1f1f1' }} />
                    <Ripple onPress={() => NavigationController.navigate('ContactUs')}>
                        <View style={styles.itemContainer}>
                            <MIcon name="call" color={Colors.SECONDARY} size={20} />
                            <Text style={[styles.menuTextStyle, { color: Colors.PRIMARY, alignSelf: 'center', marginStart: wp('3%') }]} >تماس با ما</Text>
                        </View>
                    </Ripple>
                    <View style={{ width: wp('65%'), borderWidth: .5, borderColor: '#f1f1f1' }} />
                    <Ripple onPress={() => NavigationController.navigate('About')}>
                        <View style={styles.itemContainer}>
                            <MIcon name="info" color={Colors.SECONDARY} size={20} />
                            <Text style={[styles.menuTextStyle, { color: Colors.PRIMARY, alignSelf: 'center', marginStart: wp('3%') }]} >درباره ما</Text>
                        </View>
                    </Ripple>

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    userInfoContainer: {
        width: wp('65%'),
        height: hp('25%'),
        backgroundColor: Colors.SECONDARY,
        justifyContent: 'flex-end',
    },
    itemContainer: {
        flexDirection: 'row',
        margin: wp('3%'),
        alignItems: 'center'
    },
    listMenuContainer: {

    },
    menuTextStyle: {
        fontFamily: IranSansMobile
    }
})

const mapStatesToProps = state => ({ userInfo: state.userInfo })
export default connect(mapStatesToProps)(Sidebar)
