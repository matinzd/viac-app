/**
 * @format
 */

import { AppRegistry } from 'react-native';
import { name as appName } from './app.json';
import Main from './src/components/Main';
import { I18nManager } from 'react-native';

I18nManager.forceRTL(true)
console.disableYellowBox = true
AppRegistry.registerComponent(appName, () => Main);
